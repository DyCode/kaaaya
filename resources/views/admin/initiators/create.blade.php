@extends('adminlte::page')

@section('title', 'Create Initiator')

@section('content_header')
    <h1>Initiator</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/initiators">Initiator</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.initiators.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.initiators.form', ["title" => "Add Initiator", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
