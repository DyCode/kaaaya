<div class="col-md-8">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				
				<div class='form-group'>
						<label for=''>Name</label>
						<input name='name' type='text' class='form-control' value='{!! $edit ? $data->name : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Institution Name</label>
						<input name='institution_name' type='text' class='form-control' value='{!! $edit ? $data->institution_name : null !!}' />
				</div>
				@if(!$edit)
				<div class='form-group'>
						<label for=''>Password</label>
						<input name='password' type='text' class='form-control' value='{!! $edit ? $data->password : null !!}' />
				</div>
				@endif
				<div class='form-group'>
						<label for=''>Email</label>
						<input name='email' type='email' class='form-control' value='{!! $edit ? $data->email : null !!}' />
				</div>
				
				
				<div class='form-group'>
						<label for=''>Home Address</label>
						<textarea name='home_address'  class='form-control' rows='5'>{!! $edit ? $data->home_address : null !!}</textarea>
				</div>
				<div class='form-group'>
						<label for=''>Office Address</label>
						<textarea name='office_address'  class='form-control' rows='5'>{!! $edit ? $data->office_address : null !!}</textarea>
				</div>
				<div class='form-group'>
						<label for=''>Active Flag</label>
						<select name="active_flag" class="form-control">
							<option value="0" {!! $edit ? $data->active_flag == '0' ? 'SELECTED' : null : null !!}>Inactive</option>
							<option value="1" {!! $edit ? $data->active_flag == '1' ? 'SELECTED' : null : null !!}>Active</option>
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
<div class="col-md-4">
	<div class="box box-primary">
		<div class="box-header">
	      <h3 class="box-title">
	      </h3>
	    </div>
		<div class="box-body">
			<div class='form-group'>
				<label for=''>Phone Number</label>
				<input name='phone_number' type='text' class='form-control' value='{!! $edit ? $data->phone_number : null !!}' />
			</div>
			<div class='form-group'>
				<label for=''>Birth Date</label>
				<input name='birth_date' type='datetime-local' class='form-control' value='{!! $edit ? $data->birth_date ? $data->birth_date->format("Y-m-d\TH:i") : null : null !!}' />
			</div>
			<div class='form-group'>
				<label for=''>Gender</label>
				<select name="gender" id="gender" class="form-control">
					<option value="M" {!! $edit ? $data->gender == "M" ? "SELECTED" : null : null !!}>Male</option>
					<option value="F" {!! $edit ? $data->gender == "F" ? "SELECTED" : null : null !!}>Female</option>
				</select>
			</div>
			<div class='form-group'>
				<label for=''>Profile Image</label>
				<input name='profile_image' type='file' onchange="return readURL(this, '#profile_image-preview')"/>
				<input name='profile_image' type='hidden' class='form-control' value='{!! $edit ? $data->profile_image : null !!}' />
				@if($edit)
				<br><label>Preview</label><br>
				<img id="profile_image-preview" src='{!! $edit ? Storage::url($data->profile_image) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
				@endif
			</div>
		</div>
	</div>
</div>
