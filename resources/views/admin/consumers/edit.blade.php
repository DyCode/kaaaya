@extends('adminlte::page')

@section('title', 'Edit Consumer')

@section('content_header')
    <h1>Consumer</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/consumers">Consumer</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.consumers.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.consumers.form', ["title" => "Edit Consumer", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
