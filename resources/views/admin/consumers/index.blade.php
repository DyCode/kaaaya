@extends('adminlte::page')

@section('title', 'Consumer')

@section('content_header')
    <h1>Consumer</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/consumers">Consumer</a></li>
	    <li class="active">Lists</li>
	 </ol>
@stop


@section('content')
    @include('admin.misc.alert')
    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
					<div class="box-header">
            <h3 class="box-title">
            @if(env('FULL_ACCESS'))
              <a href="{!! route('admin.consumers.create') !!}" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> Consumer</a>
			@endif
            </h3>
            <div class="box-tools">

                <form>
              <div class="input-group input-group-sm" style="width: 300px;">
                <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
                </form>
            </div>
          </div>
          <div class="box-body table-responsive">
          	<table class="table table-hover">
						  <tbody><tr>
						    <th>#</th>
								<th>Profile Image</th>
								<th>Name</th>
								<th>Institution Name</th>
								<th>Email</th>
								<th>Phone Number</th>
								<th>Birth Date</th>
								<th>Gender</th>
								<th>Home Address</th>
								<th>Office Address</th>
								<th>Active Flag</th>
								<th>Created</th>
								<th>Updated</th>
						    <th></th>
						  </tr>
						    @foreach($data as $i => $d)
						    <tr>
						      <td>{!! $i + $data->firstItem() !!}</td>
									<td><img src='{!! Storage::url($d->profile_image) !!}' width='150' onerror="this.src='/images/notfound.jpeg';" /></td>
									<td>{!! $d->name !!}</td>
									<td>{!! $d->institution_name !!}</td>
									<td>{!! $d->email !!}</td>
									<td>{!! $d->phone_number !!}</td>
									<td>{!! $d->birth_date !!}</td>
									<td>{!! $d->gender !!}</td>
									<td>{!! $d->home_address !!}</td>
									<td>{!! $d->office_address !!}</td>
									<td>{!! $d->active_flag ? '<span class=\'glyphicon glyphicon-ok-sign text-success\'></span>' : '<span class=\'glyphicon glyphicon-remove-sign text-danger\'></span>' !!}</td>
									<td>{!! optional($d->created_by_user)->name !!}</td>
									<td>{!! optional($d->updated_by_user)->name !!}</td>
						      <td>
						        <form method="post" action="{!! route('admin.consumers.destroy', ['id' => $d->id]) !!}">
						          <input type="hidden" name="_method" value="delete" />
						          @csrf
						        <div class="btn-group pull-right"><a href="{!! route('admin.consumers.edit', ['id' => $d->id]) !!}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a><button class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></button></div>
						        </form>
						      </td>
						    </tr>
						    @endforeach
						  </tbody>
						</table>
						{!! $data->render() !!}
          </div>
    		</div>
    	</div>

    </div>
@stop
