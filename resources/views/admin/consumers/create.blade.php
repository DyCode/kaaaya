@extends('adminlte::page')

@section('title', 'Create Consumer')

@section('content_header')
    <h1>Consumer</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/consumers">Consumer</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.consumers.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.consumers.form', ["title" => "Add Consumer", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
