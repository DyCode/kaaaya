@extends('adminlte::page')

@section('title', 'Create Dream')

@section('content_header')
    <h1>Dream</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/dreams">Dream</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.dreams.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.dreams.form', ["title" => "Add Dream", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
