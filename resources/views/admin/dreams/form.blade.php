<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				@foreach(config('translatable.locales') as $lang)
					<div class="form-group">
						<label for=""><img src="/images/flags/{!! $lang !!}.png" alt="{!! $lang !!}" width="16" style="margin-right: 10px">Title</label>
						<input type="text" name="{!! $lang !!}[title]"  class="form-control" value="{!! $edit ? $data->{'title:'.$lang} : null !!}" />
					</div>
					<div class="form-group">
						<label for=""><img src="/images/flags/{!! $lang !!}.png" alt="{!! $lang !!}" width="16" style="margin-right: 10px">Description</label>
						<textarea name="{!! $lang !!}[description]"  class="form-control" rows="5">{!! $edit ? $data->{'description:'.$lang} : null !!}</textarea>
					</div>
				@endforeach
				<div class='form-group'>
					<label for=''>Consumer</label>
					<select name='consumer_id'  class='form-control'>
						<option value='' >Select First</option>
						@foreach($consumers as $consumer)	
							<option value='{!! $consumer->id !!}' {!! $edit ? $consumer->id == $data->consumer_id ? 'SELECTED' : null : null !!}>{!! $consumer->name !!}</option>
						@endforeach
					</select>
				</div>
				<div class='form-group'>
					<label for=''>Donation Category</label>
					<select name='donation_category_id'  class='form-control'>
						<option value='' >Select First</option>
						@foreach($donation_categories as $donation_category)	
							<option value='{!! $donation_category->id !!}' {!! $edit ? $donation_category->id == $data->donation_category_id ? 'SELECTED' : null : null !!}>{!! $donation_category->category !!}</option>
						@endforeach
					</select>
				</div>
				<div class='form-group'>
						<label for=''>Image</label>
						<input name='image' type='file' />
						<input name='image' type='hidden' class='form-control' value='{!! $edit ? $data->image : null !!}' />
						@if($edit)
						<br><label>Preview</label><br>
						<img src='{!! $edit ? Storage::url($data->image) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
						@endif
				</div>
				<div class='form-group'>
						<label for=''>Due Date</label>
						<input name='due_date' type='datetime-local' class='form-control' value='{!! $edit ? $data->due_date ? $data->due_date->format("Y-m-d\TH:i") : null : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Verification Flag</label>
						<select name="verification_flag" class="form-control">
							<option value="0" {!! $edit ? $data->verification_flag == '0' ? 'SELECTED' : null : null !!}>Unverified</option>
							<option value="1" {!! $edit ? $data->verification_flag == '1' ? 'SELECTED' : null : null !!}>Verified</option>
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
