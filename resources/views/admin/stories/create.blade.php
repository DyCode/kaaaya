@extends('adminlte::page')

@section('title', 'Create Story')

@section('content_header')
    <h1>Story</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/stories">Story</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.stories.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.stories.form', ["title" => "Add Story", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
