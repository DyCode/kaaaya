@extends('adminlte::page')

@section('title', 'Create Gallery Comment')

@section('content_header')
    <h1>Gallery Comment</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/gallery_comments">Gallery Comment</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.gallery_comments.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.gallery_comments.form', ["title" => "Add Gallery Comment", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
