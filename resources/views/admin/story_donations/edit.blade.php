@extends('adminlte::page')

@section('title', 'Edit Story Donation')

@section('content_header')
    <h1>Story Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/story_donations">Story Donation</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.story_donations.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.story_donations.form', ["title" => "Edit Story Donation", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
