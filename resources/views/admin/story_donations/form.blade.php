<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Story</label>
						<select name='story_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($stories as $story)	
								<option value='{!! $story->id !!}' {!! $edit ? $story->id == $data->story_id ? 'SELECTED' : null : null !!}>{!! $story->title !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Consumer</label>
							<select name='consumer_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($consumers as $consumer)	
								<option value='{!! $consumer->id !!}' {!! $edit ? $consumer->id == $data->consumer_id ? 'SELECTED' : null : null !!}>{!! $consumer->name !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Date</label>
						<input name='date' type='datetime-local' class='form-control' value='{!! $edit ? $data->date ? $data->date->format("Y-m-d\TH:i") : null : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Amount</label>
						<input name='amount' type='number' min='0' class='form-control' value='{!! $edit ? $data->amount ? $data->amount : null : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Transfer Slip</label>
						<input name='transfer_slip' type='file' />
						<input name='transfer_slip' type='hidden' class='form-control' value='{!! $edit ? $data->transfer_slip : null !!}' />
						@if($edit)
						<br><label>Preview</label><br>
						<img src='{!! $edit ? Storage::url($data->transfer_slip) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
						@endif
				</div>
				<div class='form-group'>
						<label for=''>Comment</label>
						<textarea name='comment'  class='form-control' rows='5'>{!! $edit ? $data->comment : null !!}</textarea>
				</div>
				<div class='form-group'>
						<label for=''>Verification Flag</label>
						<select name="verification_flag" class="form-control">
							<option value="0" {!! $edit ? $data->verification_flag == '0' ? 'SELECTED' : null : null !!}>Unverified</option>
							<option value="1" {!! $edit ? $data->verification_flag == '1' ? 'SELECTED' : null : null !!}>Verified</option>
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Active Flag</label>
						<select name="active_flag" class="form-control">
							<option value="0" {!! $edit ? $data->active_flag == '0' ? 'SELECTED' : null : null !!}>Inactive</option>
							<option value="1" {!! $edit ? $data->active_flag == '1' ? 'SELECTED' : null : null !!}>Active</option>
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
