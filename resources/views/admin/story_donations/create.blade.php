@extends('adminlte::page')

@section('title', 'Create Story Donation')

@section('content_header')
    <h1>Story Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/story_donations">Story Donation</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.story_donations.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.story_donations.form', ["title" => "Add Story Donation", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
