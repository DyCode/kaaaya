<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Consumer</label>
						<select name='consumer_id'  class='form-control'>
								<option value='' >Select First</option>
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Date</label>
						<input name='date' type='datetime-local' class='form-control' value='{!! $edit ? $data->date ? $data->date->format("Y-m-d\TH:i") : null : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Notification</label>
						<input name='notification' type='text' class='form-control' value='{!! $edit ? $data->notification : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Is Read</label>
						<input name='is_read' type='radio' value='0' {!! $edit ? $data->is_read == '0' ? 'CHECKED' : null : null !!} /> No
						<input name='is_read' type='radio' value='1' {!! $edit ? $data->is_read == '1' ? 'CHECKED' : null : null !!} /> Yes
				</div>
				<div class='form-group'>
						<label for=''>Is Sent</label>
						<input name='is_sent' type='radio' value='0' {!! $edit ? $data->is_sent == '0' ? 'CHECKED' : null : null !!} /> No
						<input name='is_sent' type='radio' value='1' {!! $edit ? $data->is_sent == '1' ? 'CHECKED' : null : null !!} /> Yes
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
