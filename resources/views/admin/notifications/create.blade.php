@extends('adminlte::page')

@section('title', 'Create Notification')

@section('content_header')
    <h1>Notification</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/notifications">Notification</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.notifications.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.notifications.form', ["title" => "Add Notification", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
