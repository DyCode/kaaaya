@extends('adminlte::page')

@section('title', 'Edit Contact Us Banner')

@section('content_header')
    <h1>Contact Us Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/contact_us_banners">Contact Us Banner</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.contact_us_banners.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.contact_us_banners.form', ["title" => "Edit Contact Us Banner", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
