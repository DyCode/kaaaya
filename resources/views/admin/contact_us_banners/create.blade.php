@extends('adminlte::page')

@section('title', 'Create Contact Us Banner')

@section('content_header')
    <h1>Contact Us Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/contact_us_banners">Contact Us Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.contact_us_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.contact_us_banners.form', ["title" => "Add Contact Us Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
