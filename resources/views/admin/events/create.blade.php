@extends('adminlte::page')

@section('title', 'Create Event')

@section('content_header')
    <h1>Event</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/events">Event</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.events.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.events.form', ["title" => "Add Event", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
