@extends('adminlte::page')

@section('title', 'Edit Event')

@section('content_header')
    <h1>Event</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/events">Event</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.events.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.events.form', ["title" => "Edit Event", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
