@extends('adminlte::page')

@section('title', 'User')

@section('content_header')
    <h1>User</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/users">User</a></li>
	    <li class="active">Lists</li>
	 </ol>
@stop


@section('content')
    @include('admin.misc.alert')
    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
					<div class="box-header">
            <h3 class="box-title">
              <a href="{!! route('admin.users.create') !!}" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> User</a>

            </h3>
            <div class="box-tools">

                <form>
              <div class="input-group input-group-sm" style="width: 300px;">
                <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
                </form>
            </div>
          </div>
          <div class="box-body table-responsive">
          	<table class="table table-hover">
						  <tbody><tr>
						    <th>#</th>
								<th>Name</th>
								<th>Email</th>
								<th>Role</th>
						    <th></th>
						  </tr>
						    @foreach($data as $i => $d)
						    <tr>
						      	<td>{!! $i + $data->firstItem() !!}</td>
								<td>{!! $d->name !!}</td>
								<td>{!! $d->email !!}</td>
								<td>{!! $d->roles->map(function($d) { return $d->name; })->implode(', ') !!}</td>
						      	<td>
							        <form method="post" action="{!! route('admin.users.destroy', ['id' => $d->id]) !!}">
							          <input type="hidden" name="_method" value="delete" />
							          @csrf
							        <div class="btn-group pull-right"><a href="{!! route('admin.users.edit', ['id' => $d->id]) !!}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a><button class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></button></div>
							        </form>
						      	</td>
						    </tr>
						    @endforeach
						  </tbody>
						</table>
						{!! $data->render() !!}
          </div>
    		</div>
    	</div>

    </div>
@stop
