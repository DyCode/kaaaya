<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
			<div class='form-group'>
				<label for=''>Name</label>
				<input name='name' type='text' class='form-control' value='{!! $edit ? $data->name : null !!}' />
			</div>
			
			@if(!$edit)
			<div class='form-group'>
				<label for=''>Email</label>
				<input name='email' type='text' class='form-control' value='{!! $edit ? $data->email : null !!}' />
			</div>

			<div class='form-group'>
				<label for=''>Password</label>
				<input name='password' type='password' class='form-control' value='{!! $edit ? $data->password : null !!}' />
			</div>
			@endif

			<div class='form-group'>
				<label for=''>Role</label>
				<select name="role_id" id="" class="form-control">
					@foreach($roles as $role)
						<option value="{!! $role->id !!}" {!! $edit ? $user_role ? ($user_role->id == $role->id ? 'SELECTED' : null ) : null : null !!}>{!! $role->name !!}</option>
					@endforeach
				</select>
			</div>
			
			<button type="submit" class="btn btn-success">Submit</button>
    	</div>
  	</div>
</div>
