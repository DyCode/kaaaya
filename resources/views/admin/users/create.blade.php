@extends('adminlte::page')

@section('title', 'Create User')

@section('content_header')
    <h1>User</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/users">User</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.users.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.users.form', ["title" => "Add User", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
