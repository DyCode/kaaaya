@extends('adminlte::page')

@section('title', 'Create Faq')

@section('content_header')
    <h1>Faq</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/faqs">Faq</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.faqs.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.faqs.form', ["title" => "Add Faq", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
