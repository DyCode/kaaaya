@extends('adminlte::page')

@section('title', 'Edit Faq')

@section('content_header')
    <h1>Faq</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/faqs">Faq</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.faqs.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.faqs.form', ["title" => "Edit Faq", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
