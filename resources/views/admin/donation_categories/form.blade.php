<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Category</label>
						<input name='category' type='text' class='form-control' value='{!! $edit ? $data->category : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Category Icon</label>
						<input name='category_icon' type='file' onchange="return readURL(this, '#category_icon-preview')"/>
						<input name='category_icon' type='hidden' class='form-control' value='{!! $edit ? $data->category_icon : null !!}' />
						@if($edit)
						<br><label>Preview</label><br>
						<img id="category_icon-preview" src='{!! $edit ? Storage::url($data->category_icon) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
						@endif
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
