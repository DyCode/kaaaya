@extends('adminlte::page')

@section('title', 'Create Donation Category')

@section('content_header')
    <h1>Donation Category</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/donation_categories">Donation Category</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.donation_categories.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.donation_categories.form', ["title" => "Add Donation Category", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
