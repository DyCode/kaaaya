<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Category</label>
						<input name='category' type='text' class='form-control' value='{!! $edit ? $data->category : null !!}' />
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
