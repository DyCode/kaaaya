@extends('adminlte::page')

@section('title', 'Create Faq Category')

@section('content_header')
    <h1>Faq Category</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/faq_categories">Faq Category</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.faq_categories.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.faq_categories.form', ["title" => "Add Faq Category", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
