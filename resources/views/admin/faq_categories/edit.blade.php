@extends('adminlte::page')

@section('title', 'Edit Faq Category')

@section('content_header')
    <h1>Faq Category</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/faq_categories">Faq Category</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.faq_categories.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.faq_categories.form', ["title" => "Edit Faq Category", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
