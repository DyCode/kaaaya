<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Banner</label>
					    <input name='banner' type='file' onchange="return readURL(this, '#banner-preview')" />
						<input name='banner' type='hidden' class='form-control' value='{!! $edit ? $data->banner : null !!}' />
						@if($edit)
						<br><label>Preview</label><br>
						<img id="banner-preview" src='{!! $edit ? Storage::url($data->banner) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
						@endif
				</div>
				<div class='form-group'>
						<label for=''>Banner No</label>
						<input name='banner_no' type='text' class='form-control' value='{!! $edit ? $data->banner_no : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Active Flag</label>
						<select name="active_flag" class="form-control">
							<option value="0" {!! $edit ? $data->active_flag == '0' ? 'SELECTED' : null : null !!}>Inactive</option>
							<option value="1" {!! $edit ? $data->active_flag == '1' ? 'SELECTED' : null : null !!}>Active</option>
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
