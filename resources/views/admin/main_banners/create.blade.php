@extends('adminlte::page')

@section('title', 'Create Main Banner')

@section('content_header')
    <h1>Main Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/main_banners">Main Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.main_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.main_banners.form', ["title" => "Add Main Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
