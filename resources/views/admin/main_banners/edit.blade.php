@extends('adminlte::page')

@section('title', 'Edit Main Banner')

@section('content_header')
    <h1>Main Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/main_banners">Main Banner</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.main_banners.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.main_banners.form', ["title" => "Edit Main Banner", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
