@extends('adminlte::page')

@section('title', 'Edit Shipping Method')

@section('content_header')
    <h1>Shipping Method</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/shipping_methods">Shipping Method</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.shipping_methods.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.shipping_methods.form', ["title" => "Edit Shipping Method", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
