@extends('adminlte::page')

@section('title', 'Create Shipping Method')

@section('content_header')
    <h1>Shipping Method</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/shipping_methods">Shipping Method</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.shipping_methods.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.shipping_methods.form', ["title" => "Add Shipping Method", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
