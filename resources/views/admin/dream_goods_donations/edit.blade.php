@extends('adminlte::page')

@section('title', 'Edit Dream Goods Donation')

@section('content_header')
    <h1>Dream Goods Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/dream_goods_donations">Dream Goods Donation</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.dream_goods_donations.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.dream_goods_donations.form', ["title" => "Edit Dream Goods Donation", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
