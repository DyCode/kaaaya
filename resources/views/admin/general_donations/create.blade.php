@extends('adminlte::page')

@section('title', 'Create General Donation')

@section('content_header')
    <h1>General Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/general_donations">General Donation</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.general_donations.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.general_donations.form', ["title" => "Add General Donation", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
