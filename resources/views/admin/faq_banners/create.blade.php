@extends('adminlte::page')

@section('title', 'Create FAQ Banner')

@section('content_header')
    <h1>FAQ Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/faq_banners">FAQ Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.faq_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.faq_banners.form', ["title" => "Add FAQ Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
