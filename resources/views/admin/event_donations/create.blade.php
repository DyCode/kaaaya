@extends('adminlte::page')

@section('title', 'Create Event Donation')

@section('content_header')
    <h1>Event Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/event_donations">Event Donation</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.event_donations.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.event_donations.form', ["title" => "Add Event Donation", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
