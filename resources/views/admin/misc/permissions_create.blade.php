@extends('adminlte::page')

@section('title', 'Create Permission')

@section('content_header')
    <h1>Permission</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/galleries">Permission</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.permissions.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.misc.permissions_form', ["title" => "Add Permission", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
