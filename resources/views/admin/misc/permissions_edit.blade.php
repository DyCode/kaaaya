@extends('adminlte::page')

@section('title', 'Edit Permission')

@section('content_header')
    <h1>Permission</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/permissions">Permission</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  	<div class="row">
		  	<form class="" method="post" action="{!! route('admin.permissions.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
				<input type="hidden" name="_method" value="put" />
		  		@csrf
		  		@include('admin.misc.permissions_form', ["title" => "Edit Permission", 'edit' => true])
	  		</form>
	  	</div>
@stop

@section('js')

@stop