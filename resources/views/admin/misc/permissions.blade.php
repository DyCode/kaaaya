@extends('adminlte::page')

@section('title', 'Permissions')

@section('content_header')
    <h1>Permissions</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/galleries">Utility</a></li>
	    <li class="active">Permissions</li>
	 </ol>
@stop


@section('content')
	@include('admin.misc.alert')
	<div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
				<div class="box-header">
		            <h3 class="box-title">
		              <a href="{!! route('admin.permissions.create') !!}" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> Roles</a>

		            </h3>
		            <div class="box-tools">
		                <form>
			              	<div class="input-group input-group-sm" style="width: 300px;">
				                <input type="text" name="search" class="form-control pull-right" placeholder="Search">

				                <div class="input-group-btn">
				                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
				                </div>
			              	</div>
		                </form>
		            </div>
		        </div>
		        <div class="box-body table-responsive">
		          	<table class="table table-hover">
					  	<thead>
						  	<tr>
							    <th>#</th>
								<th width="30%">Roles</th>
								<th>Permission</th>
							    <th width="10%"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($roles as $i => $d)
							<tr>
								<td>{!! $i + 1 !!}</td>
								<td>{!! $d->name !!}</td>
								<td>
									@foreach(group_permission($d->permissions) as $group => $permissions)
										<div class="row">
											<h5 style="margin-bottom: 0;margin-top: 15px">{!! $group !!}</h5>
											<div class="row">
											@foreach($permissions as $permission)
												<div class="col-md-4"><small>{!! $permission['name'] !!}</small></div>
											@endforeach
											</div>
										</div>
									@endforeach
								</td>
								<td>
									<form method="post" action="{!! route('admin.permissions.destroy', ['id' => $d->id]) !!}">
							          <input type="hidden" name="_method" value="delete" />
							          @csrf
							        <div class="btn-group pull-right"><a href="{!! route('admin.permissions.edit', ['id' => $d->id]) !!}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a><button class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></button></div>
							        </form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop
