@php
	$permission_maps = $edit ? $data->permissions->map(function($d) { return $d->name; })->toArray() : [];
@endphp
<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
			<div class='form-group'>
				<label for=''>Name</label>
				<input class='form-control' type="text" name="name" value='{!! $edit ? $data->name : null !!}'>
			</div>

			<div class='form-group'>
				<label for=''>Permissions</label>
				@foreach(group_permission($permissions) as $group => $perm)
					<h5  style="margin-bottom: 0;margin-top: 15px"><input id="{!! str_replace(" ", "-", strtolower($group)) !!}" type="checkbox" class="check-all"> {!! $group !!}</h5>
					<div class="row">
						@foreach($perm as $permission)
							<div class="col-md-4">
								<small><input class="{!! str_replace(" ", "-", strtolower($group)) !!}" name="permission[{!! $permission['permission_name'] !!}]" type="checkbox" value="1" {!! in_array($permission['permission_name'], $permission_maps) ? 'CHECKED' : NULL !!}> {!! $permission['name'] !!}</small>
							</div>
						@endforeach
					</div>
				@endforeach
			</div>
				
			<button type="submit" class="btn btn-success">Submit</button>
    	</div>
  </div>
</div>
