@extends('adminlte::page')

@section('title', 'Edit Event Goods Donation')

@section('content_header')
    <h1>Event Goods Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/event_goods_donations">Event Goods Donation</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.event_goods_donations.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.event_goods_donations.form', ["title" => "Edit Event Goods Donation", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
