<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Event</label>
						<select name='event_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($events as $event)	
								<option value='{!! $event->id !!}' {!! $edit ? $event->id == $data->event_id ? 'SELECTED' : null : null !!}>{!! $event->title !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Consumer</label>
						<select name='consumer_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($consumers as $consumer)	
								<option value='{!! $consumer->id !!}' {!! $edit ? $consumer->id == $data->consumer_id ? 'SELECTED' : null : null !!}>{!! $consumer->name !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Goods Needed</label>
						<input name='goods_needed' type='file' />
						<input name='goods_needed' type='hidden' class='form-control' value='{!! $edit ? $data->goods_needed : null !!}' />
						@if($edit)
						<br><label>Preview</label><br>
						<img src='{!! $edit ? Storage::url($data->goods_needed) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
						@endif
				</div>
				<div class='form-group'>
						<label for=''>Amount Needed</label>
						<input name='amount_needed' type='radio' value='0' {!! $edit ? $data->amount_needed == '0' ? 'CHECKED' : null : null !!} /> No
						<input name='amount_needed' type='radio' value='1' {!! $edit ? $data->amount_needed == '1' ? 'CHECKED' : null : null !!} /> Yes
				</div>
				<div class='form-group'>
						<label for=''>Unit</label>
						<select name='unit_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($units as $unit)	
								<option value='{!! $unit->id !!}' {!! $edit ? $unit->id == $data->unit_id ? 'SELECTED' : null : null !!}>{!! $unit->unit !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Donation Date</label>
						<input name='donation_date' type='datetime-local' class='form-control' value='{!! $edit ? $data->donation_date ? $data->donation_date->format("Y-m-d\TH:i") : null : null !!}' />
				</div>
				<div class='form-group'>
						<label for=''>Amount Donated</label>
						<input name='amount_donated' type='radio' value='0' {!! $edit ? $data->amount_donated == '0' ? 'CHECKED' : null : null !!} /> No
						<input name='amount_donated' type='radio' value='1' {!! $edit ? $data->amount_donated == '1' ? 'CHECKED' : null : null !!} /> Yes
				</div>
				<div class='form-group'>
						<label for=''>Shipping method</label>
						<select name='shipping_method_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($shipping_methods as $shipping_method)	
								<option value='{!! $shipping_method->id !!}' {!! $edit ? $shipping_method->id == $data->shipping_method_id ? 'SELECTED' : null : null !!}>{!! $shipping_method->method !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Comment</label>
						<textarea name='comment'  class='form-control' rows='5'>{!! $edit ? $data->comment : null !!}</textarea>
				</div>
				<div class='form-group'>
						<label for=''>Verification Flag</label>
						<select name="verification_flag" class="form-control">
							<option value="0" {!! $edit ? $data->verification_flag == '0' ? 'SELECTED' : null : null !!}>Unverified</option>
							<option value="1" {!! $edit ? $data->verification_flag == '1' ? 'SELECTED' : null : null !!}>Verified</option>
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Active Flag</label>
						<select name="active_flag" class="form-control">
							<option value="0" {!! $edit ? $data->active_flag == '0' ? 'SELECTED' : null : null !!}>Inactive</option>
							<option value="1" {!! $edit ? $data->active_flag == '1' ? 'SELECTED' : null : null !!}>Active</option>
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
