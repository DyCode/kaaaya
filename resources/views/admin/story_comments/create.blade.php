@extends('adminlte::page')

@section('title', 'Create Story Comment')

@section('content_header')
    <h1>Story Comment</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/story_comments">Story Comment</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.story_comments.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.story_comments.form', ["title" => "Add Story Comment", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
