<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				<div class='form-group'>
						<label for=''>Story</label>
						<select name='story_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($stories as $story)	
								<option value='{!! $story->id !!}' {!! $edit ? $story->id == $data->story_id ? 'SELECTED' : null : null !!}>{!! $story->title !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Consumer</label>
							<select name='consumer_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($consumers as $consumer)	
								<option value='{!! $consumer->id !!}' {!! $edit ? $consumer->id == $data->consumer_id ? 'SELECTED' : null : null !!}>{!! $consumer->name !!}</option>
							@endforeach
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Comment</label>
						<textarea name='comment'  class='form-control' rows='5'>{!! $edit ? $data->comment : null !!}</textarea>
				</div>
				<div class='form-group'>
						<label for=''>Reply Flag</label>
						<input name='reply_flag' type='radio' value='0' {!! $edit ? $data->reply_flag == '0' ? 'CHECKED' : null : null !!} /> No
						<input name='reply_flag' type='radio' value='1' {!! $edit ? $data->reply_flag == '1' ? 'CHECKED' : null : null !!} /> Yes
				</div>
				<div class='form-group'>
						<label for=''>Verification Flag</label>
						<select name="verification_flag" class="form-control">
							<option value="0" {!! $edit ? $data->verification_flag == '0' ? 'SELECTED' : null : null !!}>Unverified</option>
							<option value="1" {!! $edit ? $data->verification_flag == '1' ? 'SELECTED' : null : null !!}>Verified</option>
						</select>
				</div>
				<div class='form-group'>
						<label for=''>Reply Header</label>
						
						<select name='reply_header_id'  class='form-control'>
							<option value='' >Select First</option>
							@foreach($headers as $header)	
								<option value='{!! $header->id !!}' {!! $edit ? $header->id == $data->reply_header_id ? 'SELECTED' : null : null !!}>{!! $header->comment !!}</option>
							@endforeach
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
