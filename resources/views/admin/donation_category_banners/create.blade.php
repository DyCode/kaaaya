@extends('adminlte::page')

@section('title', 'Create Donation Category Banner')

@section('content_header')
    <h1>Donation Category Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/donation_category_banners">Donation Category Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.donation_category_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.donation_category_banners.form', ["title" => "Add Donation Category Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
