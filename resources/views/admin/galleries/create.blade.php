@extends('adminlte::page')

@section('title', 'Create Gallery')

@section('content_header')
    <h1>Gallery</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/galleries">Gallery</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.galleries.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.galleries.form', ["title" => "Add Gallery", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
