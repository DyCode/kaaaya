@extends('adminlte::page')

@section('title', 'Edit Donation Banner')

@section('content_header')
    <h1>Donation Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/donation_banners">Donation Banner</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.donation_banners.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.donation_banners.form', ["title" => "Edit Donation Banner", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
