@extends('adminlte::page')

@section('title', 'Create Donation Banner')

@section('content_header')
    <h1>Donation Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/donation_banners">Donation Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.donation_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.donation_banners.form', ["title" => "Add Donation Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
