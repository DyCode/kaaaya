@extends('adminlte::page')

@section('title', 'Donation Banner')

@section('content_header')
    <h1>Donation Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/donation_banners">Donation Banner</a></li>
	    <li class="active">Lists</li>
	 </ol>
@stop


@section('content')
    @include('admin.misc.alert')
    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
					<div class="box-header">
            <h3 class="box-title">
              <a href="{!! route('admin.donation_banners.create') !!}" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> Donation Banner</a>

            </h3>
            <div class="box-tools">

                <form>
              <div class="input-group input-group-sm" style="width: 300px;">
                <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
                </form>
            </div>
          </div>
          <div class="box-body table-responsive">
          	<table class="table table-hover">
						  <tbody><tr>
						    <th>#</th>
								<th>Banner</th>
								<th>Active Flag</th>
								<th>Created</th>
								<th>Updated</th>
						    <th></th>
						  </tr>
						    @foreach($data as $i => $d)
						    <tr>
						      <td>{!! $i + $data->firstItem() !!}</td>
									<td><img src='{!! Storage::url($d->banner) !!}' width='150' onerror="this.src='/images/notfound.jpeg';" /></td>
									<td>{!! $d->active_flag ? '<span class=\'glyphicon glyphicon-ok-sign text-success\'></span>' : '<span class=\'glyphicon glyphicon-remove-sign text-danger\'></span>' !!}</td>
									<td>{!! optional($d->created_by_user)->name !!}</td>
									<td>{!! optional($d->updated_by_user)->name !!}</td>
						      <td>
						        <form method="post" action="{!! route('admin.donation_banners.destroy', ['id' => $d->id]) !!}">
						          <input type="hidden" name="_method" value="delete" />
						          @csrf
						        <div class="btn-group pull-right"><a href="{!! route('admin.donation_banners.edit', ['id' => $d->id]) !!}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a><button class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></button></div>
						        </form>
						      </td>
						    </tr>
						    @endforeach
						  </tbody>
						</table>
						{!! $data->render() !!}
          </div>
    		</div>
    	</div>

    </div>
@stop
