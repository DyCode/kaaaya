@extends('adminlte::page')

@section('title', 'Create Event Banner')

@section('content_header')
    <h1>Event Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/event_banners">Event Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.event_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.event_banners.form', ["title" => "Add Event Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
