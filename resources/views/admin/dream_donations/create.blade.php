@extends('adminlte::page')

@section('title', 'Create Dream Donation')

@section('content_header')
    <h1>Dream Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/dream_donations">Dream Donation</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.dream_donations.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.dream_donations.form', ["title" => "Add Dream Donation", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
