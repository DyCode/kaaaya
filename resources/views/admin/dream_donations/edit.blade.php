@extends('adminlte::page')

@section('title', 'Edit Dream Donation')

@section('content_header')
    <h1>Dream Donation</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/dream_donations">Dream Donation</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.dream_donations.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.dream_donations.form', ["title" => "Edit Dream Donation", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
