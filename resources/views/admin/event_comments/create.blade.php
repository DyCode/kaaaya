@extends('adminlte::page')

@section('title', 'Create Event Comment')

@section('content_header')
    <h1>Event Comment</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/event_comments">Event Comment</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.event_comments.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.event_comments.form', ["title" => "Add Event Comment", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
