@extends('adminlte::page')

@section('title', 'Create Dream Banner')

@section('content_header')
    <h1>Dream Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/dream_banners">Dream Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.dream_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.dream_banners.form', ["title" => "Add Dream Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
