@extends('adminlte::page')

@section('title', 'Edit Story Banner')

@section('content_header')
    <h1>Story Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/story_banners">Story Banner</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.story_banners.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.story_banners.form', ["title" => "Edit Story Banner", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
