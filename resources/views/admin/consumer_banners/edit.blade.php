@extends('adminlte::page')

@section('title', 'Edit Consumer Banner')

@section('content_header')
    <h1>Consumer Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/consumer_banners">Consumer Banner</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.consumer_banners.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.consumer_banners.form', ["title" => "Edit Consumer Banner", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
