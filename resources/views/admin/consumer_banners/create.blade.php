@extends('adminlte::page')

@section('title', 'Create Consumer Banner')

@section('content_header')
    <h1>Consumer Banner</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/consumer_banners">Consumer Banner</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.consumer_banners.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.consumer_banners.form', ["title" => "Add Consumer Banner", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
