@extends('adminlte::page')

@section('title', 'Edit Event Label')

@section('content_header')
    <h1>Event Label</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/event_labels">Event Label</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.event_labels.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.event_labels.form', ["title" => "Edit Event Label", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
