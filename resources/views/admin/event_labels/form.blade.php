<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
			<div class="box-body">
				<div class='form-group'>
				@foreach(config('translatable.locales') as $lang)

				<div class="form-group">
					<label for=""><img src="/images/flags/{!! $lang !!}.png" alt="{!! $lang !!}" width="16" style="margin-right: 10px">Label</label>
					<input name="{!! $lang !!}[label]"  class="form-control" rows="5" value="{!! $edit ? $data->{'label:'.$lang} : null !!}" />
				</div>

				@endforeach
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>
