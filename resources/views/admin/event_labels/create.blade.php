@extends('adminlte::page')

@section('title', 'Create Event Label')

@section('content_header')
    <h1>Event Label</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/event_labels">Event Label</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.event_labels.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.event_labels.form', ["title" => "Add Event Label", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
