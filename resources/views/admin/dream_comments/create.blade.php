@extends('adminlte::page')

@section('title', 'Create Dream Comment')

@section('content_header')
    <h1>Dream Comment</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/dream_comments">Dream Comment</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.dream_comments.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.dream_comments.form', ["title" => "Add Dream Comment", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
