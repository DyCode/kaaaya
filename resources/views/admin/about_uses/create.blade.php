@extends('adminlte::page')

@section('title', 'Create About Us')

@section('content_header')
    <h1>About Us</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/about_uses">About Us</a></li>
	    <li class="active">Create</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.about_uses.store') !!}" enctype="multipart/form-data">
	  		@csrf
	  		@include('admin.about_uses.form', ["title" => "Add About Us", 'edit' => false])
  		</form>
	  </div>
@stop

@section('js')

@stop
