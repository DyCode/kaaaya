@extends('adminlte::page')

@section('title', 'Edit About Us')

@section('content_header')
    <h1>About Us</h1>
    <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="/admin/about_uses">About Us</a></li>
	    <li class="active">Edit</li>
	 </ol>
@stop
@section('content')
		@include('admin.misc.alert')
	  <div class="row">
	  	<form class="" method="post" action="{!! route('admin.about_uses.update', ['id' => $data->id]) !!}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="put" />
	  		@csrf
	  		@include('admin.about_uses.form', ["title" => "Edit About Us", 'edit' => true])
  		</form>
	  </div>
@stop

@section('js')

@stop
