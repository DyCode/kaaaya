<div class="col-md-8">
	<div class="box box-primary">
		<div class="box-header">
      <h3 class="box-title">
				{!! $title !!}
      </h3>
    </div>
		<div class="box-body">
				
				@foreach(config('translatable.locales') as $lang)
				<div class="form-group">
					<label for=""><img src="/images/flags/{!! $lang !!}.png" alt="{!! $lang !!}" width="16" style="margin-right: 10px">Description</label>
					<textarea name="{!! $lang !!}[description]"  class="form-control" rows="5">{!! $edit ? $data->{'description:'.$lang} : null !!}</textarea>
				</div>

				<div class="form-group">
					<label for=""><img src="/images/flags/{!! $lang !!}.png" alt="{!! $lang !!}" width="16" style="margin-right: 10px">Who We Are</label>
					<textarea name="{!! $lang !!}[who_we_are_description]"  class="form-control" rows="5">{!! $edit ? $data->{'who_we_are_description:'.$lang} : null !!}</textarea>
				</div>

				@endforeach


				
				<div class='form-group'>
						<label for=''>Active Flag</label>
						<select name="active_flag" class="form-control">
							<option value="0" {!! $edit ? $data->active_flag == '0' ? 'SELECTED' : null : null !!}>Inactive</option>
							<option value="1" {!! $edit ? $data->active_flag == '1' ? 'SELECTED' : null : null !!}>Active</option>
						</select>
				</div>
			<button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>
</div>

<div class="col-md-4">
	<div class="box box-primary">
		<div class="box-header">
	      <h3 class="box-title">
					
	      </h3>
	    </div>
	    <div class="box-body">
			<div class='form-group'>
					<label for=''>Banner</label>
					<input name='banner' type='file' onchange="return readURL(this, '#banner-preview')"/>
					<input name='banner' type='hidden' class='form-control' value='{!! $edit ? $data->banner : null !!}' />
					@if($edit)
					<br><label>Preview</label><br>
					<img id="banner-preview" src='{!! $edit ? Storage::url($data->banner) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
					@endif
			</div>
			<div class='form-group'>
					<label for=''>Image</label>
					<input name='image' type='file' onchange="return readURL(this, '#image-preview')"/>
					<input name='image' type='hidden' class='form-control' value='{!! $edit ? $data->image : null !!}' />
					@if($edit)
					<br><label>Preview</label><br>
					<img id="image-preview" src='{!! $edit ? Storage::url($data->image) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
					@endif
			</div>
			<div class='form-group'>
					<label for=''>Who We Are Image</label>
					<input name='who_we_are_image' type='file' onchange="return readURL(this, '#who_we_are_image-preview')"/>
					<input name='who_we_are_image' type='hidden' class='form-control' value='{!! $edit ? $data->who_we_are_image : null !!}' />
					@if($edit)
					<br><label>Preview</label><br>
					<img id="who_we_are_image-preview" src='{!! $edit ? Storage::url($data->who_we_are_image) : "/images/notfound.jpeg" !!}' width='300' onerror="this.src='/images/notfound.jpeg';" />
					@endif
			</div>
		</div>
	</div>

</div>
