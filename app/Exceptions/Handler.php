<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {   
        if (preg_match('/api\/v1/', $request->getPathInfo())) {
            $message = "";
            $meta = [];
            $code = 500;
            if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
                $message = $exception->getMessage();
                $code = 401;
            } else if ($exception instanceof \Illuminate\Validation\ValidationException) {
                $message = $exception->getMessage();
                $meta = ['error_validation' => $exception->errors()];
                $code = 400;
            } else {
                dd($exception);
                $message = "unknown error";
                $code = 500;
            }
            return response()->json([
                'status' => 'error',
                'message' => $message,
                'meta' => $meta,
            ], $code);
        }
        
        return parent::render($request, $exception);
    }
}
