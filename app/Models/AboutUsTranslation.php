<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUsTranslation extends Model
{

    protected $table = 'about_us_translations';
    public $timestamps = false; 
    protected $fillable = [
		'about_us_id',
		'description',
		'who_we_are_description',
		'locale',
	];
    
}