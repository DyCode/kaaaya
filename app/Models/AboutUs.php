<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AboutUs extends Model
{
    use SoftDeletes;
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = [
        'description',
        'who_we_are_description',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'about_uses';

    

    protected $casts = [
		'banner' => 'string',
		'image' => 'string',
		'who_we_are_image' => 'string',
		'active_flag' => 'boolean',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'banner',
		'image',
		'who_we_are_image',
		'active_flag',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->whereHas('translations', function ($query) use ($value) {
                $query->where('description', 'like', '%' . $value . '%');
                $query->orWhere('who_we_are_description', 'like', '%' . $value . '%');
            });
        });
    }
}