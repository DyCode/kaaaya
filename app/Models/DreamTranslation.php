<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DreamTranslation extends Model
{
    
    public $timestamps = false; 
    protected $table = 'dream_translations';
    protected $fillable = [
		'dream_id',
		'title',
		'description',
		'locale',
	];

}