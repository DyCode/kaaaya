<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Gallery extends Model
{
    use SoftDeletes;
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = [
        'title',
        'description',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'galleries';

    

    protected $casts = [
		'donation_category_id' => 'string',
		'image' => 'string',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'donation_category_id',
		'image',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
        $class::observe(new \App\Observers\ConsumerCreatedUpdated);
	}

    
	public function consumer()
	{
		return $this->belongsTo(Consumer::class);
	}
	public function donation_category()
	{
		return $this->belongsTo(DonationCategory::class);
	}
    
    public function created_by_user()
    {
        return $this->belongsTo(\App\User::class, 'created_by', 'id');
    }
    public function updated_by_user()
    {
        return $this->belongsTo(\App\User::class, 'updated_by', 'id');
    }

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->whereHas('translations', function ($query) use ($value) {
                $query->where('title', 'like', '%' . $value . '%');
                $query->orWhere('description', 'like', '%' . $value . '%');
            });
        });
    }
}