<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DonationCategory extends Model
{
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'donation_categories';

    

    protected $casts = [
		'category' => 'string',
		'category_icon' => 'string',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'category',
		'category_icon',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
	public function dreams()
	{
		return $this->hasMany(Dream::class);
	}

	public function events()
	{
		return $this->hasMany(Event::class);
	}
	public function galleries()
	{
		return $this->hasMany(Gallery::class);
	}
	public function banners()
	{
		return $this->hasMany(DonationCategoryBanner::class);
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->where('category', 'like', '%' . $value . '%');
			$query->orWhere('category_icon', 'like', '%' . $value . '%');
        });
    }
}