<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicyTranslation extends Model
{
    public $timestamps = false; 
    protected $table = 'privacy_policy_translations';
    protected $fillable = [
		'privacy_policy_id',
		'description',
		'locale',
	];
}