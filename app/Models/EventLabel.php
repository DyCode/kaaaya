<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventLabel extends Model
{
    use SoftDeletes;
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = [
        'label',
    ];

    protected $table = 'event_labels';

    

    protected $casts = [
		'label' => 'string',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


    public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
        $class::observe(new \App\Observers\ConsumerCreatedUpdated);
	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->whereHas('translations', function ($query) use ($value) {
                $query->where('label', 'like', '%' . $value . '%');
            });
        });
    }

}
