<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;


class Consumer extends Authenticatable implements JWTSubject
{
    use SoftDeletes;
    use Notifiable;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'consumers';

    

    protected $casts = [
		'profile_image' => 'string',
		'name' => 'string',
		'password' => 'string',
		'email' => 'string',
		'phone_number' => 'string',
		'birth_date' => 'datetime',
		'gender' => 'string',
		'home_address' => 'string',
		'office_address' => 'string',
		'institution_name' => 'string',
		'active_flag' => 'boolean',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
		'birth_date',
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'profile_image',
		'name',
		'password',
		'email',
		'phone_number',
		'birth_date',
		'gender',
		'home_address',
		'office_address',
		'active_flag',
		'institution_name',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
    
	public function events()
	{
		return $this->hasMany(\App\Models\Event::class);
	}

	public function event_donations()
	{
		return $this->hasMany(\App\Models\EventDonation::class);
	}

	public function dreams()
	{
		return $this->hasMany(\App\Models\Dream::class);
	}

	public function dream_donations()
	{
		return $this->hasMany(\App\Models\DreamDonation::class);
	}

	public function stories()
	{
		return $this->hasMany(\App\Models\Story::class);
	}


	public function story_donations()
	{
		return $this->hasMany(\App\Models\StoryDonation::class);
	}



	public function event_comments()
	{
		return $this->hasMany(\App\Models\EventComment::class);
	}

	public function dream_comments()
	{
		return $this->hasMany(\App\Models\DreamComment::class);
	}

	public function story_comments()
	{
		return $this->hasMany(\App\Models\StoryComment::class);
	}

	public function gallery_comments()
	{
		return $this->hasMany(\App\Models\GalleryComment::class);
	}


    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->where('profile_image', 'like', '%' . $value . '%');
			$query->orWhere('name', 'like', '%' . $value . '%');
			$query->orWhere('password', 'like', '%' . $value . '%');
			$query->orWhere('email', 'like', '%' . $value . '%');
			$query->orWhere('phone_number', 'like', '%' . $value . '%');
			$query->orWhere('birth_date', 'like', '%' . $value . '%');
			$query->orWhere('gender', 'like', '%' . $value . '%');
			$query->orWhere('home_address', 'like', '%' . $value . '%');
			$query->orWhere('office_address', 'like', '%' . $value . '%');
        });
    }

     public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getFullNameAttribute()
    {
    	return $this->name;
    }
}