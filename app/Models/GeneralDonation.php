<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class GeneralDonation extends Model
{
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'general_donations';

    

    protected $casts = [
		'consumer_id' => 'string',
		'date' => 'datetime',
		'amount' => 'float',
		'transfer_slip' => 'string',
		'comment' => 'string',
		'verification_flag' => 'boolean',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
		'date',
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'consumer_id',
		'date',
		'amount',
		'transfer_slip',
		'comment',
		'verification_flag',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\ConsumerCreatedUpdated);
		$class::observe(new \App\Observers\CreatedUpdatedBy);
	}

    
	public function consumer()
	{
		return $this->belongsTo(Consumer::class);
	}
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->orWhere('date', 'like', '%' . $value . '%');
			$query->orWhere('transfer_slip', 'like', '%' . $value . '%');
			$query->orWhere('comment', 'like', '%' . $value . '%');
        });
    }
}