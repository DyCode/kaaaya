<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DreamGoodsDonation extends Model
{
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dream_goods_donations';

    

    protected $casts = [
		'dream_id' => 'string',
		'consumer_id' => 'string',
		'goods_needed' => 'string',
		'amount_needed' => 'boolean',
		'unit_id' => 'string',
		'donation_date' => 'datetime',
		'amount_donated' => 'boolean',
		'shipping_method_id' => 'string',
		'comment' => 'string',
		'verification_flag' => 'boolean',
		'created_by' => 'string',
		'updated_by' => 'string',
		'active_flag' => 'boolean',
    ];

    protected $dates = [
		'donation_date',
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'dream_id',
		'consumer_id',
		'goods_needed',
		'amount_needed',
		'unit_id',
		'donation_date',
		'amount_donated',
		'shipping_method_id',
		'comment',
		'verification_flag',
		'created_by',
		'updated_by',
		'active_flag',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
		$class::observe(new \App\Observers\ConsumerCreatedUpdated);
		
	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
	public function dream()
	{
		return $this->belongsTo(Dream::class);
	}
	public function consumer()
	{
		return $this->belongsTo(Consumer::class);
	}
	public function unit()
	{
		return $this->belongsTo(Unit::class);
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->orWhere('goods_needed', 'like', '%' . $value . '%');
			$query->orWhere('donation_date', 'like', '%' . $value . '%');
			$query->orWhere('comment', 'like', '%' . $value . '%');
        });
    }
}