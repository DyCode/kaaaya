<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryTranslation extends Model
{
    public $timestamps = false; 
    protected $table = 'gallery_translations';
    protected $fillable = [
		'dream_id',
		'title',
		'description',
		'locale',
	];
}