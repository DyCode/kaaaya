<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Faq extends Model
{
    use SoftDeletes;
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = [
        'question',
        'answer',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faqs';

    

    protected $casts = [
		'faq_category_id' => 'string',
		'image' => 'string',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'faq_category_id',
		'image',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
	public function faq_category()
	{
		return $this->belongsTo(FaqCategory::class);
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->whereHas('translations', function ($query) use ($value) {
                $query->where('question', 'like', '%' . $value . '%');
                $query->orWhere('answer', 'like', '%' . $value . '%');
            });
        });
    }
}