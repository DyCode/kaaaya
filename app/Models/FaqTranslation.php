<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqTranslation extends Model
{
    public $timestamps = false; 
    protected $table = 'faq_translations';
    protected $fillable = [
		'faq_id',
		'question',
		'answer',
		'locale',
	];

}