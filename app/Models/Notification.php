<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Notification extends Model
{
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    

    protected $casts = [
		'consumer_id' => 'string',
		'date' => 'datetime',
		'notification' => 'string',
		'is_read' => 'boolean',
		'is_sent' => 'boolean',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
		'date',
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'consumer_id',
		'date',
		'notification',
		'is_read',
		'is_sent',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
	}

    
	public function consumer()
	{
		return $this->belongsTo(Consumer::class);
	}
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->orWhere('date', 'like', '%' . $value . '%');
			$query->orWhere('notification', 'like', '%' . $value . '%');
        });
    }
}