<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventLabelTranslation extends Model
{
    protected $table = 'event_label_translations';
    public $timestamps = false; 
    protected $fillable = [
		'label',
		'locale',
	];
}
