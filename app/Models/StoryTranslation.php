<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoryTranslation extends Model
{
    
    public $timestamps = false; 
    protected $table = 'story_translations';
    protected $fillable = [
		'story_id',
		'title',
		'description',
		'locale',
	];
}