<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class StoryComment extends Model
{
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'story_comments';

    

    protected $casts = [
		'story_id' => 'string',
		'consumer_id' => 'string',
		'comment' => 'string',
		'reply_flag' => 'boolean',
		'verification_flag' => 'boolean',
		'reply_header_id' => 'string',
		'created_by' => 'string',
		'updated_by' => 'string',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'story_id',
		'consumer_id',
		'comment',
		'reply_flag',
		'verification_flag',
		'reply_header_id',
		'created_by',
		'updated_by',
	];

    
    
	public static function boot()
	{
		parent::boot();
		$class = get_called_class();
		$class::observe(new \App\Observers\CreatedUpdatedBy);
        $class::observe(new \App\Observers\ConsumerCreatedUpdated);

	}

    
	public function created_by_user()
	{
		return $this->belongsTo(\App\User::class, 'created_by', 'id');
	}
	public function updated_by_user()
	{
		return $this->belongsTo(\App\User::class, 'updated_by', 'id');
	}
	public function header()
	{
		return $this->belongsTo(StoryComment::class, 'reply_header_id', 'id');
	}

	public function replies()
	{
		return $this->hasMany(StoryComment::class, 'reply_header_id' , 'id' );
	} 

	public function story()
	{
		return $this->belongsTo(Story::class);
	}
	public function consumer()
	{
		return $this->belongsTo(Consumer::class);
	}
    

    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
			$query->orWhere('comment', 'like', '%' . $value . '%');
        });
    }
}