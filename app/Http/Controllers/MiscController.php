<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiscController extends Controller
{
    //
    public function setlocale($locale)
    {
        if (in_array($locale, \Config::get('translatable.locales'))) {
            // dd($locale);
            \Session::put('applocale', $locale);
        }

        return back();
    }
}
