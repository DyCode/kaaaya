<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



/**
 * @OA\SecurityScheme(
 *     securityScheme="bearerAuth",
 *     name="Authorization",
 *     in="header",
 *     type="apiKey",
 *     description="Enter your bearer token in the format **Bearer &lt;token>**",
 * )
 */
class ApiController extends Controller
{
    //
}
