<?php

namespace App\Http\Controllers\Api;

use App\Models\StoryBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryBannerApiCreateRequest;
use App\Http\Requests\StoryBannerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\StoryBanner as StoryBannerSingle;
use App\Http\Resources\StoryBannerCollection;

class StoryBannerController extends Controller
{
    public $story_banners;

    public function __construct(StoryBanner $story_banners)
    {
        $this->story_banners = $story_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/story_banners",
     *   tags={"Banner"},
     *   summary="Get Story Banners",
     *   description="Get Story Banners",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new StoryBannerSingle($this->story_banners->where('active_flag', true)->orderBy('created_at', 'desc')->firstOrFail());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryBannerApiCreateRequest $request)
    {
        abort(404);
        // try {
        //     $this->story_banners->create($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
 
    public function show($id)
    {
        abort(404);
        // return new StoryBannerSingle($this->story_banners->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryBannerApiUpdateRequest $request, $id)
    {
        $this->story_banners = $this->story_banners->findOrFail($id);
        try {
            $this->story_banners->update($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->story_banners->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
