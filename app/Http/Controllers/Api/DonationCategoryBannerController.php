<?php

namespace App\Http\Controllers\Api;

use App\Models\DonationCategoryBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationCategoryBannerApiCreateRequest;
use App\Http\Requests\DonationCategoryBannerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DonationCategoryBanner as DonationCategoryBannerSingle;
use App\Http\Resources\DonationCategoryBannerCollection;


class DonationCategoryBannerController extends Controller
{
    public $donation_category_banners;

    public function __construct(DonationCategoryBanner $donation_category_banners)
    {
        $this->donation_category_banners = $donation_category_banners;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/donation_category_banners",
     *   tags={"Banner"},
     *   summary="Get Donation Category Banners",
     *   description="Get Donation Category Banners",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return new DonationCategoryBannerSingle($this->donation_category_banners->where('active_flag', true)->orderBy('created_at', 'desc')->firstOrFail());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationCategoryBannerApiCreateRequest $request)
    {
        abort(404);
    }

   
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonationCategoryBannerApiUpdateRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
