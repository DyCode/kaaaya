<?php

namespace App\Http\Controllers\Api;

use App\Models\DreamBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamBannerApiCreateRequest;
use App\Http\Requests\DreamBannerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DreamBanner as DreamBannerSingle;
use App\Http\Resources\DreamBannerCollection;

class DreamBannerController extends Controller
{
    public $dream_banners;

    public function __construct(DreamBanner $dream_banners)
    {
        $this->dream_banners = $dream_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dream_banners",
     *   tags={"Banner"},
     *   summary="Get Dream Banners",
     *   description="Get Dream Banners",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new DreamBannerSingle($this->dream_banners->where('active_flag', true)->orderBy('created_at', 'desc')->firstOrFail());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamBannerApiCreateRequest $request)
    {
        abort(404);
        // try {
        //     $this->dream_banners->create($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }


    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamBannerApiUpdateRequest $request, $id)
    {
        $this->dream_banners = $this->dream_banners->findOrFail($id);
        try {
            $this->dream_banners->update($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->dream_banners->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
