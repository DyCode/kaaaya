<?php

namespace App\Http\Controllers\Api;

use App\Models\StoryDonation;
use App\Models\Story;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryDonationApiCreateRequest;
use App\Http\Requests\StoryDonationApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\StoryDonation as StoryDonationSingle;
use App\Http\Resources\StoryDonationCollection;
use App\Notifications\ContentCreated;
use DB;

class StoryDonationController extends Controller
{
    public $story_donations;

    public function __construct(StoryDonation $story_donations, Story $stories)
    {
        $this->story_donations = $story_donations;
        $this->stories = $stories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/stories/{id}/donations",
     *   tags={"Story"},
     *   summary="Get Donations for story",
     *   description="Get Donations for story",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $story_id)
    {
        $story = $this->stories->findOrFail($story_id);
        return new StoryDonationCollection($story->donations()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\POST(path="/api/v1/stories/{id}/donations",
     *   tags={"Story"},
     *   summary="Post Donation for Story",
     *   description="Post Donation for Story",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"date", "amount"},
     *                 @OA\Property(
     *                     property="date",
     *                     description="Donation Date",
     *                     type="date",
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     description="Donation Amount",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Donation Comment",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(StoryDonationApiCreateRequest $request, $story_id)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $input['story_id'] = $story_id;
            if($request->hasFile('transfer_slip')) {
                $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
                $input['transfer_slip'] = $path;
            }
            $input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
            $story_donation = $this->story_donations->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($story_donation, 'story_donation'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/stories/{id}/donations/{donation_id}/receipt",
     *   tags={"Story"},
     *   summary="Post Donation Receipt for Story",
     *   description="Post Donation Receipt for Story",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"transfer_slip"},
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function receipt(StoryDonationApiUpdateRequest $request, $story_id, $id)
    {
        $input = $request->all();
        DB::beginTransaction();
        // dd(auth('api')->user()->id);
        try {
            $story_donation = $this->story_donations->findOrFail($id);
            if ($story_donation->consumer_id != auth('api')->user()->id) {
                throw new \Exception("the Donation does not belong to the user");
            }
            if ($story_donation->verification_flag) {
                throw new \Exception("the Donation has been verified, cannot edited");
            }
            $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
            $input['transfer_slip'] = $path;
            $story_donation->update($input);

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new StoryDonationSingle($this->story_donations->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryDonationApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->story_donations = $this->story_donations->findOrFail($id);
        // try {
        //     $this->story_donations->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->story_donations->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
