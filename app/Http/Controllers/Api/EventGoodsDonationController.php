<?php

namespace App\Http\Controllers\Api;

use App\Models\EventGoodsDonation;
use App\Models\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventGoodsDonationApiCreateRequest;
use App\Http\Requests\EventGoodsDonationApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\EventGoodsDonation as EventGoodsDonationSingle;
use App\Http\Resources\EventGoodsDonationCollection;
use App\Notifications\ContentCreated;
use DB;

class EventGoodsDonationController extends Controller
{
    public $event_goods_donations;
    public $events;

    public function __construct(EventGoodsDonation $event_goods_donations, Event $events)
    {
        $this->event_goods_donations = $event_goods_donations;
        $this->events = $events;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events/{id}/goods_donations",
     *   tags={"Event"},
     *   summary="Get Goods Donations for event",
     *   description="Get Goods Donations for event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $event_id)
    {
        $event = $this->events->findOrFail($event_id);
        return new EventGoodsDonationCollection($event->goods_donations()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/events/{id}/goods_donations",
     *   tags={"Event"},
     *   summary="Post Goods Donation for Event",
     *   description="Post Goods Donation for Event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"goods_needed","amount_needed", "unit_id", "donation_date", "amount_donated", "shipping_method_id"},
     *                 @OA\Property(
     *                     property="goods_needed",
     *                     description="Goods Needed",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="amount_needed",
     *                     description="Amount Needed",
     *                     enum={"1", "0"},
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="unit_id",
     *                     description="Unit ID",
     *                     type="number",
     *                 ),
     *                 @OA\Property(
     *                     property="donation_date",
     *                     description="Donation Date",
     *                     type="date",
     *                 ),
     *                 @OA\Property(
     *                     property="amount_donated",
     *                     description="Amount Donated",
     *                     enum={"1", "0"},
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="shipping_method_id",
     *                     description="Shipping Method ID",
     *                     type="number",
     *                 ),
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Donation Comment",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(EventGoodsDonationApiCreateRequest $request, $event_id)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $input['event_id'] = $event_id;
           
            $input['donation_date'] = \Carbon\Carbon::parse($input['donation_date'])->format('Y-m-d H:i:s');
            $event_goods_donation = $this->event_goods_donations->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($event_goods_donation, 'event_goods_donation'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events/{id}/goods_donations/{good_donation_id}",
     *   tags={"Event"},
     *   summary="Get Goods Donation for event",
     *   description="Get Goods Donation for event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Parameter(
     *     name="good_donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Goods Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($event_id, $id)
    {
        return new EventGoodsDonationSingle($this->event_goods_donations->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventGoodsDonationApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->event_goods_donations = $this->event_goods_donations->findOrFail($id);
        // try {
        //     $this->event_goods_donations->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        
        // try {
        //     $this->event_goods_donations->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
