<?php

namespace App\Http\Controllers\Api;

use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConsumerApiCreateRequest;
use App\Http\Requests\ConsumerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\Consumer as ConsumerSingle;
use App\Http\Resources\ConsumerCollection;

class ConsumerController extends Controller
{
    public $consumers;

    public function __construct(Consumer $consumers)
    {
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort(404);
        // return ConsumerCollection::collection($this->consumers->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConsumerApiCreateRequest $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConsumerApiUpdateRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
