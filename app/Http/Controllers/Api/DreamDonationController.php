<?php

namespace App\Http\Controllers\Api;

use App\Models\DreamDonation;
use App\Models\Dream;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamDonationApiCreateRequest;
use App\Http\Requests\DreamDonationApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DreamDonation as DreamDonationSingle;
use App\Http\Resources\DreamDonationCollection;
use App\Notifications\ContentCreated;
use DB;

class DreamDonationController extends Controller
{
    public $dream_donations;

    public function __construct(DreamDonation $dream_donations, Dream $dreams)
    {
        $this->dream_donations = $dream_donations;
        $this->dreams = $dreams;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}/donations",
     *   tags={"Dream"},
     *   summary="Get Donations for dream",
     *   description="Get Donations for dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $dream_id)
    {
        $dream = $this->dreams->findOrFail($dream_id);
        return new DreamDonationCollection($dream->donations()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/dreams/{id}/donations",
     *   tags={"Dream"},
     *   summary="Post Donation for Dream",
     *   description="Post Donation for Dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"date", "amount"},
     *                 @OA\Property(
     *                     property="date",
     *                     description="Donation Date",
     *                     type="date",
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     description="Donation Amount",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Donation Comment",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(DreamDonationApiCreateRequest $request, $dream_id)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $input['dream_id'] = $dream_id;
            if($request->hasFile('transfer_slip')) {
                $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
                $input['transfer_slip'] = $path;
            }
            $input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
            $dream_donation = $this->dream_donations->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($dream_donation, 'dream_donation'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/dreams/{id}/donations/{donation_id}/receipt",
     *   tags={"Dream"},
     *   summary="Post Donation Receipt for Dream",
     *   description="Post Donation Receipt for Dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"transfer_slip"},
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function receipt(DreamDonationApiUpdateRequest $request, $dream_id, $id)
    {
        $input = $request->all();
        DB::beginTransaction();
        // dd(auth('api')->user()->id);
        try {
            $dream_donation = $this->dream_donations->findOrFail($id);
            if ($dream_donation->consumer_id != auth('api')->user()->id) {
                throw new \Exception("the Donation does not belong to the user");
            }
            if ($dream_donation->verification_flag) {
                throw new \Exception("the Donation has been verified, cannot edited");
            }
            $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
            $input['transfer_slip'] = $path;
            $dream_donation->update($input);

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}/donations/{donation_id}",
     *   tags={"Dream"},
     *   summary="Get Donation for dream",
     *   description="Get Donation for dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($id)
    {
        return new DreamDonationSingle($this->dream_donations->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamDonationApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->dream_donations = $this->dream_donations->findOrFail($id);
        // try {
        //     $this->dream_donations->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->dream_donations->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
