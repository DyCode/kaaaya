<?php

namespace App\Http\Controllers\Api;

use App\Models\DreamComment;
use App\Models\Dream;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamCommentApiCreateRequest;
use App\Http\Requests\DreamCommentApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DreamComment as DreamCommentSingle;
use App\Http\Resources\DreamCommentCollection;

class DreamCommentController extends Controller
{
    public $dream_comments;

    public function __construct(DreamComment $dream_comments, Dream $dreams)
    {
        $this->dream_comments = $dream_comments;
        $this->dreams = $dreams;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}/comments",
     *   tags={"Dream"},
     *   summary="Get Comments for dream",
     *   description="Get Comments for dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $dream_id)
    {
        $dream = $this->dreams->findOrFail($dream_id);
        return new DreamCommentCollection($dream->comments()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\POST(path="/api/v1/dreams/{id}/comments",
     *   tags={"Dream"},
     *   summary="Post Comment for Dream",
     *   description="Post Comment for Dream",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Comment",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(DreamCommentApiCreateRequest $request, $dream_id)
    {
        $input = $request->all();

        try {
            $input['dream_id'] = $dream_id;
            $this->dream_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/dreams/{id}/comments/{comment_id}/reply",
     *   tags={"Dream"},
     *   summary="Post Reply for Comment",
     *   description="Post Reply for Comment",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Reply",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function reply(DreamCommentApiCreateRequest $request, $dream_id, $id)
    {
        $input = $request->all();

        try {
            $input['dream_id'] = null;
            $input['reply_header_id'] = $id;
            $this->dream_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}/comments/{comment_id}",
     *   tags={"Dream"},
     *   summary="Get Detail Comment for dream",
     *   description="Get Detail Comment for dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($event_id, $id)
    {
        return new DreamCommentSingle($this->dream_comments->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamCommentApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->dream_comments = $this->dream_comments->findOrFail($id);
        // try {
        //     $this->dream_comments->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->dream_comments->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
