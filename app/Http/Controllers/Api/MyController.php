<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventCollection;
use App\Http\Resources\EventDonationCollection;
use App\Http\Resources\EventCommentCollection;
use App\Http\Resources\Event as EventSingle;
use App\Http\Resources\DreamCollection;
use App\Http\Resources\DreamDonationCollection;
use App\Http\Resources\DreamCommentCollection;
use App\Http\Resources\Dream as DreamSingle;
use App\Http\Resources\StoryCollection;
use App\Http\Resources\Story as StorySingle;
use App\Http\Resources\GalleryCommentCollection;
use App\Http\Resources\Consumer;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileImageUploadRequest;
use App\Http\Requests\UpdateProfileRequest;

class MyController extends ApiController
{
	function __construct($foo = null)
	{
		$this->my = auth('api')->user();
	}
	
	/**
     * @OA\GET(path="/api/v1/my",
     *   tags={"My"},
     *   summary="Get My Profile",
     *   description="Get My Profile",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
	public function index()
	{
		return new Consumer($this->my);
	}

	/**
     * @OA\GET(path="/api/v1/my/events",
     *   tags={"My"},
     *   summary="Get My Events",
     *   description="Get My Events",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
	public function events()
	{
		return new EventCollection($this->my->events()->paginate());
	}

	/**
     * @OA\GET(path="/api/v1/my/events/{id}",
     *   tags={"My"},
     *   summary="Get My Event",
     *   description="Get My Event",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID My Event",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function event($id)
     {
          return new EventSingle($this->my->events()->findOrFail($id));
     }

     /**
     * @OA\GET(path="/api/v1/my/donations/events",
     *   tags={"My"},
     *   summary="Get My Donations for Events",
     *   description="Get My Donations for Events",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function event_donations()
     {
          return new EventDonationCollection($this->my->event_donations()->paginate());
     }

     


     /**
     * @OA\GET(path="/api/v1/my/dreams",
     *   tags={"My"},
     *   summary="Get My Dreams",
     *   description="Get My Dreams",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function dreams()
     {
          return new DreamCollection($this->my->dreams()->paginate());
     }


     /**
     * @OA\GET(path="/api/v1/my/dreams/{id}",
     *   tags={"My"},
     *   summary="Get My Dreams",
     *   description="Get My Dreams",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID My Dreams",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function dream($id)
     {
          return new DreamsSingle($this->my->dreams()->findOrFail($id));
     }

     /**
     * @OA\GET(path="/api/v1/my/donations/dreams",
     *   tags={"My"},
     *   summary="Get My Donations for Dreams",
     *   description="Get My Donations for Dreams",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function dream_donations()
     {
          return new DreamDonationCollection($this->my->dream_donations()->paginate());
     }
     /**
     * @OA\GET(path="/api/v1/my/donations/stories",
     *   tags={"My"},
     *   summary="Get My Donations for Stories",
     *   description="Get My Donations for Stories",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function story_donations()
     {
          return new DreamDonationCollection($this->my->story_donations()->paginate());
     }

     /**
     * @OA\GET(path="/api/v1/my/stories",
     *   tags={"My"},
     *   summary="Get My Stories",
     *   description="Get My Stories",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function stories()
     {
          return new StoryCollection($this->my->stories()->paginate());
     }

     /**
     * @OA\GET(path="/api/v1/my/stories/{id}",
     *   tags={"My"},
     *   summary="Get My Story",
     *   description="Get My Story",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID My Story",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function story($id)
     {
          return new StorySingle($this->my->stories()->findOrFail($id));
     }

    
     /**
     * @OA\GET(path="/api/v1/my/comments/events",
     *   tags={"My"},
     *   summary="Get My Comments for Events",
     *   description="Get My Comments for Events",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function event_comments()
     {
          return new EventCommentCollection($this->my->event_comments()->paginate());
     }

     /**
     * @OA\GET(path="/api/v1/my/comments/dreams",
     *   tags={"My"},
     *   summary="Get My Comments for Dreams",
     *   description="Get My Comments for Dreams",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function dream_comments()
     {
          return new DreamCommentCollection($this->my->dream_comments()->paginate());
     }

     /**
     * @OA\GET(path="/api/v1/my/comments/stories",
     *   tags={"My"},
     *   summary="Get My Comments for Stories",
     *   description="Get My Comments for Stories",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function story_comments()
     {
          return new StoryCommentCollection($this->my->story_comments()->paginate());
     }

     /**
     * @OA\GET(path="/api/v1/my/comments/galleries",
     *   tags={"My"},
     *   summary="Get My Comments for Galleries",
     *   description="Get My Comments for Galleries",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function gallery_comments()
     {
          return new GalleryCommentCollection($this->my->gallery_comments()->paginate());
     }

     /**
     * @OA\POST(path="/api/v1/my/change_password",
     *   tags={"My"},
     *   summary="Change My Password",
     *   description="Change My Password",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"password"},
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function change_password(ChangePasswordRequest $request)
     {
          $this->my->update(['password' => bcrypt($request->get('password'))]);
          try {
               return response_success('Password Changed', 'auth');
          } catch (\Exception $e) {
               return response_error('unknown error', $code = 500);
          }
     }

     /**
     * @OA\POST(path="/api/v1/my/profile_image",
     *   tags={"My"},
     *   summary="Upload My Profile Image",
     *   description="Upload My Profile Image",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"profile_image"},
     *                 @OA\Property(
     *                     property="profile_image",
     *                     description="Profile Picture",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function profile_image(ProfileImageUploadRequest $request)
     {
          $input = $request->all();
       
          if($request->hasFile('profile_image')) {
               $path = $request->profile_image->store('images/profile_image', env('FILESYSTEM_DRIVER', 'public'));
               $input['profile_image'] = $path;
          }
          $this->my->update($input);
          try {
               $input['profile_image'] = \Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($path);
               return response_success('Profile Image Changed', 'auth', $input);
          } catch (\Exception $e) {
               return response_error('unknown error', $code = 500);
          }
     }


     /**
     * @OA\POST(path="/api/v1/my/update_profile",
     *   tags={"My"},
     *   summary="Upload My Profile Image",
     *   description="Upload My Profile Image",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="Full Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="profile_image",
     *                     description="Profile Picture",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     description="Handphone / Fix Phone",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="birth_date",
     *                     description="Birth Date",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="gender",
     *                     description="ex: M or F",
     *                     enum={"M", "F"},
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="home_address",
     *                     description="Home Address",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="office_address",
     *                     description="Office Address",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
     public function update_profile(UpdateProfileRequest $request)
     {
          $input = $request->except(['email', 'password']);
       
          if($request->hasFile('profile_image')) {
               $path = $request->profile_image->store('images/profile_image', env('FILESYSTEM_DRIVER', 'public'));
               $input['profile_image'] = $path;
               $input['profile_image'] = \Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($path);
          }
          $this->my->update($input);
          try {
               return response_success('Profile Changed', 'auth');
          } catch (\Exception $e) {
               return response_error('unknown error', $code = 500);
          }
     }

}
