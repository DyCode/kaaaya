<?php

namespace App\Http\Controllers\Api;

use App\Models\DreamGoodsDonation;
use App\Models\Dream;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamGoodsDonationApiCreateRequest;
use App\Http\Requests\DreamGoodsDonationApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DreamGoodsDonation as DreamGoodsDonationSingle;
use App\Http\Resources\DreamGoodsDonationCollection;
use App\Notifications\ContentCreated;
use DB;

class DreamGoodsDonationController extends Controller
{
    public $dream_goods_donations;
    public $dreams;

    public function __construct(DreamGoodsDonation $dream_goods_donations, Dream $dreams)
    {
        $this->dream_goods_donations = $dream_goods_donations;
        $this->dreams = $dreams;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}/goods_donations",
     *   tags={"Dream"},
     *   summary="Get Goods Donations for dream",
     *   description="Get Goods Donations for dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $dream_id)
    {
        $dream = $this->dreams->findOrFail($dream_id);

        return new DreamGoodsDonationCollection($dream->goods_donations()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/dreams/{id}/goods_donations",
     *   tags={"Dream"},
     *   summary="Post Goods Donation for Dream",
     *   description="Post Goods Donation for Dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"goods_needed","amount_needed", "unit_id", "donation_date", "amount_donated", "shipping_method_id"},
     *                 @OA\Property(
     *                     property="goods_needed",
     *                     description="Goods Needed",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="amount_needed",
     *                     description="Amount Needed",
     *                     enum={"1", "0"},
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="unit_id",
     *                     description="Unit ID",
     *                     type="number",
     *                 ),
     *                 @OA\Property(
     *                     property="donation_date",
     *                     description="Donation Date",
     *                     type="date",
     *                 ),
     *                 @OA\Property(
     *                     property="amount_donated",
     *                     description="Amount Donated",
     *                     enum={"1", "0"},
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="shipping_method_id",
     *                     description="Shipping Method ID",
     *                     type="number",
     *                 ),
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Donation Comment",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(DreamGoodsDonationApiCreateRequest $request, $dream_id)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $input['dream_id'] = $dream_id;
           
            $input['donation_date'] = \Carbon\Carbon::parse($input['donation_date'])->format('Y-m-d H:i:s');
            $dream_goods_donation = $this->dream_goods_donations->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($dream_goods_donation, 'dream_goods_donation'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error($e->getMessage(), 400);

            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}/goods_donations/{good_donation_id}",
     *   tags={"Dream"},
     *   summary="Get Goods Donation for dream",
     *   description="Get Goods Donation for dream",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Dream ID",
     *   ),
     *   @OA\Parameter(
     *     name="good_donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Goods Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($id)
    {
        return new DreamGoodsDonationSingle($this->dream_goods_donations->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamGoodsDonationApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->dream_goods_donations = $this->dream_goods_donations->findOrFail($id);
        // try {
        //     $this->dream_goods_donations->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->dream_goods_donations->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
