<?php

namespace App\Http\Controllers\Api;

use App\Models\GalleryComment;
use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryCommentApiCreateRequest;
use App\Http\Requests\GalleryCommentApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\GalleryComment as GalleryCommentSingle;
use App\Http\Resources\GalleryCommentCollection;

class GalleryCommentController extends Controller
{
    public $gallery_comments;

    public function __construct(GalleryComment $gallery_comments)
    {
        $this->gallery_comments = $gallery_comments;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/donation_categories/{donation_category_id}/galleries/{gallery_id}/comments",
     *   tags={"Static"},
     *   summary="Get Comments for Donation Category Gallery",
     *   description="Get Comments for Donation Category Gallery",
     *   @OA\Parameter(
     *     name="donation_category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation Category ID",
     *   ),
     *   @OA\Parameter(
     *     name="gallery_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Gallery ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $donation_category_id, $gallery_id)
    {
        return new GalleryCommentCollection($this->gallery_comments->where('gallery_id', $gallery_id)->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/donation_categories/{donation_category_id}/galleries/{gallery_id}/comments",
     *   tags={"Static"},
     *   summary="Post Comments for Donation Category Gallery",
     *   description="Post Comments for Donation Category Gallery",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="donation_category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation Category ID",
     *   ),
     *   @OA\Parameter(
     *     name="gallery_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Gallery ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Comment",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(GalleryCommentApiCreateRequest $request, $donation_category_id, $gallery_id)
    {
        $input = $request->all();
        try {
            $input['gallery_id'] = $gallery_id;
            $this->gallery_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/donation_categories/{donation_category_id}/galleries/{gallery_id}/comments/{comment_id}/reply",
     *   tags={"Static"},
     *   summary="Post Reply for Comment",
     *   description="Post Reply for Comment",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="donation_category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation Category ID",
     *   ),
     *   @OA\Parameter(
     *     name="gallery_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Gallery ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Reply",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function reply(GalleryCommentApiCreateRequest $request, $donation_category_id, $gallery_id, $id)
    {
        $input = $request->all();

        try {
            $input['gallery_id'] = null;
            $input['reply_header_id'] = $id;
            $this->gallery_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/donation_categories/{donation_category_id}/galleries/{gallery_id}/comments/{id}",
     *   tags={"Static"},
     *   summary="Get Comments for Donation Category",
     *   description="Get Comments for Donation Category",
     *   @OA\Parameter(
     *     name="donation_category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation Category ID",
     *   ),
     *   @OA\Parameter(
     *     name="gallery_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Gallery ID",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($donation_category_id, $gallery_id, $id)
    {
        return new GalleryCommentSingle($this->gallery_comments->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryCommentApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->gallery_comments = $this->gallery_comments->findOrFail($id);
        // try {
        //     $this->gallery_comments->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->gallery_comments->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
