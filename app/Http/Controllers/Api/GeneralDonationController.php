<?php

namespace App\Http\Controllers\Api;

use App\Models\GeneralDonation;
use App\Http\Controllers\Controller;
use App\Http\Requests\GeneralDonationApiCreateRequest;
use App\Http\Requests\GeneralDonationApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\GeneralDonation as GeneralDonationSingle;
use App\Http\Resources\GeneralDonationCollection;
use App\Notifications\ContentCreated;
use DB;

class GeneralDonationController extends Controller
{
    public $general_donations;

    public function __construct(GeneralDonation $general_donations)
    {
        $this->general_donations = $general_donations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\GET(path="/api/v1/general_donations",
     *   tags={"General"},
     *   summary="Get General Donations",
     *   description="Get General Donations",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request)
    {
        return new GeneralDonationCollection($this->general_donations->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/general_donations",
     *   tags={"General"},
     *   summary="Post General Donation",
     *   description="Post General Donation",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"date", "amount"},
     *                 @OA\Property(
     *                     property="date",
     *                     description="Donation Date",
     *                     type="date",
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     description="Donation Amount",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Donation Comment",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(GeneralDonationApiCreateRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            if($request->hasFile('transfer_slip')) {
                $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
                $input['transfer_slip'] = $path;
            }
            $input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
            $general_donation = $this->general_donations->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($general_donation, 'general_donation'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/general_donations/{donation_id}/receipt",
     *   tags={"General"},
     *   summary="Post General Donation Receipt",
     *   description="Post General Donation Receipt",
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="General Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"transfer_slip"},
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function receipt(GeneralDonationApiUpdateRequest $request, $id)
    {
        $input = $request->all();
        DB::beginTransaction();
        // dd(auth('api')->user()->id);
        try {
            $general_donation = $this->general_donations->findOrFail($id);
            if ($general_donation->consumer_id != auth('api')->user()->id) {
                throw new \Exception("the Donation does not belong to the user");
            }
            if ($general_donation->verification_flag) {
                throw new \Exception("the Donation has been verified, cannot edited");
            }
            $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
            $input['transfer_slip'] = $path;
            $general_donation->update($input);

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/general_donations/{donation_id}",
     *   tags={"General"},
     *   summary="Get General Donation",
     *   description="Get General Donation",
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="General Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($id)
    {
        return new GeneralDonationSingle($this->general_donations->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GeneralDonationApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->general_donations = $this->general_donations->findOrFail($id);
        // try {
        //     $this->general_donations->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->general_donations->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
