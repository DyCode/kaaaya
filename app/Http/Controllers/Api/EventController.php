<?php

namespace App\Http\Controllers\Api;

use App\Models\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventApiCreateRequest;
use App\Http\Requests\EventApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\Event as EventSingle;
use App\Http\Resources\EventCollection;
use App\Notifications\ContentCreated;
use DB;
class EventController extends Controller
{
    public $events;

    public function __construct(Event $events)
    {
        $this->events = $events;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events",
     *   tags={"Event"},
     *   summary="Get Events",
     *   description="Get Events",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),  
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request)
    {
        return new EventCollection($this->events->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/events",
     *   tags={"Event"},
     *   summary="Create Events",
     *   description="Create Events",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
    
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="id[title]",
     *                     description="Title in Indonesian",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="id[description]",
     *                     description="Description in Indonesian",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="en[title]",
     *                     description="Title in English",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="en[description]",
     *                     description="Description in English",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="donation_category_id",
     *                     description="Donation Category",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="due_date",
     *                     description="Due Date",
     *                     type="date"
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     description="Image",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(EventApiCreateRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            if($request->hasFile('image')) {
                $path = $request->image->store('images/event', env('FILESYSTEM_DRIVER', 'public'));
                $input['image'] = $path;
            }
            $input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $event = $this->events->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($event, 'event'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();

            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events/{id}",
     *   tags={"Event"},
     *   summary="Get Event",
     *   description="Get Event",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Event",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($id)
    {
        return new EventSingle($this->events->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\PUT(path="/api/v1/events/{id}",
     *   tags={"Event"},
     *   summary="Update Events",
     *   description="Update Events",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Event",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="id[title]",
     *                     description="Title in Indonesian",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="id[description]",
     *                     description="Description in Indonesian",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="en[title]",
     *                     description="Title in English",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="en[description]",
     *                     description="Description in English",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="donation_category_id",
     *                     description="Donation Category",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="due_date",
     *                     description="Due Date",
     *                     type="date"
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     description="Image",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function update(EventApiUpdateRequest $request, $id)
    {
        $event = $this->events->findOrFail($id);
        $input = [];
        new \App\Classes\ParseInputStream($input);
        $request->merge($input);
        $input = $request->all();
        DB::beginTransaction();
        try {
            if ($event->consumer_id != auth('api')->user()->id) {
                throw new \Exception("the event does not belong to the user");
            }
            if ($event->verification_flag) {
                throw new \Exception("the event has been verified, cannot edited");
            }
            if($request->image instanceof \Illuminate\Http\UploadedFile) {
                $path = $request->image->store('images/event', env('FILESYSTEM_DRIVER', 'public'));
                $input['image'] = $path;
            }
            $input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');

            $event->update($input);
            
            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->events->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
