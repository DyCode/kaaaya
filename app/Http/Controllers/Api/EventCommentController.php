<?php

namespace App\Http\Controllers\Api;

use App\Models\EventComment;
use App\Models\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventCommentApiCreateRequest;
use App\Http\Requests\EventCommentApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\EventComment as EventCommentSingle;
use App\Http\Resources\EventCommentCollection;

class EventCommentController extends Controller
{
    public $event_comments;
    public $events;

    public function __construct(EventComment $event_comments, Event $events)
    {
        $this->events = $events;
        $this->event_comments = $event_comments;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events/{id}/comments",
     *   tags={"Event"},
     *   summary="Get Comments for event",
     *   description="Get Comments for event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $event_id)
    {
        $event = $this->events->findOrFail($event_id);
        return new EventCommentCollection($event->comments()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/events/{id}/comments",
     *   tags={"Event"},
     *   summary="Post Comment for Event",
     *   description="Post Comment for Event",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Comment",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(EventCommentApiCreateRequest $request, $event_id)
    {
        $input = $request->all();

        try {
            $input['event_id'] = $event_id;
            $this->event_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/events/{id}/comments/{comment_id}/reply",
     *   tags={"Event"},
     *   summary="Post Reply for Comment",
     *   description="Post Reply for Comment",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Reply",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function reply(EventCommentApiCreateRequest $request, $event_id, $id)
    {
        $input = $request->all();

        try {
            $input['event_id'] = null;
            $input['reply_header_id'] = $id;
            $this->event_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events/{id}/comments/{comment_id}",
     *   tags={"Event"},
     *   summary="Get Detail Comment for event",
     *   description="Get Detail Comment for event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($event_id, $id)
    {
        return new EventCommentSingle($this->event_comments->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventCommentApiUpdateRequest $request, $event_id, $id)
    {
        abort(404);
        // $this->event_comments = $this->event_comments->findOrFail($id);
        // try {
        //     $this->event_comments->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id, $id)
    {
        abort(404);
        // try {
        //     $this->event_comments->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
