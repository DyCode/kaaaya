<?php

namespace App\Http\Controllers\Api;

use App\Models\MainBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\MainBannerApiCreateRequest;
use App\Http\Requests\MainBannerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\MainBanner as MainBannerSingle;
use App\Http\Resources\MainBannerCollection;

class MainBannerController extends Controller
{
    public $main_banners;

    public function __construct(MainBanner $main_banners)
    {
        $this->main_banners = $main_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/main_banners",
     *   tags={"Banner"},
     *   summary="Get Main Banners",
     *   description="Get Main Banners",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new MainBannerCollection($this->main_banners->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MainBannerApiCreateRequest $request)
    {
        try {
            $this->main_banners->create($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/main_banners/{id}",
     *   tags={"Content"},
     *   summary="Get Main Banner",
     *   description="Get Main Banner",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Main Banner",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function show($id)
    {
        return new MainBannerSingle($this->main_banners->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MainBannerApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->main_banners = $this->main_banners->findOrFail($id);
        // try {
        //     $this->main_banners->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->main_banners->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
