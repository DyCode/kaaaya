<?php

namespace App\Http\Controllers\Api;

use App\Models\DonationBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationBannerApiCreateRequest;
use App\Http\Requests\DonationBannerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DonationBanner as DonationBannerSingle;
use App\Http\Resources\DonationBannerCollection;

class DonationBannerController extends Controller
{
    public $donation_banners;

    public function __construct(DonationBanner $donation_banners)
    {
        $this->donation_banners = $donation_banners;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/donation_banners",
     *   tags={"Banner"},
     *   summary="Get Consumer Banners",
     *   description="Get Consumer Banners",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return new DonationBannerSingle($this->donation_banners->where('active_flag', true)->orderBy('created_at', 'desc')->firstOrFail());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationBannerApiCreateRequest $request)
    {
        abort(404);
    }

   
    public function show($id)
    {
        // return new DonationBannerSingle($this->donation_banners->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonationBannerApiUpdateRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
