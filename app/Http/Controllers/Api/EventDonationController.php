<?php

namespace App\Http\Controllers\Api;

use App\Models\EventDonation;
use App\Models\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventDonationApiCreateRequest;
use App\Http\Requests\EventDonationApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\EventDonation as EventDonationSingle;
use App\Http\Resources\EventDonationCollection;
use App\Notifications\ContentCreated;
use DB;

class EventDonationController extends Controller
{
    public $event_donations;
    public $events;

    public function __construct(EventDonation $event_donations, Event $events)
    {
        $this->events = $events;
        $this->event_donations = $event_donations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\GET(path="/api/v1/events/{id}/donations",
     *   tags={"Event"},
     *   summary="Get Donations for event",
     *   description="Get Donations for event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $event_id)
    {
        $event = $this->events->findOrFail($event_id);
        return new EventDonationCollection($event->donations()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/events/{id}/donations",
     *   tags={"Event"},
     *   summary="Post Donation for Event",
     *   description="Post Donation for Event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"date", "amount"},
     *                 @OA\Property(
     *                     property="date",
     *                     description="Donation Date",
     *                     type="date",
     *                 ),
     *                 @OA\Property(
     *                     property="amount",
     *                     description="Donation Amount",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Donation Comment",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(EventDonationApiCreateRequest $request, $event_id)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $input['event_id'] = $event_id;
            if($request->hasFile('transfer_slip')) {
                $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
                $input['transfer_slip'] = $path;
            }
            $input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
            $event_donation = $this->event_donations->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($event_donation, 'event_donation'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/events/{id}/donations/{donation_id}/receipt",
     *   tags={"Event"},
     *   summary="Post Donation Receipt for Event",
     *   description="Post Donation Receipt for Event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"transfer_slip"},
     *                 @OA\Property(
     *                     property="transfer_slip",
     *                     description="Donation Receipt",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function receipt(EventDonationApiUpdateRequest $request, $event_id, $id)
    {
        $input = $request->all();
        DB::beginTransaction();
        // dd(auth('api')->user()->id);
        try {
            $event_donation = $this->event_donations->findOrFail($id);
            if ($event_donation->consumer_id != auth('api')->user()->id) {
                throw new \Exception("the Donation does not belong to the user");
            }
            if ($event_donation->verification_flag) {
                throw new \Exception("the Donation has been verified, cannot edited");
            }
            $path = $request->transfer_slip->store('images/transfer_slip', env('FILESYSTEM_DRIVER', 'public'));
            $input['transfer_slip'] = $path;
            $event_donation->update($input);

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/events/{id}/donations/{donation_id}",
     *   tags={"Event"},
     *   summary="Get Donation for event",
     *   description="Get Donation for event",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Event ID",
     *   ),
     *   @OA\Parameter(
     *     name="donation_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Donation ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($event_id, $id)
    {
        return new EventDonationSingle($this->event_donations->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventDonationApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->event_donations = $this->event_donations->findOrFail($id);
        // try {
        //     $this->event_donations->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->event_donations->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
