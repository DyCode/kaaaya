<?php

namespace App\Http\Controllers\Api;

use App\Models\EventBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventBannerApiCreateRequest;
use App\Http\Requests\EventBannerApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\EventBanner as EventBannerSingle;
use App\Http\Resources\EventBannerCollection;

class EventBannerController extends Controller
{
    public $event_banners;

    public function __construct(EventBanner $event_banners)
    {
        $this->event_banners = $event_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\GET(path="/api/v1/event_banners",
     *   tags={"Banner"},
     *   summary="Get Event Banners",
     *   description="Get Event Banners",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new EventBannerSingle($this->event_banners->where('active_flag', true)->orderBy('created_at', 'desc')->firstOrFail());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventBannerApiCreateRequest $request)
    {
        abort(404);
        // try {
        //     $this->event_banners->create($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }


    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventBannerApiUpdateRequest $request, $id)
    {
        $this->event_banners = $this->event_banners->findOrFail($id);
        try {
            $this->event_banners->update($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->event_banners->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
