<?php

namespace App\Http\Controllers\Api;

use App\Models\EventLabel;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventLabelApiCreateRequest;
use App\Http\Requests\EventLabelApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\EventLabel as EventLabelSingle;
use App\Http\Resources\EventLabelCollection;

class EventLabelController extends Controller
{
    public $event_labels;

    public function __construct(EventLabel $event_labels)
    {
        $this->event_labels = $event_labels;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\GET(path="/api/v1/event_labels",
     *   tags={"Content"},
     *   summary="Get Event Labels",
     *   description="Get Event Labels",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new EventLabelCollection($this->event_labels->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventLabelApiCreateRequest $request)
    {
        abort(404);
        // try {
        //     $this->event_labels->create($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/event_labels/{id}",
     *   tags={"Content"},
     *   summary="Get Event Label",
     *   description="Get Event Label",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function show($id)
    {
        return new EventLabelSingle($this->event_labels->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventLabelApiUpdateRequest $request, $id)
    {
        $this->event_labels = $this->event_labels->findOrFail($id);
        try {
            $this->event_labels->update($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->event_labels->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
