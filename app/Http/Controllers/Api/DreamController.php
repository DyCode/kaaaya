<?php

namespace App\Http\Controllers\Api;

use App\Models\Dream;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamApiCreateRequest;
use App\Http\Requests\DreamApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\Dream as DreamSingle;
use App\Http\Resources\DreamCollection;
use App\Notifications\ContentCreated;
use DB;

class DreamController extends Controller
{
    public $dreams;

    public function __construct(Dream $dreams)
    {
        $this->dreams = $dreams;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\GET(path="/api/v1/dreams",
     *   tags={"Dream"},
     *   summary="Get Dreams",
     *   description="Get Dreams",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request)
    {
        return new DreamCollection($this->dreams->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\POST(path="/api/v1/dreams",
     *   tags={"Dream"},
     *   summary="Create Dream",
     *   description="Create Dream",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
    
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="id[title]",
     *                     description="Title in Indonesian",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="id[description]",
     *                     description="Description in Indonesian",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="en[title]",
     *                     description="Title in English",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="en[description]",
     *                     description="Description in English",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="donation_category_id",
     *                     description="Donation Category",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="due_date",
     *                     description="Due Date",
     *                     type="date"
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     description="Image",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(DreamApiCreateRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            if($request->hasFile('image')) {
                $path = $request->image->store('images/dream', env('FILESYSTEM_DRIVER', 'public'));
                $input['image'] = $path;
            }
            $input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $dream = $this->dreams->create($input);
            
            //send notification email to consumer
            auth('api')->user()->notify(new ContentCreated($dream, 'dream'));

            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();

            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/dreams/{id}",
     *   tags={"Dream"},
     *   summary="Get Dream",
     *   description="Get Dream",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Dream",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($id)
    {
        return new DreamSingle($this->dreams->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\PUT(path="/api/v1/dreams/{id}",
     *   tags={"Dream"},
     *   summary="Update Dreams",
     *   description="Update Dreams",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Dream",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="id[title]",
     *                     description="Title in Indonesian",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="id[description]",
     *                     description="Description in Indonesian",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="en[title]",
     *                     description="Title in English",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="en[description]",
     *                     description="Description in English",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="donation_category_id",
     *                     description="Donation Category",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="due_date",
     *                     description="Due Date",
     *                     type="date"
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     description="Image",
     *                     type="file"
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function update(DreamApiUpdateRequest $request, $id)
    {
        $dream = $this->dreams->findOrFail($id);
        $input = [];
        new \App\Classes\ParseInputStream($input);
        $request->merge($input);
        $input = $request->all();
        DB::beginTransaction();
        try {
            if ($dream->consumer_id != auth('api')->user()->id) {
                throw new \Exception("the dream does not belong to the user");
            }
            if ($dream->verification_flag) {
                throw new \Exception("the dream has been verified, cannot edited");
            }
            if($request->image instanceof \Illuminate\Http\UploadedFile) {
                $path = $request->image->store('images/dream', env('FILESYSTEM_DRIVER', 'public'));
                $input['image'] = $path;
            }
            $input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');

            $dream->update($input);
            
            DB::commit();
            return response_success('operation success');
        } catch (\Exception $e) {
            DB::rollBack();
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->dreams->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
