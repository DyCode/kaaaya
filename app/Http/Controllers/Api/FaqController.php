<?php

namespace App\Http\Controllers\Api;

use App\Models\Faq;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqApiCreateRequest;
use App\Http\Requests\FaqApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\Faq as FaqSingle;
use App\Http\Resources\FaqCollection;

class FaqController extends Controller
{
    public $faqs;

    public function __construct(Faq $faqs)
    {
        $this->faqs = $faqs;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**

    /**
     * @OA\GET(path="/api/v1/faqs",
     *   tags={"Content"},
     *   summary="Get Faq's",
     *   description="Get Faq's",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new FaqCollection($this->faqs->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqApiCreateRequest $request)
    {
        try {
            $this->faqs->create($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     /**
     * @OA\GET(path="/api/v1/faqs/{id}",
     *   tags={"Content"},
     *   summary="Get Faq",
     *   description="Get Faq",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Faq",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function show($id)
    {
        return new FaqSingle($this->faqs->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->faqs = $this->faqs->findOrFail($id);
        // try {
        //     $this->faqs->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->faqs->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
