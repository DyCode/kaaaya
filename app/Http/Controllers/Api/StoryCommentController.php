<?php

namespace App\Http\Controllers\Api;

use App\Models\StoryComment;
use App\Models\Story;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryCommentApiCreateRequest;
use App\Http\Requests\StoryCommentApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\StoryComment as StoryCommentSingle;
use App\Http\Resources\StoryCommentCollection;

class StoryCommentController extends Controller
{
    public $story_comments;

    public function __construct(StoryComment $story_comments, Story $stories)
    {
        $this->story_comments = $story_comments;
        $this->stories = $stories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/stories/{id}/comments",
     *   tags={"Story"},
     *   summary="Get Comments for story",
     *   description="Get Comments for story",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function index(Request $request, $story_id)
    {
        $story = $this->stories->findOrFail($story_id);
        return new StoryCommentCollection($story->comments()->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\POST(path="/api/v1/stories/{id}/comments",
     *   tags={"Story"},
     *   summary="Post Comment for Story",
     *   description="Post Comment for Story",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Comment",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function store(StoryCommentApiCreateRequest $request, $story_id)
    {
        $input = $request->all();

        try {
            $input['story_id'] = $story_id;
            $this->story_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * @OA\POST(path="/api/v1/stories/{id}/comments/{comment_id}/reply",
     *   tags={"Story"},
     *   summary="Post Reply for Comment",
     *   description="Post Reply for Comment",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="comment",
     *                     description="Your Reply",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function reply(StoryCommentApiCreateRequest $request, $story_id, $id)
    {
        $input = $request->all();

        try {
            $input['story_id'] = null;
            $input['reply_header_id'] = $id;
            $this->story_comments->create($input);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/stories/{id}/comments/{comment_id}",
     *   tags={"Story"},
     *   summary="Get Detail Comment for story",
     *   description="Get Detail Comment for story",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Story ID",
     *   ),
     *   @OA\Parameter(
     *     name="comment_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Comment ID",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     *   security={
     *         {"bearerAuth": {}}
     *     },
     * )
     */
    public function show($id)
    {
        return new StoryCommentSingle($this->story_comments->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryCommentApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->story_comments = $this->story_comments->findOrFail($id);
        // try {
        //     $this->story_comments->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->story_comments->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
