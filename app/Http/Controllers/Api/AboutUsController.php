<?php

namespace App\Http\Controllers\Api;

use App\Models\AboutUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\AboutUsApiCreateRequest;
use App\Http\Requests\AboutUsApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\AboutUs as AboutUsSingle;
use App\Http\Resources\AboutUsCollection;

class AboutUsController extends ApiController
{
    public $about_uses;

    public function __construct(AboutUs $about_uses)
    {
        $this->about_uses = $about_uses;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/about_uses",
     *   tags={"Static"},
     *   summary="Get About Us",
     *   description="Get About Us",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new AboutUsSingle($this->about_uses->where('active_flag', true)->orderBy('created_at', 'desc')->firstOrFail());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutUsApiCreateRequest $request)
    {
        abort(404);
        // return new AboutUsSingle($this->about_uses->where('active_flag', 1)->orderBy('created_at', 'desc')->first());
    }

  
    public function show($id)
    {
        abort(404);
        // return new AboutUsSingle($this->about_uses->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutUsApiUpdateRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
