<?php

namespace App\Http\Controllers\Api;

use App\Models\PrivacyPolicy;
use App\Http\Controllers\Controller;
use App\Http\Requests\PrivacyPolicyApiCreateRequest;
use App\Http\Requests\PrivacyPolicyApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\PrivacyPolicy as PrivacyPolicySingle;
use App\Http\Resources\PrivacyPolicyCollection;

class PrivacyPolicyController extends Controller
{
    public $privacy_policies;

    public function __construct(PrivacyPolicy $privacy_policies)
    {
        $this->privacy_policies = $privacy_policies;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/privacy_policies",
     *   tags={"Content"},
     *   summary="Get Privacy Policy",
     *   description="Get Privacy Policy",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
    
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new PrivacyPolicyCollection($this->privacy_policies->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyApiCreateRequest $request)
    {
        try {
            $this->privacy_policies->create($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/privacy_policies/{id}",
     *   tags={"Content"},
     *   summary="Get Privacy Policy",
     *   description="Get Privacy Policy",
     *   @OA\Parameter(
     *     name="Accept-Language",
     *     in="header",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="Language code **ex: id**",
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Privacy Policy",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function show($id)
    {
        return new PrivacyPolicySingle($this->privacy_policies->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyApiUpdateRequest $request, $id)
    {
        $this->privacy_policies = $this->privacy_policies->findOrFail($id);
        try {
            $this->privacy_policies->update($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->privacy_policies->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
