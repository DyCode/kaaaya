<?php

namespace App\Http\Controllers\Api;

use App\Models\FaqCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqCategoryApiCreateRequest;
use App\Http\Requests\FaqCategoryApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\FaqCategory as FaqCategorySingle;
use App\Http\Resources\FaqCategoryCollection;

class FaqCategoryController extends Controller
{
    public $faq_categories;

    public function __construct(FaqCategory $faq_categories)
    {
        $this->faq_categories = $faq_categories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/faq_categories",
     *   tags={"Content"},
     *   summary="Get FAQ Categories",
     *   description="Get FAQ Categories",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new FaqCategoryCollection($this->faq_categories->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqCategoryApiCreateRequest $request)
    {
        try {
            $this->faq_categories->create($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/faq_categories/{id}",
     *   tags={"Content"},
     *   summary="Get Faq Category",
     *   description="Get Faq Category",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Faq Category",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function show($id)
    {
        return new FaqCategorySingle($this->faq_categories->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqCategoryApiUpdateRequest $request, $id)
    {
        $this->faq_categories = $this->faq_categories->findOrFail($id);
        try {
            $this->faq_categories->update($request->all());
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->faq_categories->destroy($id);
            return response_success('operation success');
        } catch (\Exception $e) {
            return response_error('operation failed', 400);
        }
    }
}
