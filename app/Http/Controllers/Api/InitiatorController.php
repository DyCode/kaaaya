<?php

namespace App\Http\Controllers\Api;

use App\Models\Initiator;
use App\Http\Controllers\Controller;
use App\Http\Requests\InitiatorApiCreateRequest;
use App\Http\Requests\InitiatorApiUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Resources\Initiator as InitiatorSingle;
use App\Http\Resources\InitiatorCollection;

class InitiatorController extends Controller
{
    public $initiators;

    public function __construct(Initiator $initiators)
    {
        $this->initiators = $initiators;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/initiators",
     *   tags={"Content"},
     *   summary="Get Initiator",
     *   description="Get Initiator",
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function index(Request $request)
    {
        return new InitiatorCollection($this->initiators->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InitiatorApiCreateRequest $request)
    {
        abort(404);
        // try {
        //     $this->initiators->create($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\GET(path="/api/v1/initators/{id}",
     *   tags={"Content"},
     *   summary="Get Initiator",
     *   description="Get Initiator",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *     ),
     *     description="ID Initiator",
     *   ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Error"),
     * )
     */
    public function show($id)
    {
        return new InitiatorSingle($this->initiators->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InitiatorApiUpdateRequest $request, $id)
    {
        abort(404);
        // $this->initiators = $this->initiators->findOrFail($id);
        // try {
        //     $this->initiators->update($request->all());
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // try {
        //     $this->initiators->destroy($id);
        //     return response_success('operation success');
        // } catch (\Exception $e) {
        //     return response_error('operation failed', 400);
        // }
    }
}
