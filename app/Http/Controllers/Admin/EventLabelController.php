<?php

namespace App\Http\Controllers\Admin;

use App\Models\EventLabel;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventLabelCreateRequest;
use App\Http\Requests\EventLabelUpdateRequest;
use Illuminate\Http\Request;

class EventLabelController extends Controller
{
    public $event_labels;

    public function __construct(EventLabel $event_labels)
    {
        $this->event_labels = $event_labels;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu event_labels');
        if ($request->has('search')) {
            $data = $this->event_labels->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->event_labels->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.event_labels.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create event_labels');
        $event_labels = $this->event_labels->all();
        $data = $this->event_labels;

        return view('admin.event_labels.create', compact('data', "event_labels"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventLabelCreateRequest $request)
    {
        check_permission('create event_labels');
        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images/banner', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->event_labels->create($input);
            flash('EventLabel Saved')->success();
            return redirect()->route('admin.event_labels.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read event_labels');
        return view('admin.event_labels.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update event_labels');
        $event_labels = $this->event_labels->all();
        $data = $this->event_labels->find($id);
        return view('admin.event_labels.edit', compact('data', 'event_labels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventLabelUpdateRequest $request, $id)
    {
        check_permission('update event_labels');
        $this->event_labels = $this->event_labels->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images/banner', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->event_labels->update($input);
            flash('EventLabel Saved')->success();
            return redirect()->route('admin.event_labels.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete event_labels');
        try {
            $this->event_labels->destroy($id);
            flash('EventLabel Deleted')->success();
            return redirect()->route('admin.event_labels.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
