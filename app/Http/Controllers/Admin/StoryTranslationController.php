<?php

namespace App\Http\Controllers\Admin;

use App\Models\StoryTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryTranslationCreateRequest;
use App\Http\Requests\StoryTranslationUpdateRequest;
use Illuminate\Http\Request;

class StoryTranslationController extends Controller
{
    public $story_translations;

    public function __construct(StoryTranslation $story_translations)
    {
        $this->story_translations = $story_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->story_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->story_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.story_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $story_translations = $this->story_translations->all();
        $data = $this->story_translations;

        return view('admin.story_translations.create', compact('data', "story_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->story_translations->create($input);
            flash('StoryTranslation Saved')->success();
            return redirect()->route('admin.story_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.story_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $story_translations = $this->story_translations->all();
        $data = $this->story_translations->find($id);
        return view('admin.story_translations.edit', compact('data', 'story_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryTranslationUpdateRequest $request, $id)
    {
        $this->story_translations = $this->story_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->story_translations->update($input);
            flash('StoryTranslation Saved')->success();
            return redirect()->route('admin.story_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->story_translations->destroy($id);
            flash('StoryTranslation Deleted')->success();
            return redirect()->route('admin.story_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
