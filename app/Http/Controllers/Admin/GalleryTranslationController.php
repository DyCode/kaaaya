<?php

namespace App\Http\Controllers\Admin;

use App\Models\GalleryTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryTranslationCreateRequest;
use App\Http\Requests\GalleryTranslationUpdateRequest;
use Illuminate\Http\Request;

class GalleryTranslationController extends Controller
{
    public $gallery_translations;

    public function __construct(GalleryTranslation $gallery_translations)
    {
        $this->gallery_translations = $gallery_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->gallery_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->gallery_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.gallery_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gallery_translations = $this->gallery_translations->all();
        $data = $this->gallery_translations;

        return view('admin.gallery_translations.create', compact('data', "gallery_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->gallery_translations->create($input);
            flash('GalleryTranslation Saved')->success();
            return redirect()->route('admin.gallery_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.gallery_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $gallery_translations = $this->gallery_translations->all();
        $data = $this->gallery_translations->find($id);
        return view('admin.gallery_translations.edit', compact('data', 'gallery_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryTranslationUpdateRequest $request, $id)
    {
        $this->gallery_translations = $this->gallery_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->gallery_translations->update($input);
            flash('GalleryTranslation Saved')->success();
            return redirect()->route('admin.gallery_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->gallery_translations->destroy($id);
            flash('GalleryTranslation Deleted')->success();
            return redirect()->route('admin.gallery_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
