<?php

namespace App\Http\Controllers\Admin;

use App\Models\Unit;
use App\Http\Controllers\Controller;
use App\Http\Requests\UnitCreateRequest;
use App\Http\Requests\UnitUpdateRequest;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public $units;

    public function __construct(Unit $units)
    {
        $this->units = $units;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu units');
        if ($request->has('search')) {
            $data = $this->units->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->units->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.units.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create units');
        $units = $this->units->all();
        $data = $this->units;

        return view('admin.units.create', compact('data', "units"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitCreateRequest $request)
    {
        check_permission('create units');
        $input = $request->except("_token");
        try {
            $this->units->create($input);
            flash('Unit Saved')->success();
            return redirect()->route('admin.units.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read units');
        return view('admin.units.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update units');
        $units = $this->units->all();
        $data = $this->units->find($id);
        return view('admin.units.edit', compact('data', 'units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnitUpdateRequest $request, $id)
    {
        check_permission('update units');
        $this->units = $this->units->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->units->update($input);
            flash('Unit Saved')->success();
            return redirect()->route('admin.units.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete units');
        try {
            $this->units->destroy($id);
            flash('Unit Deleted')->success();
            return redirect()->route('admin.units.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
