<?php

namespace App\Http\Controllers\Admin;

use App\Models\StoryBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryBannerCreateRequest;
use App\Http\Requests\StoryBannerUpdateRequest;
use Illuminate\Http\Request;

class StoryBannerController extends Controller
{
    public $story_banners;

    public function __construct(StoryBanner $story_banners)
    {
        $this->story_banners = $story_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        check_permission('menu story_banners');
        if ($request->has('search')) {
            $data = $this->story_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->story_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.story_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create story_banners');
        $story_banners = $this->story_banners->all();
        $data = $this->story_banners;

        return view('admin.story_banners.create', compact('data', "story_banners"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryBannerCreateRequest $request)
    {
        check_permission('create story_banners');
        disable_active_flag($this->story_banners);
        $input = $request->except("_token");
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images/story', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->story_banners->create($input);
            flash('StoryBanner Saved')->success();
            return redirect()->route('admin.story_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read story_banners');
        return view('admin.story_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update story_banners');
        $story_banners = $this->story_banners->all();
        $data = $this->story_banners->find($id);
        return view('admin.story_banners.edit', compact('data', 'story_banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryBannerUpdateRequest $request, $id)
    {
        check_permission('update story_banners');
        disable_active_flag($this->story_banners);
        
        $this->story_banners = $this->story_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images/story', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->story_banners->update($input);
            flash('StoryBanner Saved')->success();
            return redirect()->route('admin.story_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete story_banners');
        try {
            $this->story_banners->destroy($id);
            flash('StoryBanner Deleted')->success();
            return redirect()->route('admin.story_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
