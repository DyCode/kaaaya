<?php

namespace App\Http\Controllers\Admin;

use App\Models\Initiator;
use App\Http\Controllers\Controller;
use App\Http\Requests\InitiatorCreateRequest;
use App\Http\Requests\InitiatorUpdateRequest;
use Illuminate\Http\Request;

class InitiatorController extends Controller
{
    public $initiators;

    public function __construct(Initiator $initiators)
    {
        $this->initiators = $initiators;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu initiators');
        if ($request->has('search')) {
            $data = $this->initiators->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->initiators->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.initiators.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create initiators');
        $initiators = $this->initiators->all();
        $data = $this->initiators;

        return view('admin.initiators.create', compact('data', "initiators"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InitiatorCreateRequest $request)
    {
        check_permission('create initiators');
        $input = $request->except("_token");
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->initiators->create($input);
            flash('Initiator Saved')->success();
            return redirect()->route('admin.initiators.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read initiators');
        return view('admin.initiators.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update initiators');
        $initiators = $this->initiators->all();
        $data = $this->initiators->find($id);
        return view('admin.initiators.edit', compact('data', 'initiators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InitiatorUpdateRequest $request, $id)
    {
        check_permission('update initiators');
        $this->initiators = $this->initiators->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->initiators->update($input);
            flash('Initiator Saved')->success();
            return redirect()->route('admin.initiators.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete initiators');
        try {
            $this->initiators->destroy($id);
            flash('Initiator Deleted')->success();
            return redirect()->route('admin.initiators.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
