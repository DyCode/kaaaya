<?php

namespace App\Http\Controllers\Admin;

use App\Models\EventComment;
use App\Models\Event;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventCommentCreateRequest;
use App\Http\Requests\EventCommentUpdateRequest;
use Illuminate\Http\Request;

class EventCommentController extends Controller
{
    public $event_comments;
    public $events;
    public $consumers;

    public function __construct(EventComment $event_comments, Event $events, Consumer $consumers)
    {
        $this->event_comments = $event_comments;
        $this->events = $events;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu event_comments');
        if ($request->has('search')) {
            $data = $this->event_comments->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->event_comments->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.event_comments.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create event_comments');
        $consumers = $this->consumers->all();
        $headers = $this->event_comments->whereNull('reply_header_id')->get();
        $events = $this->events->get();

        return view('admin.event_comments.create', compact('consumers', "headers", "events"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventCommentCreateRequest $request)
    {
        check_permission('create event_comments');
        $input = $request->except("_token");
        try {
            $this->event_comments->create($input);
            flash('EventComment Saved')->success();
            return redirect()->route('admin.event_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read event_comments');
        return view('admin.event_comments.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update event_comments');
        $consumers = $this->consumers->all();
        $headers = $this->event_comments->whereNull('reply_header_id')->get();
        $events = $this->events->get();
        $data = $this->event_comments->find($id);
        return view('admin.event_comments.edit', compact('data', 'consumers', 'headers', 'events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventCommentUpdateRequest $request, $id)
    {
        check_permission('update event_comments');
        $this->event_comments = $this->event_comments->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->event_comments->update($input);
            flash('EventComment Saved')->success();
            return redirect()->route('admin.event_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete event_comments');
        try {
            $this->event_comments->destroy($id);
            flash('EventComment Deleted')->success();
            return redirect()->route('admin.event_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
