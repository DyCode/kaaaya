<?php

namespace App\Http\Controllers\Admin;

use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConsumerCreateRequest;
use App\Http\Requests\ConsumerUpdateRequest;
use Illuminate\Http\Request;

class ConsumerController extends Controller
{
    public $consumers;

    public function __construct(Consumer $consumers)
    {
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        check_permission('menu consumers');
        if ($request->has('search')) {
            $data = $this->consumers->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->consumers->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.consumers.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create consumers');
        $consumers = $this->consumers->all();
        $data = $this->consumers;

        return view('admin.consumers.create', compact('data', "consumers"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConsumerCreateRequest $request)
    {
        check_permission('create consumers');
        $input = $request->except("_token");
        try {
			if($request->hasFile('profile_image')) {
			$path = $request->profile_image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['profile_image'] = $path;
			}
			$input['birth_date'] = \Carbon\Carbon::parse($input['birth_date'])->format('Y-m-d H:i:s');
            $input['password'] = bcrypt($input['password']);
            $this->consumers->create($input);
            flash('Consumer Saved')->success();
            return redirect()->route('admin.consumers.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read consumers');
        return view('admin.consumers.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update consumers');
        $consumers = $this->consumers->all();
        $data = $this->consumers->find($id);
        return view('admin.consumers.edit', compact('data', 'consumers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConsumerUpdateRequest $request, $id)
    {
        check_permission('update consumers');
        $this->consumers = $this->consumers->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('profile_image')) {
			$path = $request->profile_image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['profile_image'] = $path;
			}
			$input['birth_date'] = \Carbon\Carbon::parse($input['birth_date'])->format('Y-m-d H:i:s');
            unset($input['password']);
            $this->consumers->update($input);
            flash('Consumer Saved')->success();
            return redirect()->route('admin.consumers.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete consumers');
        try {
            $this->consumers->destroy($id);
            flash('Consumer Deleted')->success();
            return redirect()->route('admin.consumers.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
