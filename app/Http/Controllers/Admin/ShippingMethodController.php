<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShippingMethod;
use App\Http\Controllers\Controller;
use App\Http\Requests\ShippingMethodCreateRequest;
use App\Http\Requests\ShippingMethodUpdateRequest;
use Illuminate\Http\Request;

class ShippingMethodController extends Controller
{
    public $shipping_methods;

    public function __construct(ShippingMethod $shipping_methods)
    {
        $this->shipping_methods = $shipping_methods;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu shipping_methods');
        if ($request->has('search')) {
            $data = $this->shipping_methods->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->shipping_methods->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.shipping_methods.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create shipping_methods');
        $shipping_methods = $this->shipping_methods->all();
        $data = $this->shipping_methods;

        return view('admin.shipping_methods.create', compact('data', "shipping_methods"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShippingMethodCreateRequest $request)
    {
        check_permission('create shipping_methods');
        $input = $request->except("_token");
        try {
            $this->shipping_methods->create($input);
            flash('ShippingMethod Saved')->success();
            return redirect()->route('admin.shipping_methods.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read shipping_methods');
        return view('admin.shipping_methods.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update shipping_methods');
        $shipping_methods = $this->shipping_methods->all();
        $data = $this->shipping_methods->find($id);
        return view('admin.shipping_methods.edit', compact('data', 'shipping_methods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ShippingMethodUpdateRequest $request, $id)
    {
        check_permission('update shipping_methods');
        $this->shipping_methods = $this->shipping_methods->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->shipping_methods->update($input);
            flash('ShippingMethod Saved')->success();
            return redirect()->route('admin.shipping_methods.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete shipping_methods');
        try {
            $this->shipping_methods->destroy($id);
            flash('ShippingMethod Deleted')->success();
            return redirect()->route('admin.shipping_methods.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
