<?php

namespace App\Http\Controllers\Admin;

use App\Models\GeneralDonation;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\GeneralDonationCreateRequest;
use App\Http\Requests\GeneralDonationUpdateRequest;
use Illuminate\Http\Request;

class GeneralDonationController extends Controller
{
    public $general_donations;
    public $consumers;

    public function __construct(GeneralDonation $general_donations, Consumer $consumers)
    {
        $this->general_donations = $general_donations;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu general_donations');
        if ($request->has('search')) {
            $data = $this->general_donations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->general_donations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.general_donations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create general_donations');
        $consumers = $this->consumers->all();
        $data = $this->general_donations;

        return view('admin.general_donations.create', compact('data', "consumers"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralDonationCreateRequest $request)
    {
        check_permission('create general_donations');
        $input = $request->except("_token");
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->general_donations->create($input);
            flash('GeneralDonation Saved')->success();
            return redirect()->route('admin.general_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read general_donations');
        return view('admin.general_donations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update general_donations');
        $consumers = $this->consumers->all();
        $data = $this->general_donations->find($id);
        return view('admin.general_donations.edit', compact('data', 'consumers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GeneralDonationUpdateRequest $request, $id)
    {
        check_permission('update general_donations');
        $this->general_donations = $this->general_donations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->general_donations->update($input);
            flash('GeneralDonation Saved')->success();
            return redirect()->route('admin.general_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete general_donations');
        try {
            $this->general_donations->destroy($id);
            flash('GeneralDonation Deleted')->success();
            return redirect()->route('admin.general_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
