<?php

namespace App\Http\Controllers\Admin;

use App\Models\StoryDonation;
use App\Models\Story;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryDonationCreateRequest;
use App\Http\Requests\StoryDonationUpdateRequest;
use Illuminate\Http\Request;

class StoryDonationController extends Controller
{
    public $story_donations;
    public $stories;
    public $consumers;

    public function __construct(StoryDonation $story_donations, Story $stories, Consumer $consumers)
    {
        $this->story_donations = $story_donations;
        $this->stories = $stories;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu story_donations');
        if ($request->has('search')) {
            $data = $this->story_donations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->story_donations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.story_donations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create story_donations');
        $consumers = $this->consumers->all();
        $stories = $this->stories->get();

        return view('admin.story_donations.create', compact('consumers', "stories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryDonationCreateRequest $request)
    {
        check_permission('create story_donations');
        $input = $request->except("_token");
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->story_donations->create($input);
            flash('StoryDonation Saved')->success();
            return redirect()->route('admin.story_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read story_donations');
        return view('admin.story_donations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update story_donations');
        $consumers = $this->consumers->all();
        $stories = $this->stories->get();
        $data = $this->story_donations->find($id);
        return view('admin.story_donations.edit', compact('data', 'stories', 'consumers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryDonationUpdateRequest $request, $id)
    {
        check_permission('update story_donations');
        $this->story_donations = $this->story_donations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->story_donations->update($input);
            flash('StoryDonation Saved')->success();
            return redirect()->route('admin.story_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete story_donations');
        try {
            $this->story_donations->destroy($id);
            flash('StoryDonation Deleted')->success();
            return redirect()->route('admin.story_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
