<?php

namespace App\Http\Controllers\Admin;

use App\Models\Dream;
use App\Models\DonationCategory;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamCreateRequest;
use App\Http\Requests\DreamUpdateRequest;
use Illuminate\Http\Request;

class DreamController extends Controller
{
    public $dreams;

    public function __construct(Dream $dreams, DonationCategory $donation_categories, Consumer $consumers)
    {
        $this->dreams = $dreams;
        $this->donation_categories = $donation_categories;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu dreams');
        if ($request->has('search')) {
            $data = $this->dreams->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->dreams->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.dreams.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create dreams');
        $consumers = $this->consumers->all();
        $donation_categories = $this->donation_categories->all();
        $data = $this->dreams;

        return view('admin.dreams.create', compact('data', "consumers", 'donation_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamCreateRequest $request)
    {
        check_permission('create dreams');
        $input = $request->except("_token");
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			$input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $this->dreams->create($input);
            flash('Dream Saved')->success();
            return redirect()->route('admin.dreams.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read dreams');
        return view('admin.dreams.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update dreams');
        $consumers = $this->consumers->all();
        $donation_categories = $this->donation_categories->all();
        $data = $this->dreams->find($id);
        return view('admin.dreams.edit', compact('data', "consumers", 'donation_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamUpdateRequest $request, $id)
    {
        check_permission('update dreams');
        $this->dreams = $this->dreams->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			$input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $this->dreams->update($input);
            flash('Dream Saved')->success();
            return redirect()->route('admin.dreams.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete dreams');
        try {
            $this->dreams->destroy($id);
            flash('Dream Deleted')->success();
            return redirect()->route('admin.dreams.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
