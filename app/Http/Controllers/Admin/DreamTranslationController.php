<?php

namespace App\Http\Controllers\Admin;

use App\Models\DreamTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamTranslationCreateRequest;
use App\Http\Requests\DreamTranslationUpdateRequest;
use Illuminate\Http\Request;

class DreamTranslationController extends Controller
{
    public $dream_translations;

    public function __construct(DreamTranslation $dream_translations)
    {
        $this->dream_translations = $dream_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->dream_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->dream_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.dream_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dream_translations = $this->dream_translations->all();
        $data = $this->dream_translations;

        return view('admin.dream_translations.create', compact('data', "dream_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->dream_translations->create($input);
            flash('DreamTranslation Saved')->success();
            return redirect()->route('admin.dream_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.dream_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $dream_translations = $this->dream_translations->all();
        $data = $this->dream_translations->find($id);
        return view('admin.dream_translations.edit', compact('data', 'dream_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamTranslationUpdateRequest $request, $id)
    {
        $this->dream_translations = $this->dream_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->dream_translations->update($input);
            flash('DreamTranslation Saved')->success();
            return redirect()->route('admin.dream_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->dream_translations->destroy($id);
            flash('DreamTranslation Deleted')->success();
            return redirect()->route('admin.dream_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
