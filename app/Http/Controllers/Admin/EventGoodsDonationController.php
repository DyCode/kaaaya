<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\Consumer;
use App\Models\Unit;
use App\Models\ShippingMethod;
use App\Models\EventGoodsDonation;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventGoodsDonationCreateRequest;
use App\Http\Requests\EventGoodsDonationUpdateRequest;
use Illuminate\Http\Request;

class EventGoodsDonationController extends Controller
{
    public $event_goods_donations;
    public $events;
    public $consumers;
    public $units;
    public $shipping_methods;

    public function __construct(EventGoodsDonation $event_goods_donations, Event $events, Consumer $consumers, Unit $units, ShippingMethod $shipping_methods)
    {
        $this->event_goods_donations = $event_goods_donations;
        $this->events = $events;
        $this->consumers = $consumers;
        $this->units = $units;
        $this->shipping_methods = $shipping_methods;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu event_goods_donations');
        if ($request->has('search')) {
            $data = $this->event_goods_donations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->event_goods_donations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.event_goods_donations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create event_goods_donations');
        $consumers = $this->consumers->all();
        $events = $this->events->get();
        $units = $this->units->get();
        $shipping_methods = $this->shipping_methods->get();

        return view('admin.event_goods_donations.create', compact('events', "consumers", "units", "shipping_methods"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventGoodsDonationCreateRequest $request)
    {
        check_permission('create event_goods_donations');
        $input = $request->except("_token");
        try {
			if($request->hasFile('goods_needed')) {
			$path = $request->goods_needed->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['goods_needed'] = $path;
			}
			$input['donation_date'] = \Carbon\Carbon::parse($input['donation_date'])->format('Y-m-d H:i:s');
            $this->event_goods_donations->create($input);
            flash('EventGoodsDonation Saved')->success();
            return redirect()->route('admin.event_goods_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read event_goods_donations');
        return view('admin.event_goods_donations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update event_goods_donations');
        $consumers = $this->consumers->all();
        $events = $this->events->get();
        $units = $this->units->get();
        $shipping_methods = $this->shipping_methods->get();
        $data = $this->event_goods_donations->find($id);
        return view('admin.event_goods_donations.edit', compact('data', 'events', 'consumers', 'units', 'shipping_methods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventGoodsDonationUpdateRequest $request, $id)
    {
        check_permission('update event_goods_donations');
        $this->event_goods_donations = $this->event_goods_donations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('goods_needed')) {
			$path = $request->goods_needed->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['goods_needed'] = $path;
			}
			$input['donation_date'] = \Carbon\Carbon::parse($input['donation_date'])->format('Y-m-d H:i:s');
            $this->event_goods_donations->update($input);
            flash('EventGoodsDonation Saved')->success();
            return redirect()->route('admin.event_goods_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete event_goods_donations');
        try {
            $this->event_goods_donations->destroy($id);
            flash('EventGoodsDonation Deleted')->success();
            return redirect()->route('admin.event_goods_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
