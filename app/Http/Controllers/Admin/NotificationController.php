<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\Http\Requests\NotificationCreateRequest;
use App\Http\Requests\NotificationUpdateRequest;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public $notifications;

    public function __construct(Notification $notifications)
    {
        $this->notifications = $notifications;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu notifications');
        if ($request->has('search')) {
            $data = $this->notifications->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->notifications->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.notifications.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create notifications');
        $notifications = $this->notifications->all();
        $data = $this->notifications;

        return view('admin.notifications.create', compact('data', "notifications"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationCreateRequest $request)
    {
        check_permission('create notifications');
        $input = $request->except("_token");
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
            $this->notifications->create($input);
            flash('Notification Saved')->success();
            return redirect()->route('admin.notifications.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read notifications');
        return view('admin.notifications.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update notifications');
        $notifications = $this->notifications->all();
        $data = $this->notifications->find($id);
        return view('admin.notifications.edit', compact('data', 'notifications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationUpdateRequest $request, $id)
    {
        check_permission('update notifications');
        $this->notifications = $this->notifications->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
            $this->notifications->update($input);
            flash('Notification Saved')->success();
            return redirect()->route('admin.notifications.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete notifications');
        try {
            $this->notifications->destroy($id);
            flash('Notification Deleted')->success();
            return redirect()->route('admin.notifications.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
