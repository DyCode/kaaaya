<?php

namespace App\Http\Controllers\Admin;


use App\Models\Consumer;
use App\Models\Story;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryCreateRequest;
use App\Http\Requests\StoryUpdateRequest;
use Illuminate\Http\Request;

class StoryController extends Controller
{
    public $stories;

    public function __construct(Story $stories, Consumer $consumers)
    {
        $this->stories = $stories;
        $this->consumers = $consumers;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu stories');
        if ($request->has('search')) {
            $data = $this->stories->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->stories->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.stories.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create stories');
        $consumers = $this->consumers->all();
        $data = $this->stories;

        return view('admin.stories.create', compact('data', "consumers"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryCreateRequest $request)
    {
        check_permission('create stories');
        $input = $request->except("_token");
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			$input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $this->stories->create($input);
            flash('Story Saved')->success();
            return redirect()->route('admin.stories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read stories');
        return view('admin.stories.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update stories');
        $consumers = $this->consumers->all();
        $data = $this->stories->find($id);
        return view('admin.stories.edit', compact('data', 'consumers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryUpdateRequest $request, $id)
    {
        check_permission('update stories');
        $this->stories = $this->stories->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			$input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $this->stories->update($input);
            flash('Story Saved')->success();
            return redirect()->route('admin.stories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete stories');
        try {
            $this->stories->destroy($id);
            flash('Story Deleted')->success();
            return redirect()->route('admin.stories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
