<?php

namespace App\Http\Controllers\Admin;

use App\Models\AboutUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\AboutUsCreateRequest;
use App\Http\Requests\AboutUsUpdateRequest;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public $about_uses;

    public function __construct(AboutUs $about_uses)
    {
        $this->about_uses = $about_uses;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        check_permission('menu about_uses');
        if ($request->has('search')) {
            $data = $this->about_uses->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->about_uses->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.about_uses.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create about_uses');
        $about_uses = $this->about_uses->all();
        $data = $this->about_uses;

        return view('admin.about_uses.create', compact('data', "about_uses"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutUsCreateRequest $request)
    {
        check_permission('create about_uses');
        disable_active_flag($this->about_uses);
        $input = $request->except("_token");
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			if($request->hasFile('who_we_are_image')) {
			$path = $request->who_we_are_image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['who_we_are_image'] = $path;
			}
            $this->about_uses->create($input);
            flash('AboutUs Saved')->success();
            return redirect()->route('admin.about_uses.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read about_uses');
        return view('admin.about_uses.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        check_permission('update about_uses');
        $about_uses = $this->about_uses->all();
        $data = $this->about_uses->find($id);
        return view('admin.about_uses.edit', compact('data', 'about_uses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutUsUpdateRequest $request, $id)
    {
        check_permission('update about_uses');
        disable_active_flag($this->about_uses);
        
        $this->about_uses = $this->about_uses->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			if($request->hasFile('who_we_are_image')) {
			$path = $request->who_we_are_image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['who_we_are_image'] = $path;
			}
            $this->about_uses->update($input);
            flash('AboutUs Saved')->success();
            return redirect()->route('admin.about_uses.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete about_uses');
        try {
            $this->about_uses->destroy($id);
            flash('AboutUs Deleted')->success();
            return redirect()->route('admin.about_uses.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
