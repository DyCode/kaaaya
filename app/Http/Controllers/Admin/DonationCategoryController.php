<?php

namespace App\Http\Controllers\Admin;

use App\Models\DonationCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationCategoryCreateRequest;
use App\Http\Requests\DonationCategoryUpdateRequest;
use Illuminate\Http\Request;

class DonationCategoryController extends Controller
{
    public $donation_categories;

    public function __construct(DonationCategory $donation_categories)
    {
        $this->donation_categories = $donation_categories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu donation_categories');
        if ($request->has('search')) {
            $data = $this->donation_categories->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->donation_categories->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.donation_categories.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create donation_categories');
        $donation_categories = $this->donation_categories->all();
        $data = $this->donation_categories;

        return view('admin.donation_categories.create', compact('data', "donation_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationCategoryCreateRequest $request)
    {
        check_permission('create donation_categories');
        $input = $request->except("_token");
        try {
			if($request->hasFile('category_icon')) {
			$path = $request->category_icon->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['category_icon'] = $path;
			}
            $this->donation_categories->create($input);
            flash('DonationCategory Saved')->success();
            return redirect()->route('admin.donation_categories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read donation_categories');
        return view('admin.donation_categories.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update donation_categories');
        $donation_categories = $this->donation_categories->all();
        $data = $this->donation_categories->find($id);
        return view('admin.donation_categories.edit', compact('data', 'donation_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonationCategoryUpdateRequest $request, $id)
    {
        check_permission('update donation_categories');
        $this->donation_categories = $this->donation_categories->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('category_icon')) {
			$path = $request->category_icon->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['category_icon'] = $path;
			}
            $this->donation_categories->update($input);
            flash('DonationCategory Saved')->success();
            return redirect()->route('admin.donation_categories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete donation_categories');
        try {
            $this->donation_categories->destroy($id);
            flash('DonationCategory Deleted')->success();
            return redirect()->route('admin.donation_categories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
