<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\DonationCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryCreateRequest;
use App\Http\Requests\GalleryUpdateRequest;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public $galleries;

    public function __construct(Gallery $galleries, DonationCategory $donation_categories)
    {
        $this->galleries = $galleries;
        $this->donation_categories = $donation_categories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu galleries');
        if ($request->has('search')) {
            $data = $this->galleries->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->galleries->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.galleries.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create galleries');
        $donation_categories = $this->donation_categories->all();
        $data = $this->galleries;

        return view('admin.galleries.create', compact('data', "donation_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryCreateRequest $request)
    {
        check_permission('create galleries');
        $input = $request->except("_token");
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images/gallery', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->galleries->create($input);
            flash('Gallery Saved')->success();
            return redirect()->route('admin.galleries.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read galleries');
        return view('admin.galleries.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update galleries');
        $galleries = $this->galleries->all();
        $data = $this->galleries->find($id);
        return view('admin.galleries.edit', compact('data', 'galleries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryUpdateRequest $request, $id)
    {
        check_permission('update galleries');
        $this->galleries = $this->galleries->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images/gallery', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->galleries->update($input);
            flash('Gallery Saved')->success();
            return redirect()->route('admin.galleries.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete galleries');
        try {
            $this->galleries->destroy($id);
            flash('Gallery Deleted')->success();
            return redirect()->route('admin.galleries.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
