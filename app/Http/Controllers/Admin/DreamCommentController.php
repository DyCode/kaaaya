<?php

namespace App\Http\Controllers\Admin;

use App\Models\DreamComment;
use App\Models\Dream;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamCommentCreateRequest;
use App\Http\Requests\DreamCommentUpdateRequest;
use Illuminate\Http\Request;

class DreamCommentController extends Controller
{
    public $dream_comments;
    public $dreams;
    public $consumers;

    public function __construct(DreamComment $dream_comments, Dream $dreams, Consumer $consumers)
    {
        $this->dream_comments = $dream_comments;
        $this->dreams = $dreams;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu dream_comments');
        if ($request->has('search')) {
            $data = $this->dream_comments->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->dream_comments->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.dream_comments.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create dream_comments');
        $consumers = $this->consumers->all();
        $headers = $this->dream_comments->whereNull('reply_header_id')->get();
        $dreams = $this->dreams->get();

        return view('admin.dream_comments.create', compact('consumers', "headers", "dreams"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamCommentCreateRequest $request)
    {
        check_permission('create dream_comments');
        $input = $request->except("_token");
        try {
            $this->dream_comments->create($input);
            flash('DreamComment Saved')->success();
            return redirect()->route('admin.dream_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read dream_comments');
        return view('admin.dream_comments.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        check_permission('update dream_comments');
        $consumers = $this->consumers->all();
        $headers = $this->dream_comments->whereNull('reply_header_id')->get();
        $dreams = $this->dreams->get();
        $data = $this->dream_comments->find($id);
        return view('admin.dream_comments.edit', compact('consumers', 'headers', 'dreams', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamCommentUpdateRequest $request, $id)
    {
        check_permission('update dream_comments');
        $this->dream_comments = $this->dream_comments->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->dream_comments->update($input);
            flash('DreamComment Saved')->success();
            return redirect()->route('admin.dream_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete dream_comments');
        try {
            $this->dream_comments->destroy($id);
            flash('DreamComment Deleted')->success();
            return redirect()->route('admin.dream_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
