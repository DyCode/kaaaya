<?php

namespace App\Http\Controllers\Admin;

use App\Models\FaqTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqTranslationCreateRequest;
use App\Http\Requests\FaqTranslationUpdateRequest;
use Illuminate\Http\Request;

class FaqTranslationController extends Controller
{
    public $faq_translations;

    public function __construct(FaqTranslation $faq_translations)
    {
        $this->faq_translations = $faq_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->faq_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->faq_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.faq_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faq_translations = $this->faq_translations->all();
        $data = $this->faq_translations;

        return view('admin.faq_translations.create', compact('data', "faq_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->faq_translations->create($input);
            flash('FaqTranslation Saved')->success();
            return redirect()->route('admin.faq_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.faq_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $faq_translations = $this->faq_translations->all();
        $data = $this->faq_translations->find($id);
        return view('admin.faq_translations.edit', compact('data', 'faq_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqTranslationUpdateRequest $request, $id)
    {
        $this->faq_translations = $this->faq_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->faq_translations->update($input);
            flash('FaqTranslation Saved')->success();
            return redirect()->route('admin.faq_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->faq_translations->destroy($id);
            flash('FaqTranslation Deleted')->success();
            return redirect()->route('admin.faq_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
