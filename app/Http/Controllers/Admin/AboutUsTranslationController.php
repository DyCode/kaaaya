<?php

namespace App\Http\Controllers\Admin;

use App\Models\AboutUsTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\AboutUsTranslationCreateRequest;
use App\Http\Requests\AboutUsTranslationUpdateRequest;
use Illuminate\Http\Request;

class AboutUsTranslationController extends Controller
{
    public $about_us_translations;

    public function __construct(AboutUsTranslation $about_us_translations)
    {
        $this->about_us_translations = $about_us_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->about_us_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->about_us_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.about_us_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $about_us_translations = $this->about_us_translations->all();
        $data = $this->about_us_translations;

        return view('admin.about_us_translations.create', compact('data', "about_us_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutUsTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->about_us_translations->create($input);
            flash('AboutUsTranslation Saved')->success();
            return redirect()->route('admin.about_us_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.about_us_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $about_us_translations = $this->about_us_translations->all();
        $data = $this->about_us_translations->find($id);
        return view('admin.about_us_translations.edit', compact('data', 'about_us_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutUsTranslationUpdateRequest $request, $id)
    {
        $this->about_us_translations = $this->about_us_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->about_us_translations->update($input);
            flash('AboutUsTranslation Saved')->success();
            return redirect()->route('admin.about_us_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->about_us_translations->destroy($id);
            flash('AboutUsTranslation Deleted')->success();
            return redirect()->route('admin.about_us_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
