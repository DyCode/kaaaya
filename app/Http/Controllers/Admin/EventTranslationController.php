<?php

namespace App\Http\Controllers\Admin;

use App\Models\EventTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventTranslationCreateRequest;
use App\Http\Requests\EventTranslationUpdateRequest;
use Illuminate\Http\Request;

class EventTranslationController extends Controller
{
    public $event_translations;

    public function __construct(EventTranslation $event_translations)
    {
        $this->event_translations = $event_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->event_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->event_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.event_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event_translations = $this->event_translations->all();
        $data = $this->event_translations;

        return view('admin.event_translations.create', compact('data', "event_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->event_translations->create($input);
            flash('EventTranslation Saved')->success();
            return redirect()->route('admin.event_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.event_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event_translations = $this->event_translations->all();
        $data = $this->event_translations->find($id);
        return view('admin.event_translations.edit', compact('data', 'event_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventTranslationUpdateRequest $request, $id)
    {
        $this->event_translations = $this->event_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->event_translations->update($input);
            flash('EventTranslation Saved')->success();
            return redirect()->route('admin.event_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->event_translations->destroy($id);
            flash('EventTranslation Deleted')->success();
            return redirect()->route('admin.event_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
