<?php

namespace App\Http\Controllers\Admin;

use App\Models\DreamGoodsDonation;
use App\Models\Dream;
use App\Models\Unit;
use App\Models\ShippingMethod;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamGoodsDonationCreateRequest;
use App\Http\Requests\DreamGoodsDonationUpdateRequest;
use Illuminate\Http\Request;

class DreamGoodsDonationController extends Controller
{
    public $dream_goods_donations;
    public $dreams;
    public $consumers;
    public $units;
    public $shipping_methods;

    public function __construct(DreamGoodsDonation $dream_goods_donations, Dream $dreams, Consumer $consumers, Unit $units, ShippingMethod $shipping_methods)
    {
        $this->dream_goods_donations = $dream_goods_donations;
        $this->dreams = $dreams;
        $this->consumers = $consumers;
        $this->units = $units;
        $this->shipping_methods = $shipping_methods;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        check_permission('menu dream_goods_donations');
        if ($request->has('search')) {
            $data = $this->dream_goods_donations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->dream_goods_donations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.dream_goods_donations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create dream_goods_donations');
        $consumers = $this->consumers->all();
        $dreams = $this->dreams->get();
        $units = $this->units->get();
        $shipping_methods = $this->shipping_methods->get();

        return view('admin.dream_goods_donations.create', compact('dreams', "consumers", "units", "shipping_methods"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamGoodsDonationCreateRequest $request)
    {
        check_permission('create dream_goods_donations');
        $input = $request->except("_token");
        try {
			if($request->hasFile('goods_needed')) {
			$path = $request->goods_needed->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['goods_needed'] = $path;
			}
			$input['donation_date'] = \Carbon\Carbon::parse($input['donation_date'])->format('Y-m-d H:i:s');
            $this->dream_goods_donations->create($input);
            flash('DreamGoodsDonation Saved')->success();
            return redirect()->route('admin.dream_goods_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read dream_goods_donations');
        return view('admin.dream_goods_donations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update dream_goods_donations');
        $consumers = $this->consumers->all();
        $dreams = $this->dreams->get();
        $units = $this->units->get();
        $shipping_methods = $this->shipping_methods->get();
        $data = $this->dream_goods_donations->find($id);
        return view('admin.dream_goods_donations.edit', compact('data', 'dreams', 'consumers', 'units', 'shipping_methods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamGoodsDonationUpdateRequest $request, $id)
    {
        check_permission('update dream_goods_donations');
        $this->dream_goods_donations = $this->dream_goods_donations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('goods_needed')) {
			$path = $request->goods_needed->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['goods_needed'] = $path;
			}
			$input['donation_date'] = \Carbon\Carbon::parse($input['donation_date'])->format('Y-m-d H:i:s');
            $this->dream_goods_donations->update($input);
            flash('DreamGoodsDonation Saved')->success();
            return redirect()->route('admin.dream_goods_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete dream_goods_donations');
        try {
            $this->dream_goods_donations->destroy($id);
            flash('DreamGoodsDonation Deleted')->success();
            return redirect()->route('admin.dream_goods_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
