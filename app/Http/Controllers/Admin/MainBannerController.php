<?php

namespace App\Http\Controllers\Admin;

use App\Models\MainBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\MainBannerCreateRequest;
use App\Http\Requests\MainBannerUpdateRequest;
use Illuminate\Http\Request;

class MainBannerController extends Controller
{
    public $main_banners;

    public function __construct(MainBanner $main_banners)
    {
        $this->main_banners = $main_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu main_banners');
        if ($request->has('search')) {
            $data = $this->main_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->main_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.main_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create main_banners');
        $main_banners = $this->main_banners->all();
        $data = $this->main_banners;

        return view('admin.main_banners.create', compact('data', "main_banners"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MainBannerCreateRequest $request)
    {
        check_permission('create main_banners');
        $input = $request->except("_token");
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->main_banners->create($input);
            flash('MainBanner Saved')->success();
            return redirect()->route('admin.main_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read main_banners');
        return view('admin.main_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update main_banners');
        $main_banners = $this->main_banners->all();
        $data = $this->main_banners->find($id);
        return view('admin.main_banners.edit', compact('data', 'main_banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MainBannerUpdateRequest $request, $id)
    {
        check_permission('update main_banners');
        $this->main_banners = $this->main_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->main_banners->update($input);
            flash('MainBanner Saved')->success();
            return redirect()->route('admin.main_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete main_banners');
        try {
            $this->main_banners->destroy($id);
            flash('MainBanner Deleted')->success();
            return redirect()->route('admin.main_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
