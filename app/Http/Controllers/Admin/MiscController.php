<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class MiscController extends Controller
{
	function __construct(Role $roles, Permission $permissions)
	{
		$this->roles = $roles;
		$this->permissions = $permissions;
	}

    public function permissions()
    {
    	check_permission('menu permissions');
    	$roles = $this->roles->all();
    	return view('admin.misc.permissions', compact('roles'));
    }

    public function permissions_create()
    {
    	check_permission('add permissions');
    	$permissions = $this->permissions->all();
    	return view('admin.misc.permissions_create', compact('permissions'));
    }

    public function permissions_store(Request $request)
    {	
    	check_permission('add permissions');
    	$input = $request->only(['name']);
    	try {
        	
	    	$role = $this->roles->create($input);

	    	$all_permissions = $this->permissions->get();
	    	$permissions = collect($request->get('permission', []));
	    	$permissions = $permissions->map(function($d, $i) { return $i; })->values()->toArray();
	    	$all_permissions->each(function($d) use($permissions, $role){
	    		if(in_array($d->name, $permissions)) {
	    			$role->givePermissionTo($d->name);
	    		} else {
	    			$role->revokePermissionTo($d->name);
	    		}
	    	});
	    	flash('Permission Saved')->success();
            return redirect()->route('admin.permissions.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    public function permissions_update(Request $request, $id)
    {
    	check_permission('edit permissions');
		$input = $request->only(['name']);

        try {
        	if ($id == 1) {
				throw new \Exception("Default Role Cannot Updated");
			}
	    	$all_permissions = $this->permissions->get();
	    	$permissions = collect($request->get('permission', []));
	    	$permissions = $permissions->map(function($d, $i) { return $i; })->values()->toArray();
	    	$role = $this->roles->findOrFail($id);
	    	$all_permissions->each(function($d) use($permissions, $role){
	    		if(in_array($d->name, $permissions)) {
	    			$role->givePermissionTo($d->name);
	    		} else {
	    			$role->revokePermissionTo($d->name);
	    		}
	    	});
	    	$role->update($input);
	    	flash('Permission Updated')->success();
            return redirect()->route('admin.permissions.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    public function permissions_edit($id)
    {
    	check_permission('add permissions');
    	$data = $this->roles->findOrFail($id);
    	$permissions = $this->permissions->all();
    	return view('admin.misc.permissions_edit', compact('data', 'permissions'));
    }

    public function permissions_destroy($id)
    {
    	check_permission('delete permissions');
    	try {
    		if ($id == 1) {
				throw new \Exception("Default Role Cannot Deleted");
			}
            $this->roles->destroy($id);
            flash('Role Deleted')->success();
            return redirect()->route('admin.permissions.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
