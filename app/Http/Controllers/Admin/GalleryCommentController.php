<?php

namespace App\Http\Controllers\Admin;

use App\Models\GalleryComment;
use App\Models\Gallery;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryCommentCreateRequest;
use App\Http\Requests\GalleryCommentUpdateRequest;
use Illuminate\Http\Request;

class GalleryCommentController extends Controller
{
    public $gallery_comments;
    public $galleries;
    public $consumers;

    public function __construct(GalleryComment $gallery_comments, Gallery $galleries, Consumer $consumers)
    {
        $this->gallery_comments = $gallery_comments;
        $this->galleries = $galleries;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu gallery_comments');
        if ($request->has('search')) {
            $data = $this->gallery_comments->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->gallery_comments->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.gallery_comments.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create gallery_comments');
        $consumers = $this->consumers->all();
        $headers = $this->gallery_comments->whereNull('reply_header_id')->get();
        $galleries = $this->galleries->get();

        return view('admin.gallery_comments.create', compact('consumers', "headers", "galleries"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryCommentCreateRequest $request)
    {
        check_permission('create gallery_comments');
        $input = $request->except("_token");
        try {
            $this->gallery_comments->create($input);
            flash('GalleryComment Saved')->success();
            return redirect()->route('admin.gallery_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read gallery_comments');
        return view('admin.gallery_comments.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update gallery_comments');
        $consumers = $this->consumers->all();
        $headers = $this->gallery_comments->whereNull('reply_header_id')->get();
        $galleries = $this->galleries->get();
        $data = $this->gallery_comments->find($id);
        return view('admin.gallery_comments.edit', compact('consumers', 'headers', 'galleries', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryCommentUpdateRequest $request, $id)
    {
        check_permission('update gallery_comments');
        $this->gallery_comments = $this->gallery_comments->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->gallery_comments->update($input);
            flash('GalleryComment Saved')->success();
            return redirect()->route('admin.gallery_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete gallery_comments');
        try {
            $this->gallery_comments->destroy($id);
            flash('GalleryComment Deleted')->success();
            return redirect()->route('admin.gallery_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
