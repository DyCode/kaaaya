<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\DonationCategory;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventCreateRequest;
use App\Http\Requests\EventUpdateRequest;
use Illuminate\Http\Request;
use DB;
class EventController extends Controller
{
    public $events;

    public function __construct(Event $events, DonationCategory $donation_categories, Consumer $consumers)
    {
        $this->events = $events;
        $this->donation_categories = $donation_categories;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu events');
        if ($request->has('search')) {
            $data = $this->events->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->events->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.events.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create events');
        $consumers = $this->consumers->all();
        $donation_categories = $this->donation_categories->all();
        $data = $this->events;

        return view('admin.events.create', compact('data', "consumers", 'donation_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventCreateRequest $request)
    {
        check_permission('create events');
        $input = $request->except("_token");
        DB::beginTransaction();
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			$input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $this->events->create($input);
            flash('Event Saved')->success();
            DB::commit();
            return redirect()->route('admin.events.index');
        } catch (\Exception $e) {
            DB::rollBack();
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read events');
        return view('admin.events.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update events');
        $consumers = $this->consumers->all();
        $donation_categories = $this->donation_categories->all();
        $data = $this->events->find($id);
        return view('admin.events.edit', compact('data', 'consumers', 'donation_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventUpdateRequest $request, $id)
    {
        check_permission('update events');
        $this->events = $this->events->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
			$input['due_date'] = \Carbon\Carbon::parse($input['due_date'])->format('Y-m-d H:i:s');
            $this->events->update($input);
            flash('Event Saved')->success();
            return redirect()->route('admin.events.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete events');
        try {
            $this->events->destroy($id);
            flash('Event Deleted')->success();
            return redirect()->route('admin.events.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
