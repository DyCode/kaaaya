<?php

namespace App\Http\Controllers\Admin;

use App\Models\StoryComment;
use App\Models\Story;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoryCommentCreateRequest;
use App\Http\Requests\StoryCommentUpdateRequest;
use Illuminate\Http\Request;

class StoryCommentController extends Controller
{
    public $story_comments;
    public $stories;
    public $consumers;

    public function __construct(StoryComment $story_comments, Story $stories, Consumer $consumers)
    {
        $this->story_comments = $story_comments;
        $this->stories = $stories;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu story_comments');
        if ($request->has('search')) {
            $data = $this->story_comments->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->story_comments->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.story_comments.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create story_comments');
        $consumers = $this->consumers->all();
        $headers = $this->story_comments->whereNull('reply_header_id')->get();
        $stories = $this->stories->get();
        return view('admin.story_comments.create', compact('stories', 'consumers', 'headers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryCommentCreateRequest $request)
    {
        check_permission('create story_comments');
        $input = $request->except("_token");
        try {
            $this->story_comments->create($input);
            flash('StoryComment Saved')->success();
            return redirect()->route('admin.story_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read story_comments');
        return view('admin.story_comments.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update story_comments');
        $consumers = $this->consumers->all();
        $stories = $this->stories->get();
        $data = $this->story_comments->find($id);
        $headers = $this->story_comments->whereNull('reply_header_id')->get();

        return view('admin.story_comments.edit', compact('data', 'consumers', 'stories', 'headers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryCommentUpdateRequest $request, $id)
    {
        check_permission('update story_comments');
        $this->story_comments = $this->story_comments->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->story_comments->update($input);
            flash('StoryComment Saved')->success();
            return redirect()->route('admin.story_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete story_comments');
        try {
            $this->story_comments->destroy($id);
            flash('StoryComment Deleted')->success();
            return redirect()->route('admin.story_comments.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
