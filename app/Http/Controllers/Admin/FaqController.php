<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use App\Models\FaqCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqCreateRequest;
use App\Http\Requests\FaqUpdateRequest;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public $faqs;

    public function __construct(Faq $faqs, FaqCategory $faq_categories)
    {
        $this->faqs = $faqs;
        $this->faq_categories = $faq_categories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu faqs');
        if ($request->has('search')) {
            $data = $this->faqs->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->faqs->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.faqs.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create faqs');
        $faq_categories = $this->faq_categories->all();
        $data = $this->faqs;

        return view('admin.faqs.create', compact('data', "faq_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqCreateRequest $request)
    {
        check_permission('create faqs');
        $input = $request->except("_token");
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->faqs->create($input);
            flash('Faq Saved')->success();
            return redirect()->route('admin.faqs.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read faqs');
        return view('admin.faqs.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update faqs');
        $faqs = $this->faqs->all();
        $data = $this->faqs->find($id);
        return view('admin.faqs.edit', compact('data', 'faqs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqUpdateRequest $request, $id)
    {
        check_permission('update faqs');
        $this->faqs = $this->faqs->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->faqs->update($input);
            flash('Faq Saved')->success();
            return redirect()->route('admin.faqs.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete faqs');
        try {
            $this->faqs->destroy($id);
            flash('Faq Deleted')->success();
            return redirect()->route('admin.faqs.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
