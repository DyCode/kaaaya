<?php

namespace App\Http\Controllers\Admin;

use App\Models\DreamDonation;
use App\Models\Dream;
use App\Models\Consumer;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamDonationCreateRequest;
use App\Http\Requests\DreamDonationUpdateRequest;
use Illuminate\Http\Request;

class DreamDonationController extends Controller
{
    public $dream_donations;
    public $dreams;
    public $consumers;

    public function __construct(DreamDonation $dream_donations, Dream $dreams, Consumer $consumers)
    {
        $this->dream_donations = $dream_donations;
        $this->dreams = $dreams;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.  
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu dream_donations');
        if ($request->has('search')) {
            $data = $this->dream_donations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->dream_donations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.dream_donations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create dream_donations');
        $consumers = $this->consumers->all();
        $dreams = $this->dreams->get();

        return view('admin.dream_donations.create', compact('consumers', "dreams"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamDonationCreateRequest $request)
    {
        check_permission('create dream_donations');
        $input = $request->except("_token");
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->dream_donations->create($input);
            flash('DreamDonation Saved')->success();
            return redirect()->route('admin.dream_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read dream_donations');
        return view('admin.dream_donations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update dream_donations');
        $consumers = $this->consumers->all();
        $dreams = $this->dreams->get();
        $data = $this->dream_donations->find($id);
        return view('admin.dream_donations.edit', compact('data', 'consumers', 'dreams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamDonationUpdateRequest $request, $id)
    {
        check_permission('update dream_donations');
        $this->dream_donations = $this->dream_donations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->dream_donations->update($input);
            flash('DreamDonation Saved')->success();
            return redirect()->route('admin.dream_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete dream_donations');
        try {
            $this->dream_donations->destroy($id);
            flash('DreamDonation Deleted')->success();
            return redirect()->route('admin.dream_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
