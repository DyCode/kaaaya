<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\Consumer;
use App\Models\EventDonation;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventDonationCreateRequest;
use App\Http\Requests\EventDonationUpdateRequest;
use Illuminate\Http\Request;

class EventDonationController extends Controller
{
    public $event_donations;
    public $events;
    public $consumers;

    public function __construct(EventDonation $event_donations, Event $events, Consumer $consumers)
    {
        $this->event_donations = $event_donations;
        $this->events = $events;
        $this->consumers = $consumers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu event_donations');
        if ($request->has('search')) {
            $data = $this->event_donations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->event_donations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.event_donations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create event_donations');
        $consumers = $this->consumers->all();
        $events = $this->events->get();

        return view('admin.event_donations.create', compact('events', "consumers"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventDonationCreateRequest $request)
    {
        check_permission('create event_donations');
        $input = $request->except("_token");
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->event_donations->create($input);
            flash('EventDonation Saved')->success();
            return redirect()->route('admin.event_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read event_donations');
        return view('admin.event_donations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update event_donations');
        $consumers = $this->consumers->all();
        $events = $this->events->get();
        $data = $this->event_donations->find($id);
        return view('admin.event_donations.edit', compact('data', 'events', 'consumers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventDonationUpdateRequest $request, $id)
    {
        check_permission('update event_donations');
        $this->event_donations = $this->event_donations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			$input['date'] = \Carbon\Carbon::parse($input['date'])->format('Y-m-d H:i:s');
			if($request->hasFile('transfer_slip')) {
			$path = $request->transfer_slip->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['transfer_slip'] = $path;
			}
            $this->event_donations->update($input);
            flash('EventDonation Saved')->success();
            return redirect()->route('admin.event_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete event_donations');
        try {
            $this->event_donations->destroy($id);
            flash('EventDonation Deleted')->success();
            return redirect()->route('admin.event_donations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
