<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{

    public $users;

    public function __construct(User $users, Role $roles, Permission $permissions)
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->permissions = $permissions;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        check_permission('menu users');

        if ($request->has('search')) {
            $data = $this->users->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->users->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.users.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('add users');

        $roles = $this->roles->all();
        return view('admin.users.create', compact('data', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        check_permission('add users');

        $input = $request->except(["_token", "_method", "role_id"]);
        try {
            $input['password'] = bcrypt($input['password']);
            $users = $this->users->create($input);
            $users->assignRole($request->get('role_id'));
            flash('User Saved')->success();
            return redirect()->route('admin.users.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        check_permission('edit users');
        $data = $this->users->find($id);
        $user_role = $data->roles->first();
        $roles = $this->roles->all();
        return view('admin.users.edit', compact('data', 'user_role', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        check_permission('edit users');
        $users = $this->users->findOrFail($id);
        $input = $request->except(["_token", "_method", "role_id"]);

        try {
            if ($users->roles->first()) {
                $old_role =$users->roles->first()->name;
                $users->removeRole($old_role);
            }
            $users->assignRole($request->get('role_id'));
            $users->update($input);
            flash('User Saved')->success();
            return redirect()->route('admin.users.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete users');
        try {
            $this->users->destroy($id);
            flash('User Deleted')->success();
            return redirect()->route('admin.users.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
