<?php

namespace App\Http\Controllers\Admin;

use App\Models\DonationCategoryBanner;
use App\Models\DonationCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationCategoryBannerCreateRequest;
use App\Http\Requests\DonationCategoryBannerUpdateRequest;
use Illuminate\Http\Request;

class DonationCategoryBannerController extends Controller
{
    public $donation_category_banners;
    public $donation_categories;

    public function __construct(DonationCategoryBanner $donation_category_banners, DonationCategory $donation_categories)
    {
        $this->donation_category_banners = $donation_category_banners;
        $this->donation_categories = $donation_categories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu donation_category_banners');
        if ($request->has('search')) {
            $data = $this->donation_category_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->donation_category_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.donation_category_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create donation_category_banners');
        $donation_categories = $this->donation_categories->all();

        return view('admin.donation_category_banners.create', compact("donation_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationCategoryBannerCreateRequest $request)
    {
        check_permission('create donation_category_banners');
        disable_active_flag($this->donation_category_banners);
        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->donation_category_banners->create($input);
            flash('DonationCategoryBanner Saved')->success();
            return redirect()->route('admin.donation_category_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read donation_category_banners');
        return view('admin.donation_category_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update donation_category_banners');
        $donation_categories = $this->donation_categories->all();
        $data = $this->donation_category_banners->find($id);
        return view('admin.donation_category_banners.edit', compact('data', 'donation_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonationCategoryBannerUpdateRequest $request, $id)
    {
        check_permission('update donation_category_banners');
        disable_active_flag($this->donation_category_banners);
        $this->donation_category_banners = $this->donation_category_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->donation_category_banners->update($input);
            flash('DonationCategoryBanner Saved')->success();
            return redirect()->route('admin.donation_category_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete donation_category_banners');
        try {
            $this->donation_category_banners->destroy($id);
            flash('DonationCategoryBanner Deleted')->success();
            return redirect()->route('admin.donation_category_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
