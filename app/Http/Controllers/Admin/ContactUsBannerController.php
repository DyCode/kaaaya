<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContactUsBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsBannerCreateRequest;
use App\Http\Requests\ContactUsBannerUpdateRequest;
use Illuminate\Http\Request;


class ContactUsBannerController extends Controller
{
    public $contact_us_banners;

    public function __construct(ContactUsBanner $contact_us_banners)
    {
        $this->contact_us_banners = $contact_us_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu contact_us_banners');
        if ($request->has('search')) {
            $data = $this->contact_us_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->contact_us_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.contact_us_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create contact_us_banners');

        return view('admin.contact_us_banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactUsBannerCreateRequest $request)
    {
        check_permission('create contact_us_banners');
        disable_active_flag($this->contact_us_banners);
        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->contact_us_banners->create($input);
            flash('ContactUsBanner Saved')->success();
            return redirect()->route('admin.contact_us_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read contact_us_banners');
        return view('admin.contact_us_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update contact_us_banners');
        $data = $this->contact_us_banners->find($id);
        return view('admin.contact_us_banners.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactUsBannerUpdateRequest $request, $id)
    {
        check_permission('update contact_us_banners');
        disable_active_flag($this->contact_us_banners);
        $this->contact_us_banners = $this->contact_us_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->contact_us_banners->update($input);
            flash('ContactUsBanner Saved')->success();
            return redirect()->route('admin.contact_us_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete contact_us_banners');
        try {
            $this->contact_us_banners->destroy($id);
            flash('ContactUsBanner Deleted')->success();
            return redirect()->route('admin.contact_us_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
