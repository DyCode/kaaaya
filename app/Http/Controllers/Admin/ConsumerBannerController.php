<?php

namespace App\Http\Controllers\Admin;

use App\Models\ConsumerBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConsumerBannerCreateRequest;
use App\Http\Requests\ConsumerBannerUpdateRequest;
use Illuminate\Http\Request;

class ConsumerBannerController extends Controller
{
    public $consumer_banners;

    public function __construct(ConsumerBanner $consumer_banners)
    {
        $this->consumer_banners = $consumer_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu consumer_banners');
        if ($request->has('search')) {
            $data = $this->consumer_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->consumer_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.consumer_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create consumer_banners');

        return view('admin.consumer_banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConsumerBannerCreateRequest $request)
    {
        check_permission('create consumer_banners');
        disable_active_flag($this->consumer_banners);
        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->consumer_banners->create($input);
            flash('ConsumerBanner Saved')->success();
            return redirect()->route('admin.consumer_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read consumer_banners');
        return view('admin.consumer_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update consumer_banners');
        $data = $this->consumer_banners->find($id);
        return view('admin.consumer_banners.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConsumerBannerUpdateRequest $request, $id)
    {
        check_permission('update consumer_banners');
        disable_active_flag($this->consumer_banners);
        $this->consumer_banners = $this->consumer_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->consumer_banners->update($input);
            flash('ConsumerBanner Saved')->success();
            return redirect()->route('admin.consumer_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete consumer_banners');
        try {
            $this->consumer_banners->destroy($id);
            flash('ConsumerBanner Deleted')->success();
            return redirect()->route('admin.consumer_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
