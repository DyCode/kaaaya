<?php

namespace App\Http\Controllers\Admin;

use App\Models\FaqBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqBannerCreateRequest;
use App\Http\Requests\FaqBannerUpdateRequest;
use Illuminate\Http\Request;

class FaqBannerController extends Controller
{
    public $faq_banners;

    public function __construct(FaqBanner $faq_banners)
    {
        $this->faq_banners = $faq_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu faq_banners');
        if ($request->has('search')) {
            $data = $this->faq_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->faq_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.faq_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create faq_banners');

        return view('admin.faq_banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqBannerCreateRequest $request)
    {
        check_permission('create faq_banners');
        disable_active_flag($this->faq_banners);
        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->faq_banners->create($input);
            flash('FaqBanner Saved')->success();
            return redirect()->route('admin.faq_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read faq_banners');
        return view('admin.faq_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update faq_banners');
        $data = $this->faq_banners->find($id);
        return view('admin.faq_banners.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqBannerUpdateRequest $request, $id)
    {
        check_permission('update faq_banners');
        disable_active_flag($this->faq_banners);
        $this->faq_banners = $this->faq_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->faq_banners->update($input);
            flash('FaqBanner Saved')->success();
            return redirect()->route('admin.faq_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete faq_banners');
        try {
            $this->faq_banners->destroy($id);
            flash('FaqBanner Deleted')->success();
            return redirect()->route('admin.faq_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
