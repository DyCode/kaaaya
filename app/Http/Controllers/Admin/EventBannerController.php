<?php

namespace App\Http\Controllers\Admin;

use App\Models\EventBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventBannerCreateRequest;
use App\Http\Requests\EventBannerUpdateRequest;
use Illuminate\Http\Request;

class EventBannerController extends Controller
{
    public $event_banners;

    public function __construct(EventBanner $event_banners)
    {
        $this->event_banners = $event_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu event_banners');
        if ($request->has('search')) {
            $data = $this->event_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->event_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.event_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create event_banners');
        $event_banners = $this->event_banners->all();
        $data = $this->event_banners;

        return view('admin.event_banners.create', compact('data', "event_banners"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventBannerCreateRequest $request)
    {
        check_permission('create event_banners');
        disable_active_flag($this->event_banners);
        $input = $request->except("_token");
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images/banner', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->event_banners->create($input);
            flash('EventBanner Saved')->success();
            return redirect()->route('admin.event_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read event_banners');
        return view('admin.event_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update event_banners');
        $event_banners = $this->event_banners->all();
        $data = $this->event_banners->find($id);
        return view('admin.event_banners.edit', compact('data', 'event_banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventBannerUpdateRequest $request, $id)
    {
        check_permission('update event_banners');
        disable_active_flag($this->event_banners);
        
        $this->event_banners = $this->event_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images/banner', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->event_banners->update($input);
            flash('EventBanner Saved')->success();
            return redirect()->route('admin.event_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete event_banners');
        try {
            $this->event_banners->destroy($id);
            flash('EventBanner Deleted')->success();
            return redirect()->route('admin.event_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
