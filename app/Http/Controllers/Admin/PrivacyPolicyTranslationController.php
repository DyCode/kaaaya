<?php

namespace App\Http\Controllers\Admin;

use App\Models\PrivacyPolicyTranslation;
use App\Http\Controllers\Controller;
use App\Http\Requests\PrivacyPolicyTranslationCreateRequest;
use App\Http\Requests\PrivacyPolicyTranslationUpdateRequest;
use Illuminate\Http\Request;

class PrivacyPolicyTranslationController extends Controller
{
    public $privacy_policy_translations;

    public function __construct(PrivacyPolicyTranslation $privacy_policy_translations)
    {
        $this->privacy_policy_translations = $privacy_policy_translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search')) {
            $data = $this->privacy_policy_translations->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->privacy_policy_translations->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.privacy_policy_translations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $privacy_policy_translations = $this->privacy_policy_translations->all();
        $data = $this->privacy_policy_translations;

        return view('admin.privacy_policy_translations.create', compact('data', "privacy_policy_translations"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyTranslationCreateRequest $request)
    {
        $input = $request->except("_token");
        try {
            $this->privacy_policy_translations->create($input);
            flash('PrivacyPolicyTranslation Saved')->success();
            return redirect()->route('admin.privacy_policy_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.privacy_policy_translations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $privacy_policy_translations = $this->privacy_policy_translations->all();
        $data = $this->privacy_policy_translations->find($id);
        return view('admin.privacy_policy_translations.edit', compact('data', 'privacy_policy_translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyTranslationUpdateRequest $request, $id)
    {
        $this->privacy_policy_translations = $this->privacy_policy_translations->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->privacy_policy_translations->update($input);
            flash('PrivacyPolicyTranslation Saved')->success();
            return redirect()->route('admin.privacy_policy_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->privacy_policy_translations->destroy($id);
            flash('PrivacyPolicyTranslation Deleted')->success();
            return redirect()->route('admin.privacy_policy_translations.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
