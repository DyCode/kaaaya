<?php

namespace App\Http\Controllers\Admin;

use App\Models\DonationBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationBannerCreateRequest;
use App\Http\Requests\DonationBannerUpdateRequest;
use Illuminate\Http\Request;

class DonationBannerController extends Controller
{
    public $donation_banners;

    public function __construct(DonationBanner $donation_banners)
    {
        $this->donation_banners = $donation_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu donation_banners');
        if ($request->has('search')) {
            $data = $this->donation_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->donation_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.donation_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create donation_banners');

        return view('admin.donation_banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationBannerCreateRequest $request)
    {
        check_permission('create donation_banners');
        disable_active_flag($this->donation_banners);
        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->donation_banners->create($input);
            flash('DonationBanner Saved')->success();
            return redirect()->route('admin.donation_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read donation_banners');
        return view('admin.donation_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update donation_banners');
        $data = $this->donation_banners->find($id);
        return view('admin.donation_banners.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonationBannerUpdateRequest $request, $id)
    {
        check_permission('update donation_banners');
        disable_active_flag($this->donation_banners);
        $this->donation_banners = $this->donation_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->donation_banners->update($input);
            flash('DonationBanner Saved')->success();
            return redirect()->route('admin.donation_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete donation_banners');
        try {
            $this->donation_banners->destroy($id);
            flash('DonationBanner Deleted')->success();
            return redirect()->route('admin.donation_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
