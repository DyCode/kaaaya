<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\GalleryBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryBannerCreateRequest;
use App\Http\Requests\GalleryBannerUpdateRequest;
use Illuminate\Http\Request;

class GalleryBannerController extends Controller
{
    public $gallery_banners;
    public $galleries;

    public function __construct(GalleryBanner $gallery_banners, Gallery $galleries)
    {
        $this->gallery_banners = $gallery_banners;
        $this->galleries = $galleries;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu gallery_banners');
        if ($request->has('search')) {
            $data = $this->gallery_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->gallery_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.gallery_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create gallery_banners');
        $galleries = $this->galleries->all();

        return view('admin.gallery_banners.create', compact("galleries"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryBannerCreateRequest $request)
    {
        check_permission('create gallery_banners');
        disable_active_flag($this->gallery_banners);

        $input = $request->except("_token");
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->gallery_banners->create($input);
            flash('GalleryBanner Saved')->success();
            return redirect()->route('admin.gallery_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read gallery_banners');
        return view('admin.gallery_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update gallery_banners');
        $galleries = $this->galleries->all();
        $data = $this->gallery_banners->find($id);
        return view('admin.gallery_banners.edit', compact('data', 'galleries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryBannerUpdateRequest $request, $id)
    {
        check_permission('update gallery_banners');
        disable_active_flag($this->gallery_banners);
        
        $this->gallery_banners = $this->gallery_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            if($request->hasFile('banner')) {
            $path = $request->banner->store('images', env('FILESYSTEM_DRIVER', 'public'));
            $input['banner'] = $path;
            }
            $this->gallery_banners->update($input);
            flash('GalleryBanner Saved')->success();
            return redirect()->route('admin.gallery_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete gallery_banners');
        try {
            $this->gallery_banners->destroy($id);
            flash('GalleryBanner Deleted')->success();
            return redirect()->route('admin.gallery_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
