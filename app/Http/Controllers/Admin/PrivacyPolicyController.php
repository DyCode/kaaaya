<?php

namespace App\Http\Controllers\Admin;

use App\Models\PrivacyPolicy;
use App\Http\Controllers\Controller;
use App\Http\Requests\PrivacyPolicyCreateRequest;
use App\Http\Requests\PrivacyPolicyUpdateRequest;
use Illuminate\Http\Request;

class PrivacyPolicyController extends Controller
{
    public $privacy_policies;

    public function __construct(PrivacyPolicy $privacy_policies)
    {
        $this->privacy_policies = $privacy_policies;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu privacy_policies');
        if ($request->has('search')) {
            $data = $this->privacy_policies->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->privacy_policies->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.privacy_policies.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create privacy_policies');
        $privacy_policies = $this->privacy_policies->all();
        $data = $this->privacy_policies;

        return view('admin.privacy_policies.create', compact('data', "privacy_policies"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyCreateRequest $request)
    {
        check_permission('create privacy_policies');
        $input = $request->except("_token");
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->privacy_policies->create($input);
            flash('PrivacyPolicy Saved')->success();
            return redirect()->route('admin.privacy_policies.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read privacy_policies');
        return view('admin.privacy_policies.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update privacy_policies');
        $privacy_policies = $this->privacy_policies->all();
        $data = $this->privacy_policies->find($id);
        return view('admin.privacy_policies.edit', compact('data', 'privacy_policies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyUpdateRequest $request, $id)
    {
        check_permission('update privacy_policies');
        $this->privacy_policies = $this->privacy_policies->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('image')) {
			$path = $request->image->store('images', env('FILESYSTEM_DRIVER', 'public'));
			$input['image'] = $path;
			}
            $this->privacy_policies->update($input);
            flash('PrivacyPolicy Saved')->success();
            return redirect()->route('admin.privacy_policies.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete privacy_policies');
        try {
            $this->privacy_policies->destroy($id);
            flash('PrivacyPolicy Deleted')->success();
            return redirect()->route('admin.privacy_policies.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
