<?php

namespace App\Http\Controllers\Admin;

use App\Models\FaqCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqCategoryCreateRequest;
use App\Http\Requests\FaqCategoryUpdateRequest;
use Illuminate\Http\Request;

class FaqCategoryController extends Controller
{
    public $faq_categories;

    public function __construct(FaqCategory $faq_categories)
    {
        $this->faq_categories = $faq_categories;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu faq_categories');
        if ($request->has('search')) {
            $data = $this->faq_categories->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->faq_categories->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.faq_categories.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create faq_categories');
        $faq_categories = $this->faq_categories->all();
        $data = $this->faq_categories;

        return view('admin.faq_categories.create', compact('data', "faq_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqCategoryCreateRequest $request)
    {
        check_permission('create faq_categories');
        $input = $request->except("_token");
        try {
            $this->faq_categories->create($input);
            flash('FaqCategory Saved')->success();
            return redirect()->route('admin.faq_categories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read faq_categories');
        return view('admin.faq_categories.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update faq_categories');
        $faq_categories = $this->faq_categories->all();
        $data = $this->faq_categories->find($id);
        return view('admin.faq_categories.edit', compact('data', 'faq_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqCategoryUpdateRequest $request, $id)
    {
        check_permission('update faq_categories');
        $this->faq_categories = $this->faq_categories->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
            $this->faq_categories->update($input);
            flash('FaqCategory Saved')->success();
            return redirect()->route('admin.faq_categories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete faq_categories');
        try {
            $this->faq_categories->destroy($id);
            flash('FaqCategory Deleted')->success();
            return redirect()->route('admin.faq_categories.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
