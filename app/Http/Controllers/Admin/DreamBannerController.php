<?php

namespace App\Http\Controllers\Admin;

use App\Models\DreamBanner;
use App\Http\Controllers\Controller;
use App\Http\Requests\DreamBannerCreateRequest;
use App\Http\Requests\DreamBannerUpdateRequest;
use Illuminate\Http\Request;

class DreamBannerController extends Controller
{
    public $dream_banners;

    public function __construct(DreamBanner $dream_banners)
    {
        $this->dream_banners = $dream_banners;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        check_permission('menu dream_banners');
        if ($request->has('search')) {
            $data = $this->dream_banners->search($request->search)->orderBy('id', 'desc')->paginate();
        } else {
            $data = $this->dream_banners->orderBy('id', 'desc')->paginate(20);

        }
        return view('admin.dream_banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        check_permission('create dream_banners');

        $dream_banners = $this->dream_banners->all();
        $data = $this->dream_banners;

        return view('admin.dream_banners.create', compact('data', "dream_banners"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DreamBannerCreateRequest $request)
    {
        check_permission('create dream_banners');
        disable_active_flag($this->dream_banners);
        
        $input = $request->except("_token");
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images/banner', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->dream_banners->create($input);
            flash('DreamBanner Saved')->success();
            return redirect()->route('admin.dream_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        check_permission('read dream_banners');
        return view('admin.dream_banners.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        check_permission('update dream_banners');
        $dream_banners = $this->dream_banners->all();
        $data = $this->dream_banners->find($id);
        return view('admin.dream_banners.edit', compact('data', 'dream_banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DreamBannerUpdateRequest $request, $id)
    {
        check_permission('update dream_banners');
        disable_active_flag($this->dream_banners);
        
        $this->dream_banners = $this->dream_banners->findOrFail($id);
        $input = $request->except(["_token", "_method"]);
        try {
			if($request->hasFile('banner')) {
			$path = $request->banner->store('images/banner', env('FILESYSTEM_DRIVER', 'public'));
			$input['banner'] = $path;
			}
            $this->dream_banners->update($input);
            flash('DreamBanner Saved')->success();
            return redirect()->route('admin.dream_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        check_permission('delete dream_banners');
        try {
            $this->dream_banners->destroy($id);
            flash('DreamBanner Deleted')->success();
            return redirect()->route('admin.dream_banners.index');
        } catch (\Exception $e) {
            flash('Error :' . $e->getMessage())->error();
            return back();
        }
    }
}
