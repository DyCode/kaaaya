<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\ForgotRequest;
use App\Models\Consumer;
use App\Notifications\RegistrationNotif;
use App\Notifications\ForgotPassword;
class AuthenticateController extends Controller
{

    function __construct(Consumer $consumers)
    {
        $this->consumers = $consumers;
    }
    /**
     * @OA\Info(title="Kaaya API", version="0.1")
     */

    /**
     * @OA\POST(path="/api/v1/login",
     *   tags={"User"},
     *   summary="Logs user into the system",
     *   description="",
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"email", "password"},
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email User",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Invalid email/password supplied")
     * )
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            $credentials['active_flag'] = 1;
            // attempt to verify the credentials and create a token for the user
            if (!$token = auth('api')->attempt($credentials)) {
                return response_error('invalid_credentials or user has not been activated', 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response_error('could_not_create_token', 500);
        }

        // all good so return the token
        return response()->json([
            'status' => 'success',
            'message' => 'Login Succeed',
            'token' => $token
        ]);
    }


    /**
     * @OA\POST(path="/api/v1/register",
     *   tags={"User"},
     *   summary="Register user into the system",
     *   description="",
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"email", "password"},
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email User",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="Full Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="profile_image",
     *                     description="Profile Picture",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     description="Handphone / Fix Phone",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="birth_date",
     *                     description="Birth Date",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="gender",
     *                     description="ex: M or F",
     *                     enum={"M", "F"},
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="home_address",
     *                     description="Home Address",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="office_address",
     *                     description="Office Address",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Invalid email/password supplied")
     * )
     */
    public function register(RegisterRequest $request)
    {
        $input = $request->all();
       
        if($request->hasFile('profile_image')) {
            $path = $request->profile_image->store('images/profile_image', env('FILESYSTEM_DRIVER', 'public'));
            $input['profile_image'] = $path;
        }

        $input['password'] = bcrypt($input['password']);

        $consumer = $this->consumers->create($input);
        $consumer->notify(new RegistrationNotif);
        return response()->json([
            'status' => 'success',
            'message' => 'registration succeed, please check your email for next process'
        ]);
        
    }

    /**
     * @OA\POST(path="/api/v1/forgot",
     *   tags={"User"},
     *   summary="Reset Password",
     *   description="",
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"email", "password"},
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email User",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *     @OA\MediaType(mediaType="application/json"),
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=400, description="Invalid email/password supplied")
     * )
     */
    public function forgot(ForgotRequest $request)
    {
        $input = $request->all();
        $consumer = Consumer::where('email', $input['email'])->first();
        $new_password = str_random('10');
        $consumer['password'] = bcrypt($new_password);
        $consumer->update(['password', $new_password]);
        $consumer->notify(new ForgotPassword($new_password));
        return response()->json([
            'status' => 'success',
            'message' => 'Password resetted',
        ]);
        
    }
}
