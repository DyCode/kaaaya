<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = [];
        foreach(config('translatable.locales') as $lang) {
            $validation[$lang.'.title'] = 'required|max:255';
            $validation[$lang.'.description'] = 'required';
        }
        return $validation;
    }

    public function messages()
    {
        $messages = [];
        foreach(config('translatable.locales') as $lang) {
            $messages[$lang.'.title.required'] = 'A ['.$lang.']Title is required';
            $messages[$lang.'.description.required'] = 'A ['.$lang.']Description is required';
        }
        return $messages;
    }
}
