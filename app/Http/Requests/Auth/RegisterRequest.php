<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:consumers',
            'name' => 'required|string|max:50',
            'password' => 'required',
            'phone_number' => 'required',
            'birth_date' => 'required|date',
            'home_address' => 'string|min:20',
            'office_address' => 'string|min:20',
            'gender' => 'required|string|in:M,F',
        ];
    }
}
