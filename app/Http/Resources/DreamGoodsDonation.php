<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DreamGoodsDonation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle();
    }

    private function getSingle()
    {
        $data = [
            'id' => $this->id,
            'donation_date' => $this->donation_date->format('Y-m-d\TH:i:sP'),
            'amount_donated' => $this->amount_donated,
            'comment' => $this->comment,
            'verification_flag' => $this->verification_flag,
            'shipping_method' => new ShippingMethodDetail($this->shipping_method),
            'unit' => new UnitDetail($this->unit),
            'consumer' => new ConsumerDetail($this->consumer),
            'active_flag' => $this->active_flag,
        ];
        
        return $data;
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle(),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
