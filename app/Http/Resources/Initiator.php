<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class Initiator extends JsonResource
{
     /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle($request);
    }

    private function getSingle($request)
    {
        \App::setLocale($request->header('accept-language', 'id'));
        return [
            'id' => $this->id,
            'image' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->image),
            'name' => $this->name,
            'description' => $this->description,
            'active_flag' => $this->active_flag,
        ];
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle($request),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
