<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class DonationCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle();
    }

    private function getSingle()
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
            'galleries' => Gallery::collection($this->galleries),
            'category_icon' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->category_icon),
        ];
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle(),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
