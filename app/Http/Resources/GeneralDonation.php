<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class GeneralDonation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle();
    }

    private function getSingle()
    {
        $data = [
            'id' => $this->id,
            'date' => $this->date->format('Y-m-d\TH:i:sP'),
            'amount' => $this->amount,
            'transfer_slip' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->transfer_slip),
            'comment' => $this->comment,
            'verification_flag' => $this->verification_flag,
            'consumer' => new ConsumerDetail($this->consumer),
            'active_flag' => $this->active_flag,
        ];
        
        return $data;
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle(),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
