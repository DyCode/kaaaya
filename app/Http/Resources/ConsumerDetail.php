<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class ConsumerDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'profile_image' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->profile_image),
            'name' => $this->name,
            'email' => $this->email,
            'active_flag' => $this->active_flag,
        ];;
    }
}
