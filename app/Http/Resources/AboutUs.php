<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class AboutUs extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle($request);
    }


    private function getSingle($request)
    {
        \App::setLocale($request->header('accept-language', 'id'));
        return [
            'id' => $this->id,
            'banner' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->banner),
            'image' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->image),
            'who_we_are_image' => Storage::url($this->who_we_are_image),
            'description' => $this->description,
            'who_we_are_description' => $this->who_we_are_description,
            'active_flag' => $this->active_flag,
        ];
    }

    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle($request),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
