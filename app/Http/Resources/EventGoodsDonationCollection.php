<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EventGoodsDonationCollection extends ResourceCollection
{
    private $meta;
    private $message;

    public function __construct($resource)
    {
        $this->meta = [
            'pagination' => [
                'total' => $resource->total(),
                'count' => $resource->count(),
                'per_page' => $resource->perPage(),
                'current_page' => $resource->currentPage(),
                'total_pages' => $resource->lastPage()
            ],
            'base_url' => url('/'),
        ];

        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'message' => "Succeed",
            'data' => EventGoodsDonation::collection($this->collection),
            'meta' => $this->meta,
        ];
    }
}
