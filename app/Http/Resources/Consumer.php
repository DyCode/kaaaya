<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Consumer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle($request);
        
    }

    private function getSingle($request)
    {
        return [
            'id' => $this->id,
            'profile_image' => \Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->profile_image),
            'name' => $this->name,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'birth_date' => $this->birth_date->format('Y-m-d\TH:i:sP'),
            'gender' => $this->gender,
            'home_address' => $this->home_address,
            'office_address' => $this->office_address,
            'institution_name' => $this->institution_name,
            'active_flag' => $this->active_flag,

        ];
    }

    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle($request),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
