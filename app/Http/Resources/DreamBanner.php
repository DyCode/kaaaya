<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class DreamBanner extends JsonResource
{

    public function toArray($request)
    {
        return $this->getSingle();
    }

    private function getSingle()
    {
        return [
            'id' => $this->id,
            'banner' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->banner),
        ];
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle(),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
