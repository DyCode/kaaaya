<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FaqCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle($request);
    }

    private function getSingle($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
        ];
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle($request),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
