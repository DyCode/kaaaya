<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class EventDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'image' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->image),
            'title' => $this->title,
            'active_flag' => $this->active_flag,
        ];
    }
}
