<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoryComment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle();
    }

    private function getSingle()
    {
        $data = [
            'id' => $this->id,
            'comment' => $this->comment,
            'is_reply' => $this->reply_header_id ? true : false,
            'consumer' => new ConsumerDetail($this->consumer),
            'replies' => StoryComment::collection($this->replies),
        ];
        if ($this->reply_header_id) {
            unset($data['replies']);
            $data['header'] = [
                'id' => $this->header->id,
                'comment' => $this->header->comment,
                'consumer' => new ConsumerDetail($this->header->consumer),
                'created_at' => $this->header->created_at->format('Y-m-d\TH:i:sP'),
            ];
        }
        $data['created_at'] = $this->created_at->format('Y-m-d\TH:i:sP');
        return $data;
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle(),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
