<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class MainBanner extends JsonResource
{
    
     /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getSingle($request);
    }

    private function getSingle($request)
    {
        \App::setLocale($request->header('accept-language', 'id'));
        return [
            'id' => $this->id,
            'banner_no' => $this->banner_no,
            'banner' => Storage::disk(env('FILESYSTEM_DRIVER', 'public'))->url($this->banner),
        ];
    }

    public function withResponse($request, $response)
    {
        $response->setData([
            'message' => "Succeed",
            'data' => $this->getSingle($request),
            'meta' => [
                'base_url' => url('/'),
            ],
        ]);
    }
}
