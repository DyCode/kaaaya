<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContentCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $data;
    public $type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $type)
    {
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = env('APP_FRONTEND_URL') . '/'.$this->type.'/'.$this->data->id;
        $name = auth('api')->user()->name;

        $type = explode("_", $this->type);
        $type = array_map(function($d) { return studly_case($d); }, $type);
        $type = implode(" ", $type);
        return (new MailMessage)
                    ->subject(env('APP_NAME'). ' ' .$type.' created')
                    ->greeting('Hello! '. $name)
                    ->line('Thank you for participating with us.')
                    ->action('Notification Action', $url)
                    ->line('Hopefully your kindness will be rewarded');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
