<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KaaayaIndentity extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
		'logo',
		'institution_name',
		'address',
		'email',
		'phone_number',
		'fax_number',
		'created_by',
		'updated_by',
	];

}
