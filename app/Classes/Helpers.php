<?php
/**
 * Show text excerpt 
 */
if (!function_exists('excerpt')) {

    function excerpt($text, $word = 50, $end = "...") {
        return \Illuminate\Support\Str::words($text,$word, $end);
    }
}


if (!function_exists('permissions_map')) {

    function permissions_map($permissions) {
        return $permissions->map(function($d) {
            $permissions_name = [];
            $names = explode(" ", $d->name);
            $ends = explode("_",end($names));
            if (count($names) == 2) {
                $permissions_name = array_merge([$names[0]], $ends); 
            } else {
                $permissions_name = $ends;
            }

            return strtoupper(implode(" ", $permissions_name));
        });
    }
}

if (!function_exists('group_permission')) {
    function group_permission($permissions) {
        $mapping = $permissions->map(function($d) {
        	$permissions_name = [];
        	$names = explode(" ", $d->name);
        	$ends = explode("_",end($names));
        	if (count($names) == 2) {
        		$permissions_name = array_merge([$names[0]], $ends); 
                $group = strtoupper(implode(" ", $ends));
            } else {
                $permissions_name = $ends;
                $group = 'OTHER';
        	}

        	return [
                'id' => $d->id,
                'name' => strtoupper(implode(" ", $permissions_name)),
                'permission_name' => $d->name,
                'group' => $group,
            ];
        })->groupBy('group');

        return $mapping;
    }
}

if (!function_exists('check_permission')) {
    function check_permission($permission) {
        if(!auth()->user()->hasPermissionTo($permission)) {
            abort(403);
        }
    }
}

if (!function_exists('response_error')) {
    function response_error($message = 'unknown error', $code = 500) {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], $code);
    }
}

if (!function_exists('response_success')) {
    function response_success($message, $type = 'event', $data = []) {
        $output = [
            'status' => 'succeed',
            'message' => $message,
        ];

        if (count($data)) {
            $output['data'] = $data;
        }
        return response()->json($output);
    }
}


if (!function_exists('disable_active_flag')) {
    function disable_active_flag($model) {
        if (request()->get('active_flag')) {
            $model->get()->each(function($d) {
                $d->update(['active_flag' => 0]);
            });
        }

    }
}

