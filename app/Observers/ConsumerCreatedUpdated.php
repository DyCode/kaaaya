<?php

namespace App\Observers;

class ConsumerCreatedUpdated
{
    public function updating($model)
    {
    	if (auth('api')->user()) {
        	$model->consumer_id = auth('api')->user()->id;
    	}
    }


    public function creating($model)
    {
    	if (auth('api')->user()) {
        	$model->consumer_id = auth('api')->user()->id;
        }
    }
}
