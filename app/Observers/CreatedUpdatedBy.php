<?php

namespace App\Observers;

class CreatedUpdatedBy
{
    private $userID;



    // public function saving($model)
    // {
    //     $model->modfied_by = auth()->user()->id;
    // }

    // public function saved($model)
    // {
    //     $model->modfied_by = auth()->user()->id;
    // }


    public function updating($model)
    {
        $model->updated_by = auth()->user() ? auth()->user()->id : 0;
    }

    // public function updated($model)
    // {
    //     $model->modfied_by = auth()->user()->id;
    // }


    public function creating($model)
    {
        $model->created_by = auth()->user() ? auth()->user()->id : 0;
        $model->updated_by = auth()->user() ? auth()->user()->id : 0;
    }

    // public function created($model)
    // {
    //     $model->created_by = auth()->user()->id;
    // }


    // public function removing($model)
    // {
    //     $model->purged_by = auth()->user()->id;
    // }

    // public function removed($model)
    // {
    //     $model->purged_by = auth()->user()->id;
    // }
}
