<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaaayaIndentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kaaaya_indentities', function (Blueprint $table) {
            $table->string('logo', 100);
            $table->string('institution_name', 50);
            $table->text('address');
            $table->string('email', 100);
            $table->string('phone_number', 20);
            $table->string('fax_number', 20)->nullable();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kaaaya_indentities');
    }
}
