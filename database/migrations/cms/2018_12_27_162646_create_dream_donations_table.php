<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDreamDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dream_donations', function (Blueprint $table) {
            $table->increments('id');
            
			$table->integer('dream_id')->unsigned();
			$table->integer('consumer_id')->unsigned();
			$table->dateTime('date')->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->float('amount', 10, 2);
			$table->string('transfer_slip');
			$table->text('comment');
			$table->boolean('verification_flag')->default(0);
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->boolean('active_flag')->default(0);
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dream_donations');
    }
}