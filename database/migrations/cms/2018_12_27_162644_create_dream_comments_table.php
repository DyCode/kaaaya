<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDreamCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dream_comments', function (Blueprint $table) {
            $table->increments('id');
            
			$table->integer('dream_id')->unsigned();
			$table->integer('consumer_id')->unsigned();
			$table->text('comment');
			$table->boolean('reply_flag')->default(0);
			$table->boolean('verification_flag')->default(0);
			$table->integer('reply_header_id')->unsigned();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dream_comments');
    }
}