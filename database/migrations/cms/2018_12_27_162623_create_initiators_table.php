<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitiatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initiators', function (Blueprint $table) {
            $table->increments('id');
            
			$table->string('image');
			$table->string('name');
			$table->text('description');
			$table->boolean('active_flag')->default(0);
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('initiators');
    }
}