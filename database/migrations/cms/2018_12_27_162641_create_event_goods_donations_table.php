<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventGoodsDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_goods_donations', function (Blueprint $table) {
            $table->increments('id');
            
			$table->integer('event_id')->unsigned();
			$table->integer('consumer_id')->unsigned();
			$table->string('goods_needed');
			$table->boolean('amount_needed')->default(0);
			$table->integer('unit_id')->unsigned();
			$table->dateTime('donation_date')->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('amount_donated')->default(0);
			$table->integer('shipping_method_id')->unsigned();
			$table->text('comment');
			$table->boolean('verification_flag')->default(0);
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->boolean('active_flag')->default(0);
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_goods_donations');
    }
}