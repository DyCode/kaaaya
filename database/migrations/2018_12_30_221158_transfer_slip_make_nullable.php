<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferSlipMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_donations', function (Blueprint $table) {
            $table->string('transfer_slip')->nullable()->change();
        });
        Schema::table('story_donations', function (Blueprint $table) {
            $table->string('transfer_slip')->nullable()->change();
        });
        Schema::table('dream_donations', function (Blueprint $table) {
            $table->string('transfer_slip')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_donations', function (Blueprint $table) {
            //
        });
    }
}
