<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditReplyHeaderId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_comments', function (Blueprint $table) {
            $table->integer('reply_header_id')->unsigned()->nullable()->change();
        });
        Schema::table('story_comments', function (Blueprint $table) {
            $table->integer('reply_header_id')->unsigned()->nullable()->change();
        });
        Schema::table('dream_comments', function (Blueprint $table) {
            $table->integer('reply_header_id')->unsigned()->nullable()->change();
        });
        Schema::table('gallery_comments', function (Blueprint $table) {
            $table->integer('reply_header_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
