<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DonationBannerPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'menu donation_banners']);
        Permission::create(['name' => 'create donation_banners']);
        Permission::create(['name' => 'read donation_banners']);
        Permission::create(['name' => 'update donation_banners']);
        Permission::create(['name' => 'delete donation_banners']);

        $role = Role::where(['name' => 'super-admin'])->first();
        $role->givePermissionTo(Permission::all());
    }
}
