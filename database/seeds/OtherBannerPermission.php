<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class OtherBannerPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'menu gallery_banners']);
        Permission::create(['name' => 'create gallery_banners']);
        Permission::create(['name' => 'read gallery_banners']);
        Permission::create(['name' => 'update gallery_banners']);
        Permission::create(['name' => 'delete gallery_banners']);

        Permission::create(['name' => 'menu contact_us_banners']);
        Permission::create(['name' => 'create contact_us_banners']);
        Permission::create(['name' => 'read contact_us_banners']);
        Permission::create(['name' => 'update contact_us_banners']);
        Permission::create(['name' => 'delete contact_us_banners']);

        Permission::create(['name' => 'menu faq_banners']);
        Permission::create(['name' => 'create faq_banners']);
        Permission::create(['name' => 'read faq_banners']);
        Permission::create(['name' => 'update faq_banners']);
        Permission::create(['name' => 'delete faq_banners']);

        Permission::create(['name' => 'menu consumer_banners']);
        Permission::create(['name' => 'create consumer_banners']);
        Permission::create(['name' => 'read consumer_banners']);
        Permission::create(['name' => 'update consumer_banners']);
        Permission::create(['name' => 'delete consumer_banners']);

        $role = Role::where(['name' => 'super-admin'])->first();
        $role->givePermissionTo(Permission::all());
    }
}
