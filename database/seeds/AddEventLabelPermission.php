<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class AddEventLabelPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'menu event_labels']);
        Permission::create(['name' => 'create event_labels']);
        Permission::create(['name' => 'read event_labels']);
        Permission::create(['name' => 'update event_labels']);
        Permission::create(['name' => 'delete event_labels']);

        $role = Role::where(['name' => 'super-admin'])->first();
        $role->givePermissionTo(Permission::all());
    }
}
