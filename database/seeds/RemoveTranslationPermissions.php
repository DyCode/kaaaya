<?php

use Illuminate\Database\Seeder;

class RemoveTranslationPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::delete(\DB::raw("DELETE FROM permissions where name like '%_translations' "));
    }
}
