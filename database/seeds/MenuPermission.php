<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class MenuPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'Menu Donation']);
        Permission::create(['name' => 'Menu Comments']);
        Permission::create(['name' => 'Menu Master']);
        Permission::create(['name' => 'Menu Utility']);
        Permission::create(['name' => 'menu permissions']);
        Permission::create(['name' => 'menu users']);
        Permission::create(['name' => 'edit permissions']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'add permissions']);
        Permission::create(['name' => 'add users']);
        Permission::create(['name' => 'delete permissions']);
        Permission::create(['name' => 'delete users']);
        $role = Role::where(['name' => 'super-admin'])->first();
        $role->givePermissionTo(Permission::all());
    }
}
