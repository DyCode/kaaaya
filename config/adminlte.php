<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'KAaAya',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>KAaAya</b> CMS',

    'logo_mini' => '<img src="/images/logo-kaaaya.png" width="32"/>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'black',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'MAIN NAVIGATION',
		// [
		// 	'text' => 'Notification',
		// 	'url'  => 'admin/notifications',
		// ],
		[
			'text' => 'Consumer',
			'url'  => 'admin/consumers',
            'role' => 'menu consumers',
		],
		
		[
			'text' => 'Event',
			'url'  => 'admin/events',
            'role' => 'menu events',
		],
        [
            'text' => 'Event Label',
            'url'  => 'admin/event_labels',
            'role' => 'menu event_labels',
        ],
		[
			'text' => 'Dream',
			'url'  => 'admin/dreams',
            'role' => 'menu dreams',
		],
		
		[
			'text' => 'Gallery',
			'url'  => 'admin/galleries',
            'role' => 'menu galleries',
		],
        [
            'text' => 'Story',
            'url'  => 'admin/stories',
            'role' => 'menu stories',
        ],
		
        
        [
            'text' => 'Donation',
            'role' => 'Menu Donation',
            'url'  => '#',
            'submenu' => [
                [
                    'text' => 'Donation Category',
                    'url'  => 'admin/donation_categories',
                    'role' => 'menu donation_categories',
                ],
                
                [
                    'text' => 'General Donation',
                    'url'  => 'admin/general_donations',
                    'role' => 'menu general_donations',
                ],
                [
                    'text' => 'Dream Donation',
                    'url'  => 'admin/dream_donations',
                    'role' => 'menu dream_donations',
                ],
                [
                    'text' => 'Dream Goods Donation',
                    'url'  => 'admin/dream_goods_donations',
                    'role' => 'menu dream_goods_donations',
                ],
                [
                    'text' => 'Event Donation',
                    'url'  => 'admin/event_donations',
                    'role' => 'menu event_donations',
                ],
                [
                    'text' => 'Event Goods Donation',
                    'url'  => 'admin/event_goods_donations',
                    'role' => 'menu event_goods_donations',
                ],
                [
                    'text' => 'Story Donation',
                    'url'  => 'admin/story_donations',
                    'role' => 'menu story_donations',
                ],
            ]
        ],
        [
            'text' => 'Comments',
            'role' => 'Menu Comments',
            'url'  => '#',
            'submenu' => [
                [
                    'text' => 'Gallery Comment',
                    'url'  => 'admin/gallery_comments',
                    'role' => 'menu gallery_comments',
                ],
                [
                    'text' => 'Dream Comment',
                    'url'  => 'admin/dream_comments',
                    'role' => 'menu dream_comments',
                ],
                [
                    'text' => 'Event Comment',
                    'url'  => 'admin/event_comments',
                    'role' => 'menu event_comments',
                ],
                [
                    'text' => 'Story Comment',
                    'url'  => 'admin/story_comments',
                    'role' => 'menu story_comments',
                ],
            ]
        ],
        [
            'text' => 'Master',
            'role' => 'Menu Master',
            'url'  => '#',
            'submenu' => [
                [
                    'text' => 'Unit',
                    'url'  => 'admin/units',
                    'role' => 'menu units',
                ],
                [
                    'text' => 'Shipping Method',
                    'url'  => 'admin/shipping_methods',
                    'role' => 'menu shipping_methods',
                ],
            ]
        ],
        [
            'text' => 'Utility',
            'role' => 'Menu Utility',
            'url'  => '#',
            'submenu' => [
                
                [
                    'text' => 'Main Banner',
                    'url'  => 'admin/main_banners',
                    'role' => 'menu main_banners',
                ],
                [
                    'text' => 'Donation Banner',
                    'url'  => 'admin/donation_banners',
                    'role' => 'menu donation_banners',
                ],
                [
                    'text' => 'Event Banner',
                    'url'  => 'admin/event_banners',
                    'role' => 'menu event_banners',
                ],
                [
                    'text' => 'Story Banner',
                    'url'  => 'admin/story_banners',
                    'role' => 'menu story_banners',
                ],
                [
                    'text' => 'Dream Banner',
                    'url'  => 'admin/dream_banners',
                    'role' => 'menu dream_banners',
                ],
                [
                    'text' => 'Donation Category Banner',
                    'url'  => 'admin/donation_category_banners',
                    'role' => 'menu donation_category_banners',
                ],
                [
                    'text' => 'Gallery Banner',
                    'url'  => 'admin/gallery_banners',
                    'role' => 'menu gallery_banners',
                ],
                [
                    'text' => 'Contact Us Banner',
                    'url'  => 'admin/contact_us_banners',
                    'role' => 'menu contact_us_banners',
                ],
                [
                    'text' => 'FAQ Banner',
                    'url'  => 'admin/faq_banners',
                    'role' => 'menu faq_banners',
                ],
                [
                    'text' => 'Consumer Banner',
                    'url'  => 'admin/consumer_banners',
                    'role' => 'menu consumer_banners',
                ],
                [
                    'text' => 'Initiator',
                    'url'  => 'admin/initiators',
                    'role' => 'menu initiators',
                ],
                [
                    'text' => 'About Us',
                    'url'  => 'admin/about_uses',
                    'role' => 'menu about_uses',
                ],
                [
                    'text' => 'Faq',
                    'url'  => 'admin/faqs',
                    'role' => 'menu faqs',
                ],
                [
                    'text' => 'Faq Category',
                    'url'  => 'admin/faq_categories',
                    'role' => 'menu faq_categories',
                ],
                [
                    'text' => 'Identity',
                    'url'  => 'admin/identities',
                    'role' => 'menu identities',
                ],
                [
                    'text' => 'Privacy Policy',
                    'url'  => 'admin/privacy_policies',
                    'role' => 'menu privacy_policies',
                ],
                [
                    'text' => 'Roles & Permission',
                    'url'  => 'admin/permissions',
                    'role' => 'menu permissions',
                ],
                [
                    'text' => 'Users',
                    'url'  => 'admin/users',
                    'role' => 'menu users',
                ],
            ]
        ],

        // 'MASTER',

        // 'ACCOUNT SETTINGS',
        // [
        //     'text' => 'Profile',
        //     'url'  => 'admin/settings',
        //     'icon' => 'user',
        // ],
        // [
        //     'text' => 'Change Password',
        //     'url'  => 'admin/settings',
        //     'icon' => 'lock',
        // ],
        // [
        //     'text'    => 'Multilevel',
        //     'icon'    => 'share',
        //     'submenu' => [
        //         [
        //             'text' => 'Level One',
        //             'url'  => '#',
        //         ],
        //         [
        //             'text'    => 'Level One',
        //             'url'     => '#',
        //             'submenu' => [
        //                 [
        //                     'text' => 'Level Two',
        //                     'url'  => '#',
        //                 ],
        //                 [
        //                     'text'    => 'Level Two',
        //                     'url'     => '#',
        //                     'submenu' => [
        //                         [
        //                             'text' => 'Level Three',
        //                             'url'  => '#',
        //                         ],
        //                         [
        //                             'text' => 'Level Three',
        //                             'url'  => '#',
        //                         ],
        //                     ],
        //                 ],
        //             ],
        //         ],
        //         [
        //             'text' => 'Level One',
        //             'url'  => '#',
        //         ],
        //     ],
        // ],
        // 'LABELS',
        // [
        //     'text'       => 'Important',
        //     'icon_color' => 'red',
        // ],
        // [
        //     'text'       => 'Warning',
        //     'icon_color' => 'yellow',
        // ],
        // [
        //     'text'       => 'Information',
        //     'icon_color' => 'aqua',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
