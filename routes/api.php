<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/login', 'Auth\AuthenticateController@authenticate');
Route::post('/register', 'Auth\AuthenticateController@register');
Route::post('/forgot', 'Auth\AuthenticateController@forgot');
Route::resource('/about_uses', 'Api\AboutUsController', ['as' => 'api']);
Route::resource('/main_banners', 'Api\MainBannerController', ['as' => 'api']);
Route::resource('/event_banners', 'Api\EventBannerController', ['as' => 'api']);
Route::resource('/event_labels', 'Api\EventLabelController', ['as' => 'api']);
Route::resource('/dream_banners', 'Api\DreamBannerController', ['as' => 'api']);
Route::resource('/story_banners', 'Api\StoryBannerController', ['as' => 'api']);
Route::resource('/privacy_policies', 'Api\PrivacyPolicyController', ['as' => 'api']);
Route::resource('/donation_categories', 'Api\DonationCategoryController', ['as' => 'api']);
Route::resource('/donation_category_banners', 'Api\DonationCategoryBannerController', ['as' => 'api']);
Route::resource('/gallery_banners', 'Api\GalleryBannerController', ['as' => 'api']);
Route::resource('/contact_us_banners', 'Api\ContactUsBannerController', ['as' => 'api']);
Route::resource('/faq_banners', 'Api\FaqBannerController', ['as' => 'api']);
Route::resource('/consumer_banners', 'Api\ConsumerBannerController', ['as' => 'api']);
Route::resource('/donation_banners', 'Api\DonationBannerController', ['as' => 'api']);
Route::resource('/donation_categories/{id}/galleries', 'Api\GalleryController', ['as' => 'api']);

Route::resource('/shipping_methods', 'Api\ShippingMethodController', ['as' => 'api']);
Route::resource('/units', 'Api\UnitController', ['as' => 'api']);
Route::resource('/faqs', 'Api\FaqController', ['as' => 'api']);
Route::resource('/faq_categories', 'Api\FaqCategoryController', ['as' => 'api']);
Route::resource('/initiators', 'Api\InitiatorController', ['as' => 'api']);
Route::middleware(['auth:api'])->group(function () {
	Route::post('/general_donations/{general_donation}/receipt', 'Api\GeneralDonationController@receipt')->name('api.general_donation.receipt');
	Route::resource('/general_donations', 'Api\GeneralDonationController', ['as' => 'api']);
	// Route::resource('/notifications', 'Api\NotificationController', ['as' => 'api']);
	Route::resource('/consumers', 'Api\ConsumerController', ['as' => 'api']);
	Route::post('/stories/{id}/comments/{story_comment}/reply', 'Api\StoryCommentController@reply')->name('api.story_comments.reply');
	Route::post('/stories/{id}/donations/{story_donation}/receipt', 'Api\StoryDonationController@receipt')->name('api.story_donation.receipt');
	Route::resource('/stories', 'Api\StoryController', ['as' => 'api']);
	Route::resource('/stories/{id}/comments', 'Api\StoryCommentController', ['as' => 'api']);
	Route::resource('/stories/{id}/donations', 'Api\StoryDonationController', ['as' => 'api']);
	Route::resource('/events', 'Api\EventController', ['as' => 'api']);
	Route::post('/events/{id}/comments/{event_comment}/reply', 'Api\EventCommentController@reply')->name('api.event_comments.reply');
	Route::resource('/events/{id}/comments', 'Api\EventCommentController', ['as' => 'api']);
	Route::resource('/events/{id}/donations', 'Api\EventDonationController', ['as' => 'api']);
	Route::post('/events/{id}/donations/{event_donation}/receipt', 'Api\EventDonationController@receipt')->name('api.event_donation.receipt');
	Route::resource('/events/{id}/goods_donations', 'Api\EventGoodsDonationController', ['as' => 'api']);
	Route::resource('/dreams', 'Api\DreamController', ['as' => 'api']);
	Route::post('/dreams/{id}/comments/{dream_comment}/reply', 'Api\DreamCommentController@reply')->name('api.dream_comment.reply');
	Route::post('/dreams/{id}/donations/{dream_donation}/receipt', 'Api\DreamDonationController@receipt')->name('api.dream_donation.receipt');
	Route::resource('/dreams/{id}/comments', 'Api\DreamCommentController', ['as' => 'api']);
	Route::resource('/dreams/{id}/donations', 'Api\DreamDonationController', ['as' => 'api']);
	Route::resource('/dreams/{id}/goods_donations', 'Api\DreamGoodsDonationController', ['as' => 'api']);
	Route::post('/donation_categories/{id}/galleries/{gallery_id}/comments/{gallery_comment}/reply', 'Api\GalleryCommentController@reply')->name('api.gallery_comments.reply');
	Route::resource('/donation_categories/{id}/galleries/{gallery_id}/comments', 'Api\GalleryCommentController', ['as' => 'api']);
	


	Route::get('/my', 'Api\MyController@index')->name('api.my.index');
	Route::get('/my/donations/events', 'Api\MyController@event_donations')->name('api.my.event_donations');
	Route::get('/my/donations/dreams', 'Api\MyController@dream_donations')->name('api.my.dream_donations');
	Route::get('/my/donations/stories', 'Api\MyController@story_donations')->name('api.my.story_donations');
	Route::get('/my/comments/events', 'Api\MyController@event_comments')->name('api.my.event_comments');
	Route::get('/my/comments/dreams', 'Api\MyController@dream_comments')->name('api.my.dream_comments');
	Route::get('/my/comments/stories', 'Api\MyController@story_comments')->name('api.my.story_comments');
	Route::get('/my/comments/galleries', 'Api\MyController@gallery_comments')->name('api.my.gallery_comments');
	Route::post('/my/change_password', 'Api\MyController@change_password')->name('api.my.change_password');
	Route::post('/my/profile_image', 'Api\MyController@profile_image')->name('api.my.profile_image');
	Route::post('/my/update_profile', 'Api\MyController@update_profile')->name('api.my.update_profile');
	Route::get('/my/events', 'Api\MyController@events')->name('api.my.events');
	Route::get('/my/events/{id}', 'Api\MyController@event')->name('api.my.event');
	Route::get('/my/dreams', 'Api\MyController@dreams')->name('api.my.dreams');
	Route::get('/my/dream/{id}', 'Api\MyController@dream')->name('api.my.dream');
	Route::get('/my/stories', 'Api\MyController@stories')->name('api.my.stories');
	Route::get('/my/stories/{id}', 'Api\MyController@story')->name('api.my.story');
});

