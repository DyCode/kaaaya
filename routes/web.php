<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::Auth();
Route::get('/', function () {
    return view('welcome');
});
Route::get('setlocale/{locale}', 'MiscController@setlocale');
Route::middleware(['auth'])->group(function () {
		Route::resource('/admin/about_uses', 'Admin\AboutUsController', ['as' => 'admin']);
		Route::resource('/admin/about_us_translations', 'Admin\AboutUsTranslationController', ['as' => 'admin']);
		Route::resource('/admin/main_banners', 'Admin\MainBannerController', ['as' => 'admin']);
		Route::resource('/admin/event_banners', 'Admin\EventBannerController', ['as' => 'admin']);
		Route::resource('/admin/event_labels', 'Admin\EventLabelController', ['as' => 'admin']);
		Route::resource('/admin/dream_banners', 'Admin\DreamBannerController', ['as' => 'admin']);
		Route::resource('/admin/story_banners', 'Admin\StoryBannerController', ['as' => 'admin']);
		Route::resource('/admin/donation_banners', 'Admin\DonationBannerController', ['as' => 'admin']);
		Route::resource('/admin/shipping_methods', 'Admin\ShippingMethodController', ['as' => 'admin']);
		Route::resource('/admin/units', 'Admin\UnitController', ['as' => 'admin']);
		Route::resource('/admin/initiators', 'Admin\InitiatorController', ['as' => 'admin']);
		Route::resource('/admin/general_donations', 'Admin\GeneralDonationController', ['as' => 'admin']);
		Route::resource('/admin/notifications', 'Admin\NotificationController', ['as' => 'admin']);
		Route::resource('/admin/privacy_policies', 'Admin\PrivacyPolicyController', ['as' => 'admin']);
		Route::resource('/admin/privacy_policy_translations', 'Admin\PrivacyPolicyTranslationController', ['as' => 'admin']);
		Route::resource('/admin/donation_categories', 'Admin\DonationCategoryController', ['as' => 'admin']);
		Route::resource('/admin/donation_category_banners', 'Admin\DonationCategoryBannerController', ['as' => 'admin']);
		Route::resource('/admin/gallery_banners', 'Admin\GalleryBannerController', ['as' => 'admin']);
		Route::resource('/admin/contact_us_banners', 'Admin\ContactUsBannerController', ['as' => 'admin']);
		Route::resource('/admin/faq_banners', 'Admin\FaqBannerController', ['as' => 'admin']);
		Route::resource('/admin/consumer_banners', 'Admin\ConsumerBannerController', ['as' => 'admin']);
		Route::resource('/admin/consumers', 'Admin\ConsumerController', ['as' => 'admin']);
		Route::resource('/admin/stories', 'Admin\StoryController', ['as' => 'admin']);
		Route::resource('/admin/story_translations', 'Admin\StoryTranslationController', ['as' => 'admin']);
		Route::resource('/admin/story_comments', 'Admin\StoryCommentController', ['as' => 'admin']);
		Route::resource('/admin/story_donations', 'Admin\StoryDonationController', ['as' => 'admin']);
		Route::resource('/admin/events', 'Admin\EventController', ['as' => 'admin']);
		Route::resource('/admin/event_translations', 'Admin\EventTranslationController', ['as' => 'admin']);
		Route::resource('/admin/event_comments', 'Admin\EventCommentController', ['as' => 'admin']);
		Route::resource('/admin/event_donations', 'Admin\EventDonationController', ['as' => 'admin']);
		Route::resource('/admin/event_goods_donations', 'Admin\EventGoodsDonationController', ['as' => 'admin']);
		Route::resource('/admin/dreams', 'Admin\DreamController', ['as' => 'admin']);
		Route::resource('/admin/dream_translations', 'Admin\DreamTranslationController', ['as' => 'admin']);
		Route::resource('/admin/dream_comments', 'Admin\DreamCommentController', ['as' => 'admin']);
		Route::resource('/admin/dream_donations', 'Admin\DreamDonationController', ['as' => 'admin']);
		Route::resource('/admin/dream_goods_donations', 'Admin\DreamGoodsDonationController', ['as' => 'admin']);
		Route::resource('/admin/galleries', 'Admin\GalleryController', ['as' => 'admin']);
		Route::resource('/admin/gallery_translations', 'Admin\GalleryTranslationController', ['as' => 'admin']);
		Route::resource('/admin/gallery_comments', 'Admin\GalleryCommentController', ['as' => 'admin']);
		Route::resource('/admin/faqs', 'Admin\FaqController', ['as' => 'admin']);
		Route::resource('/admin/faq_translations', 'Admin\FaqTranslationController', ['as' => 'admin']);
		Route::resource('/admin/faq_categories', 'Admin\FaqCategoryController', ['as' => 'admin']);
		Route::resource('/admin/users', 'Admin\UserController', ['as' => 'admin']);
    	Route::get('/admin/permissions', 'Admin\MiscController@permissions')->name('admin.permissions.index');
    	Route::get('/admin/permissions/create', 'Admin\MiscController@permissions_create')->name('admin.permissions.create');
    	Route::post('/admin/permissions/store', 'Admin\MiscController@permissions_store')->name('admin.permissions.store');
    	Route::put('/admin/permissions/update/{id}', 'Admin\MiscController@permissions_update')->name('admin.permissions.update');
    	Route::delete('/admin/permissions/destroy/{id}', 'Admin\MiscController@permissions_destroy')->name('admin.permissions.destroy');
    	Route::get('/admin/permissions/edit/{id}', 'Admin\MiscController@permissions_edit')->name('admin.permissions.edit');
    	Route::get('/home', 'Admin\HomeController@index')->name('home');
});
